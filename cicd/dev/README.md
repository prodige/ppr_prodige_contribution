# Développements sur ce projet avec `pr_prodige`

Installer au préalable `pr_prodige` en mode développement selon la documentation `pr_prodige/documentation/installation_developpement.md`.

## Après démarrage du Docker

Dans un terminal se connecter au conteneur :

```bash
# Connect to container composer install as www-data
docker exec --user www-data -w /var/www/html/site -it ppr_prodige_contribution_web bash
```

Dans le conteneur :

```bash
# Install with composer
composer install

# Migrate database
php ./bin/console doctrine:migrations:migrate --no-interaction

#Clear cache
php ./bin/console cache:clear --env dev
php ./bin/console cache:clear --env prod

# Follow symfony logs
tail -f var/log/dev.log
```
