<?php

declare(strict_types=1);

namespace App\DataProvider;

use ApiPlatform\Core\DataProvider\ContextAwareCollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use ApiPlatform\Core\DataProvider\SerializerAwareDataProviderInterface;
use App\Entity\Data;
use App\Response\JsonLDResponse;
use App\Service\AdminCartoDataService;
use App\Service\AdminService;
use App\Service\FeatureCatalogueService;
use App\Service\GeonetworkService;
use Doctrine\Persistence\ManagerRegistry;
use Psr\Container\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Security\Core\Security;
use Psr\Log\LoggerInterface;


final class DataProvider implements ContextAwareCollectionDataProviderInterface, RestrictedDataProviderInterface,
                                    SerializerAwareDataProviderInterface, ItemDataProviderInterface
{
    public function __construct(
        private ManagerRegistry $managerRegistry,
        private AdminCartoDataService $adminCartoDataService,
        private AdminService $adminService,
        private Security $security,
        private ParameterBagInterface $parameterBag,
        private GeonetworkService $geonetworkService,
        private FeatureCatalogueService $featureCatalogueService,
        private LoggerInterface $logger
    ) {
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return Data::class === $resourceClass;
    }

    /**
     * @throws \Exception
     */
    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = [])
    {
        $manager = $this->managerRegistry->getManagerForClass($resourceClass);
        $data = $manager->getRepository($resourceClass)->findOneBy(['id' => $id]);
        $getDataRights = false;
        if ($data === null) {
            return new JsonLDResponse(["error" => 'Data not found'],
                404,
                [],
                false,
                "Data",
                $id);
        }
        $user = $this->security->getUser()->getUsername();

        /** @var Data $data */
        if ($data->getAdmincartoDataId() !== null) {
            if ((isset($context['item_operation_name'])) || $operationName === null) {


                if ($user === $this->parameterBag->get('PHPCLI_DEFAULT_LOGIN')) {
                    $getDataRights = true;
                }
                $dataFields = $this->adminCartoDataService->curlGetData($data->getAdmincartoDataId());
                if (array_key_exists("error", $dataFields)) {
                    return new JsonLDResponse(["error" => $dataFields['error']],
                        $dataFields['status'],
                        [],
                        false,
                        "Data");
                }

                if ($data->getMetadataSheetId() !== null) {
                    $getType = $this->adminService->dataType($data, $dataFields);

                    if (isset($getType['uri'])) {
                        $dataFields['Data']['type'] = $getType['uri'];
                        $subdomains = $this->adminService->getSubdomainsById($data->getMetadataSheetId());
                        if (array_key_exists('error', $subdomains)) {
                            return new JsonLDResponse(["error" => $subdomains['error']],
                                $subdomains['status'],
                                [],
                                false,
                                "Data");
                        }
                        $dataFields['Data']['subdomains'] = $subdomains['data'];
                        if ($user !== $this->parameterBag->get('PHPCLI_DEFAULT_LOGIN')) {
                            $xmlInfo = $this->geonetworkService->getItemInfo($data->getMetadataUuid());
                            if (array_key_exists('error', $xmlInfo)) {
                                return new JsonLDResponse(["error" => $xmlInfo['error']],
                                    $xmlInfo['status'],
                                    [],
                                    false,
                                    "Data");
                            }
                            $dataFields['Data'] = array_merge($dataFields['Data'], $xmlInfo);
                        }
                        $layer = $this->adminService->getAdminLayerIdByTableName($dataFields['Data']['table']);

                        if (array_key_exists('error', $layer)) {
                            return new JsonLDResponse(["error" => $layer['error']],
                                $layer['status'],
                                [],
                                false,
                                "Data");
                        }
                        $layerInfo = $this->adminService->getLayerInfo($layer['layer']['id']);

                        if (isset($layerInfo['data']['wfsMetadataUuid'])) {
                            $dataFields['Data']['wfsMetadataUuid'] = $layerInfo['data']['wfsMetadataUuid'];
                        }
                        if (array_key_exists('error', $layerInfo)) {
                            return new JsonLDResponse(["error" => $layerInfo['error']],
                                $layerInfo['status'],
                                [],
                                false,
                                "Data");
                        }
                        $wmsWfsInfo = $this->adminService->getWmsWfs($layerInfo['data']);
                        if (array_key_exists('error', $wmsWfsInfo)) {
                            return new JsonLDResponse(["error" => $wmsWfsInfo['error']],
                                $wmsWfsInfo['status'],
                                [],
                                false,
                                "Data");
                        }
                        if ($user !== $this->parameterBag->get('PHPCLI_DEFAULT_LOGIN')) {
                            $dataFields['Data'] = $this->featureCatalogueService->getDescription(
                                $data,
                                $dataFields['Data']
                            );
                            $dataFields['Data'] = array_merge($dataFields['Data'], $wmsWfsInfo);
                        }
                    } else {
                        return new JsonLDResponse(["error" => 'Type de données non récupéré'], 404, [], false, "Data");
                    }
                } else {
                    $i = 0;
                    foreach ($dataFields['Data']['fields'] as $file) {
                        $dataFields['Data']['fields'][$i]['description'] = $file['name'];
                        $i++;
                    }
                }
                foreach ($dataFields['Data'] as $key => $value) {
                    switch ($key) {
                        case 'map':
                            $data->setMap($value);
                            break;
                        case 'layer':
                            $data->setLayerName($value);
                            break;
                        case 'fields':
                            $data->setFields($value);
                            break;
                        case 'sourceSRS':
                            $data->setSourceSRS((int)$value);
                            break;
                        case 'sourceEncoding':
                            $data->setSourceEncoding($value);
                            break;
                        case 'extent':
                            $data->setExtent($value);
                            break;
                        case 'table':
                            $data->setTable($value);
                            break;
                        case 'title':
                            $data->setTitle($value);
                            break;
                        case 'abstract':
                            $data->setAbstract($value);
                            break;
                        case 'lineage':
                            $data->setLineage($value);
                            break;
                        case 'localisationKeyword':
                            $data->setLocalisationKeyword($value);
                            break;
                        case 'themeKeyword':
                            $data->setThemeKeyword($value);
                            break;
                        case 'otherKeyword':
                            $data->setOtherKeyword($value);
                            break;
                        case 'equivalentScale':
                            $data->setEquivalentScale((int)$value);
                            break;
                        case 'contact':
                            $data->setContact($value);
                            break;
                        case 'status':
                            $data->setGeonetworkStatus(!is_int($value) ? $value : 'null');
                            break;
                        case 'wfsMetadataUuid':
                            $data->setWfsMetadataUuid($value);
                            break;
                        case 'openData':
                            $data->setOpenData($value);
                            break;
                        case 'subdomains':
                            $data->setSubdomains($value);
                            break;
                        case 'wms':
                            $data->setWms($value);
                            break;
                        case 'wfs':
                            $data->setWfs($value);
                            break;
                        case 'type':
                            $data->setType($value);
                            break;
                        case 'mdConstrainte':
                            $data->setMdContrainte($value);
                            break;
                        case 'thematiqueIso':
                            $data->setThematiqueISO($value);
                            break;
                        case 'mdMaintenance':
                            $data->setMdMaintenance($value);
                            break;
                    }
                }
            }

            if ($getDataRights !== true && $data->getSubdomains()) {
                $getDataRights = $this->adminService->controlsMetadataRight($data);
                if (isset($getDataRights['traitement']) && $getDataRights['traitement'] === false) {
                    return new JsonLDResponse(["error" => 'Access denied'], 403, [], false, "Data");
                }
            }
        }

        if(empty($data->getContact())){
            $getStructureContact = $this->adminService->getStructureContact($this->security->getUser());
            if(!array_key_exists('error', $getStructureContact) || !empty($getStructureContact)){
                $data->setContact($getStructureContact);
            }
        }

        return $data;
    }

    public function getCollection(string $resourceClass, string $operationName = null, array $context = []): array
    {
        $manager = $this->managerRegistry->getManagerForClass($resourceClass);
        $data = $manager->getRepository($resourceClass)->findAll();
        return $data;
    }

    public function setSerializerLocator(ContainerInterface $serializerLocator)
    {
        // TODO: Implement setSerializerLocator() method.
    }
}
