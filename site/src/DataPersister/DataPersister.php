<?PHP

declare(strict_types=1);

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use ApiPlatform\Exception\ResourceClassNotSupportedException;
use App\DataProvider\DataProvider;
use App\Entity\Data;
use App\Entity\DataFile;
use App\Entity\Status;
use App\Service\AdminCartoDataService;
use App\Response\JsonLDResponse;
use App\Service\AdminService;
use App\Service\FeatureCatalogueService;
use App\Service\GeonetworkService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Prodige\ProdigeBundle\Services\AdminClientService;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Security;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

final class DataPersister implements ContextAwareDataPersisterInterface
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private Security $security,
        private AdminCartoDataService $adminCartoDataService,
        private DataProvider $dataProvider,
        private AdminClientService $adminClientService,
        private AdminService $adminService,
        private GeonetworkService $geonetworkService,
        private ParameterBagInterface $parameterBag,
        private FeatureCatalogueService $featureCatalogueService,
        private ValidatorInterface $validator,
        private LoggerInterface $logger
    ) {
    }

    public function supports($data, array $context = []): bool
    {
        return ($data instanceof Data);
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws ResourceClassNotSupportedException
     * @throws Exception
     */
    public function persist($data, array $context = [])
    {
        $user = $this->adminService->getUserFromAdmin($this->security->getUser()->getId());

        if (array_key_exists('user', $user)) {
            $this->validator->validate($data, null, ['ValidInitializeData']);
            $user = $user['user'];
            //I check if the user has CMS rights
            if (array_key_exists('right', $this->adminClientService->verifyRights('CMS')) && $this->adminClientService->verifyRights('CMS')['right']['success']) {
                if ($context['operation_name'] === "api_data_post_collection") {
                    $data->setUserId($this->security->getUser()->getId());
                    $data->setStatus($this->entityManager->getRepository(Status::class)->find(["id" => 1]));
                } else {
                    switch ($context['operation_name']) {
                        case "api_data_initialize_item":
                            if ($data->getStatus()->getId() === 3 && $this->entityManager->getUnitOfWork()->getOriginalEntityData($data)['status']->getId() === 2) {
                                $files = $this->entityManager->getRepository(DataFile::class)->findBy(['data' => $data]
                                );

                                // I check the integrity of the files before starting
                                $responseIntegrity = $this->adminCartoDataService->checkFieldIntegrity($files);

                                if (array_key_exists("error", $responseIntegrity)) {
                                    return new JsonLDResponse(["error" => $responseIntegrity['error']],
                                        $responseIntegrity['status'],
                                        [],
                                        false,
                                        "Data");
                                }
                                $datafiles = $this->adminCartoDataService->createRequestCurlPost($files);
                                if ($data->getProjection()) {
                                    $datafiles['projection'] = $data->getProjection();
                                }
                                $postData = $this->adminCartoDataService->curlPostData($datafiles, $files);

                                if (array_key_exists("error", $postData)) {
                                    if (count($files) === 1 && pathinfo(
                                            $files[0]->getFilePath(),
                                            PATHINFO_EXTENSION
                                        ) === 'csv') {
                                        if (str_contains(
                                            $postData["error"],
                                            "la colonne Â« the_geom Â» est spÃ©cifiÃ©e plus d'une fois"
                                        )) {
                                            return new JsonLDResponse(
                                                ["error" => "Un csv ne doit pas contenir de colonne 'the_geom'"],
                                                $postData['status'],
                                                [],
                                                false,
                                                "Data"
                                            );
                                        }
                                    }
                                    return new JsonLDResponse(["error" => $postData['error']],
                                        $postData['status'],
                                        [],
                                        false,
                                        "Data");
                                }
                                if (array_key_exists('Data', $postData['data']) && $postData['typeId'] === 1) {
                                    if (preg_match(
                                            '/TMP_(\d+).map/',
                                            $postData['data']['Data']['map'],
                                            $admincartoId
                                        ) === 0) {
                                        return new JsonLDResponse(["error" => "Update of map id failed"],
                                            422,
                                            [],
                                            false,
                                            "Data");
                                    }
                                    $data->setAdmincartoDataId((int)$admincartoId[1]);
                                } elseif (array_key_exists(
                                        'Data',
                                        $postData['data']
                                    ) && $postData['typeId'] === 3) {
                                    if (preg_match(
                                            '/\/api\/data\/(\d+)/',
                                            $postData['data']['@id'],
                                            $adminCartoId
                                        ) === 0) {
                                        return new JsonLDResponse(["error" => "Update of map id failed"],
                                            422,
                                            [],
                                            false,
                                            "Data");
                                    }
                                    $data->setAdmincartoDataId((int)$adminCartoId[1]);
                                } else {
                                    return new JsonLDResponse(["error" => "La création de la map a échoué"],
                                        422,
                                        [],
                                        false,
                                        "Data");
                                }
                            } else {
                                return new JsonLDResponse(
                                    ["error" => "Le status de la données ne lui permet pas d'etre initialisé"],
                                    422,
                                    [],
                                    false,
                                    "Data"
                                );
                            }
                            break;
                        case "api_data_patch_item":
                            $getDataRights = $this->adminService->controlsMetadataRight($data);
                            if (isset($getDataRights['traitement']) && $getDataRights['traitement'] === true) {
                                //patch data
                                if ($data->getStatus()->getId() === 4) {
                                    if (preg_match(
                                            '/\/api\/lex_layer_types\/(\d+)/',
                                            $data->getType(),
                                            $dataTypeId
                                        ) !== 0) {
                                        $dataType = (int)$dataTypeId[1];
                                    } else {
                                        return new JsonLDResponse(
                                            ["error" => "Le type de donnée n'a pas pu être récupéré"],
                                            400,
                                            [],
                                            false,
                                            "Data"
                                        );
                                    }

                                    if (empty(
                                    $this->entityManager->getUnitOfWork()->getOriginalEntityData(
                                        $data
                                    )
                                    )) {
                                        return new JsonLDResponse(
                                            ["error" => "La donnée n'existe pas"],
                                            404,
                                            [],
                                            false,
                                            "Data"
                                        );
                                    }

                                    switch ($this->entityManager->getUnitOfWork()->getOriginalEntityData($data)['status']->getId()) {
                                        case 3:
                                            if ($dataType === 1 && $data->getExtent() !== null) {
                                                $extent = $this->geonetworkService->getExtentForXml(
                                                    $data->getExtent(),
                                                    $data->getSourceSRS()
                                                );
                                            } else {
                                                $extent = null;
                                            }

                                            //Création du xml de la metadonnée
                                            $metadata = $this->geonetworkService->XmlGeonetworkAction(
                                                $data,
                                                $user,
                                                "CREATE",
                                                $dataType,
                                                $extent
                                            );
                                            if ($metadata === false || (isset($metadata['status']) && $metadata['status'] !== 200)) {
                                                return new JsonLDResponse(
                                                    ["error" => 'la modification du xml a échoué'],
                                                    400,
                                                    [],
                                                    false,
                                                    "XML"
                                                );
                                            }
                                            $metadataUuid = $metadata['uuid'];
                                            $data->setMetadataUuid($metadataUuid);
                                            $metadataSheet = $this->adminService->postAdminMetadataSheet(
                                                $metadata['id'],
                                                $data->getSubdomains()
                                            );

                                            $metadataSheet = $this->adminService->getMetasheetId(
                                                $metadataSheet['data']['@id']
                                            );
                                            if (array_key_exists("error", $metadataSheet)) {
                                                return new JsonLDResponse(["error" => $metadataSheet["error"]],
                                                    $metadataSheet["status"],
                                                    [],
                                                    false,
                                                    "Data");
                                            }
                                            $data->setMetadataSheetId($metadataSheet['metadataSheetId']);
                                            $requestLayer = $this->adminService->createRequestAdminLayer(
                                                $data,
                                                $metadataSheet['metadataSheetId'],
                                                $data->getType()
                                            );

                                            $postLayer = $this->adminService->postLayerAdmin($requestLayer);

                                            if (array_key_exists("error", $postLayer)) {
                                                return new JsonLDResponse(["error" => $postLayer["error"]],
                                                    404,
                                                    [],
                                                    false,
                                                    "Data");
                                            }
                                            $this->entityManager->persist($data);
                                            $this->entityManager->flush();
                                            $this->entityManager->refresh($data);

                                            $wfsMetadataUuid = null;
                                            if ($data->getWms() === true) {
                                                $addWmsService = $this->geonetworkService->addServiceInMetadata(
                                                    null,
                                                    $data,
                                                    $this->parameterBag->get('PRODIGE_URL_DATACARTO')."/wms?service=WMS&request=GetCapabilities",
                                                    "OGC:WMS-1.1.1-http-get-map",
                                                    $data->getTable(),
                                                    "CREATE"
                                                );
                                                if (is_array($addWmsService) && array_key_exists("error", $addWmsService)) {
                                                    return new JsonLDResponse(["error" => $addWmsService['error']],
                                                        $addWmsService['status'],
                                                        [],
                                                        false,
                                                        "Data");
                                                }
                                                $addInWms = $this->geonetworkService->actionMetadataInWxSService(
                                                    $metadataUuid,
                                                    $data->getTitle(),
                                                    "add",
                                                    "wms"
                                                );
                                                if (array_key_exists('error', $addInWms)) {
                                                    return new JsonLDResponse(["error" => $addInWms['error']],
                                                        $addInWms['status'],
                                                        [],
                                                        false,
                                                        "Data");
                                                }
                                            }

                                            if ($data->getWfs() === true) {
                                                $createMetadataWfs = $this->geonetworkService->metadataWfsService(
                                                    $data,
                                                    "create"
                                                );

                                                if (array_key_exists('error', $createMetadataWfs)) {
                                                    return new JsonLDResponse(["error" => $createMetadataWfs['error']],
                                                        $createMetadataWfs['status'],
                                                        [],
                                                        false,
                                                        "Data");
                                                }
                                                $wfsMetadataUuid = $createMetadataWfs['uuid'];
                                                $addWfsService = $this->geonetworkService->addServiceInMetadata(
                                                    null,
                                                    $data,
                                                    $this->parameterBag->get('PRODIGE_URL_DATACARTO')."/wfs/".$wfsMetadataUuid."?service=WFS&request=GetCapabilities",
                                                    "OGC:WFS-1.0.0-http-get-capabilities",
                                                    $data->getTable(),
                                                    "CREATE"
                                                );
                                                if (is_array($addWfsService) && array_key_exists(
                                                        "error",
                                                        $addWfsService
                                                    )) {
                                                    return new JsonLDResponse(["error" => $addWfsService['error']],
                                                        $addWfsService['status'],
                                                        [],
                                                        false,
                                                        "Data");
                                                }
                                                $addWfsMetadataUuidService = $this->geonetworkService->addServiceInMetadata(
                                                    null,
                                                    $data,
                                                    $this->parameterBag->get(
                                                        'PRODIGE_URL_DATACARTO'
                                                    )."/wfs?service=WFS&request=GetCapabilities",
                                                    "OGC:WFS-1.0.0-http-get-capabilities",
                                                    $data->getTable(),
                                                    "CREATE"
                                                );
                                                if (is_array($addWfsMetadataUuidService) && array_key_exists(
                                                        "error",
                                                        $addWfsMetadataUuidService
                                                    )) {
                                                    return new JsonLDResponse(
                                                        ["error" => $addWfsMetadataUuidService['error']],
                                                        $addWfsMetadataUuidService['status'],
                                                        [],
                                                        false,
                                                        "Data"
                                                    );
                                                }
                                                $addInWfs = $this->geonetworkService->actionMetadataInWxSService(
                                                    $metadataUuid,
                                                    $data->getTitle(),
                                                    "add",
                                                    "wfs"
                                                );
                                                if (array_key_exists('error', $addInWfs)) {
                                                    return new JsonLDResponse(["error" => $addInWfs['error']],
                                                        $addInWfs['status'],
                                                        [],
                                                        false,
                                                        "Data");
                                                }

                                                $addConstraintInWfsMetadata = $this->geonetworkService->addServiceInMetadataServiceWfs(
                                                    $wfsMetadataUuid,
                                                    $metadataUuid,
                                                    $user
                                                );
                                                if (array_key_exists('error', $addConstraintInWfsMetadata)) {
                                                    return new JsonLDResponse(
                                                        ["error" => $addConstraintInWfsMetadata['error']],
                                                        $addConstraintInWfsMetadata['status'],
                                                        [],
                                                        false,
                                                        "Data"
                                                    );
                                                }
                                            }

                                            if ($data->getWms() || $data->getWfs()) {
                                                $requestPatch = $this->adminService->requestPatchAdminLayer($data, $dataType, $wfsMetadataUuid);
                                                $patchLayer = $this->adminService->patchAdminLayer($postLayer['data']['id'], $requestPatch);
                                                if (array_key_exists('error', $patchLayer)) {
                                                    return new JsonLDResponse(["error" => $patchLayer['error']],
                                                        $patchLayer['status'],
                                                        [],
                                                        false,
                                                        "Data");
                                                }
                                            }

                                            if ($dataType === 1 && $data->getOpenData() === true) {
                                                $addDownloadService = $this->geonetworkService->addServiceInMetadata(
                                                    null,
                                                    $data,
                                                    $this->parameterBag->get(
                                                        'PRODIGE_URL_TELECHARGEMENT'
                                                    )."/download/".$data->getMetadataUuid(),
                                                    "WWW:DOWNLOAD-1.0-http--download",
                                                    null,
                                                    "CREATE"
                                                );
                                                if (is_array($addDownloadService) && array_key_exists("error", $addDownloadService)) {
                                                    return new JsonLDResponse(["error" => $addDownloadService['error']],
                                                        $addDownloadService['status'],
                                                        [],
                                                        false,
                                                        "Data");
                                                }
                                                $addLinkService = $this->geonetworkService->addServiceInMetadata(
                                                    null,
                                                    $data,
                                                    $this->parameterBag->get(
                                                        'PRODIGE_URL_CATALOGUE'
                                                    )."/rss/atomfeed/atomdataset/".$data->getMetadataUuid(),
                                                    "WWW:LINK-1.0-http--link",
                                                    "Téléchargement direct des données",
                                                    "CREATE"
                                                );
                                                if (is_array($addLinkService) && array_key_exists("error", $addLinkService)) {
                                                    return new JsonLDResponse(["error" => $addLinkService['error']],
                                                        $addLinkService['status'],
                                                        [],
                                                        false,
                                                        "Data");
                                                }
                                                $addWmsService = $this->geonetworkService->addServiceInMetadata(
                                                    null,
                                                    $data,
                                                    $this->parameterBag->get('PRODIGE_URL_DATACARTO')."/wms?service=WMS&request=GetCapabilities",
                                                    "OGC:WMS-1.1.1-http-get-map",
                                                    $data->getTable(),
                                                    "CREATE"
                                                );
                                                if (is_array($addWmsService) && array_key_exists("error", $addWmsService)) {
                                                    return new JsonLDResponse(["error" => $addWmsService['error']],
                                                        $addWmsService['status'],
                                                        [],
                                                        false,
                                                        "Data");
                                                }
                                                $addInWms = $this->geonetworkService->actionMetadataInWxSService(
                                                    $metadataUuid,
                                                    $data->getTitle(),
                                                    "add",
                                                    "wms"
                                                );
                                                if (array_key_exists('error', $addInWms)) {
                                                    return new JsonLDResponse(["error" => $addInWms['error']],
                                                        $addInWms['status'],
                                                        [],
                                                        false,
                                                        "Data");
                                                }
                                                //mis en commentaire en attendant la sauvegarde de l'uuid dans admin
                                                $createMetadataWfs = $this->geonetworkService->metadataWfsService(
                                                    $data,
                                                    "create"
                                                );

                                                if (array_key_exists('error', $createMetadataWfs)) {
                                                    return new JsonLDResponse(["error" => $createMetadataWfs['error']],
                                                        $createMetadataWfs['status'],
                                                        [],
                                                        false,
                                                        "Data");
                                                }
                                                $wfsMetadataUuid = $createMetadataWfs['uuid'];
                                                $addWfsService = $this->geonetworkService->addServiceInMetadata(
                                                    null,
                                                    $data,
                                                    $this->parameterBag->get('PRODIGE_URL_DATACARTO')."/wfs/".$wfsMetadataUuid."?service=WFS&request=GetCapabilities",
                                                    "OGC:WFS-1.0.0-http-get-capabilities",
                                                    $data->getTable(),
                                                    "CREATE"
                                                );
                                                if (is_array($addWfsService) && array_key_exists(
                                                        "error",
                                                        $addWfsService
                                                    )) {
                                                    return new JsonLDResponse(["error" => $addWfsService['error']],
                                                        $addWfsService['status'],
                                                        [],
                                                        false,
                                                        "Data");
                                                }
                                                $addWfsMetadataUuidService = $this->geonetworkService->addServiceInMetadata(
                                                    null,
                                                    $data,
                                                    $this->parameterBag->get(
                                                        'PRODIGE_URL_DATACARTO'
                                                    )."/wfs?service=WFS&request=GetCapabilities",
                                                    "OGC:WFS-1.0.0-http-get-capabilities",
                                                    $data->getTable(),
                                                    "CREATE"
                                                );
                                                if (is_array($addWfsMetadataUuidService) && array_key_exists(
                                                        "error",
                                                        $addWfsMetadataUuidService
                                                    )) {
                                                    return new JsonLDResponse(
                                                        ["error" => $addWfsMetadataUuidService['error']],
                                                        $addWfsMetadataUuidService['status'],
                                                        [],
                                                        false,
                                                        "Data"
                                                    );
                                                }
                                                $addInWfs = $this->geonetworkService->actionMetadataInWxSService(
                                                    $metadataUuid,
                                                    $data->getTitle(),
                                                    "add",
                                                    "wfs"
                                                );
                                                if (array_key_exists('error', $addInWfs)) {
                                                    return new JsonLDResponse(["error" => $addInWfs['error']],
                                                        $addInWfs['status'],
                                                        [],
                                                        false,
                                                        "Data");
                                                }

                                                $addConstraintInWfsMetadata = $this->geonetworkService->addServiceInMetadataServiceWfs(
                                                    $wfsMetadataUuid,
                                                    $metadataUuid,
                                                    $user
                                                );
                                                if (array_key_exists('error', $addConstraintInWfsMetadata)) {
                                                    return new JsonLDResponse(
                                                        ["error" => $addConstraintInWfsMetadata['error']],
                                                        $addConstraintInWfsMetadata['status'],
                                                        [],
                                                        false,
                                                        "Data"
                                                    );
                                                }
                                                $publishWfsXml = $this->geonetworkService->publishXml($wfsMetadataUuid);

                                                $requestPatch = $this->adminService->requestPatchAdminLayer($data, $dataType, $wfsMetadataUuid);
                                                $patchLayer = $this->adminService->patchAdminLayer($postLayer['data']['id'], $requestPatch);
                                                if (array_key_exists('error', $patchLayer)) {
                                                    return new JsonLDResponse(["error" => $patchLayer['error']],
                                                        $patchLayer['status'],
                                                        [],
                                                        false,
                                                        "Data");
                                                }
                                            }
                                            $createCatalogue = $this->featureCatalogueService->catalogueAttributeAction(
                                                $data,
                                                "create"
                                            );

                                            if (array_key_exists("error", $createCatalogue)) {
                                                return new JsonLDResponse(["error" => $createCatalogue['error']],
                                                    $createCatalogue['status'],
                                                    [],
                                                    false,
                                                    "Data");
                                            }

                                            $publishCatalogueXml = $this->geonetworkService->publishXml(
                                                $createCatalogue['uuid']
                                            );

                                            if (in_array($dataType, [1, 3], true) && $data->getOpenData() === true) {
                                                $addInSimpleDownload = $this->geonetworkService->actionMetadataInSimpleDownloadService(
                                                    $metadataUuid,
                                                    "add"
                                                );

                                                if (array_key_exists('error', $addInSimpleDownload)) {
                                                    return new JsonLDResponse(
                                                        ["error" => $addInSimpleDownload['error']],
                                                        $addInSimpleDownload['status'],
                                                        [],
                                                        false,
                                                        "Data"
                                                    );
                                                }
                                            }
                                            $apiFields = $this->adminService->apiField($data);
                                            if (array_key_exists("error", $apiFields)) {
                                                return new JsonLDResponse(["error" => $apiFields['error']],
                                                    $apiFields['status'],
                                                    [],
                                                    false,
                                                    "Data");
                                            }
                                            break;
                                        case 4:
                                            /** @var Data $data */
                                            $getDataType = $this->adminService->dataType($data);
                                            if (!array_key_exists('data', $getDataType)) {
                                                return new JsonLDResponse(["error" => $getDataType['error']],
                                                    $getDataType['status'],
                                                    [],
                                                    false,
                                                    "Data");
                                            }
                                            $dataType = $getDataType['data'];
                                            $layer = $this->adminService->getAdminLayerIdByTableName($data->getTable());
                                            if ($dataType === 1) {
                                                $wfsMetadataUuid = $this->adminService->getMetadataWfsUuid($layer['layer']['id'])['wfsMetadataUuid'];
                                            } else {
                                                $wfsMetadataUuid = null;
                                            }

                                            $openData = $this->adminService->isOpenData($layer['layer']['id']);
                                            //change le nom dans les xxml
                                            if ($dataType === 1 && $openData['isOpenData'] && ($data->getLayerName() !== $data->getTitle())) {
                                                $changeName = $this->geonetworkService->changeLayerName($data);
                                                if (array_key_exists("error", $changeName)) {
                                                    return new JsonLDResponse(["error" => $changeName['error']],
                                                        $changeName['status'],
                                                        [],
                                                        false,
                                                        "Data");
                                                }
                                            }

                                            $requestPatch = $this->adminService->requestPatchAdminLayer(
                                                $data,
                                                $dataType,
                                                $wfsMetadataUuid
                                            );
                                            if (array_key_exists("error", $layer)) {
                                                return new JsonLDResponse(["error" => $layer['error']],
                                                    $layer['status'],
                                                    [],
                                                    false,
                                                    "Data");
                                            }
                                            $patchLayer = $this->adminService->patchAdminLayer(
                                                $layer['layer']['id'],
                                                $requestPatch
                                            );
                                            if (array_key_exists("error", $patchLayer)) {
                                                return new JsonLDResponse(["error" => $patchLayer['error']],
                                                    $patchLayer['status'],
                                                    [],
                                                    false,
                                                    "Data");
                                            }

                                            if ($dataType === 1 && $data->getExtent() !== null) {
                                                $extent = $this->geonetworkService->getExtentForXml(
                                                    $data->getExtent(),
                                                    $data->getSourceSRS()
                                                );
                                            } else {
                                                $extent = $data->getExtent();
                                            }

                                            $metadata = $this->geonetworkService->XmlGeonetworkAction(
                                                $data,
                                                $user,
                                                "UPDATE",
                                                $dataType,
                                                $extent
                                            );
                                            if ($metadata === false || $metadata['status'] !== 200) {
                                                return new JsonLDResponse(
                                                    ["error" => 'la modification du xml a échoué'],
                                                    400,
                                                    [],
                                                    false,
                                                    "XML"
                                                );
                                            }
                                            $metadataUuid = $data->getMetadataUuid();

                                            if ($openData['isOpenData'] === true & $dataType === 1) {
                                                $layerNameChecking = $this->geonetworkService->layerNameInWfsMetadata($data);
                                                if (array_key_exists('error', $layerNameChecking)) {
                                                    return new JsonLDResponse(["error" => $layerNameChecking['error']],
                                                        $layerNameChecking['status'],
                                                        [],
                                                        false,
                                                        "Data");
                                                }
                                            }

                                            if ($openData['isOpenData'] === false && $data->getOpenData() === true) {
                                                $response = $this->geonetworkService->actionMetadataInSimpleDownloadService(
                                                    $metadataUuid,
                                                    "add"
                                                );
                                                if (array_key_exists('error', $response)) {
                                                    return new JsonLDResponse(["error" => $response['error']],
                                                        $response['status'],
                                                        [],
                                                        false,
                                                        "Data");
                                                }
                                            } elseif ($openData['isOpenData'] === true && $data->getOpenData() === false) {
                                                $response = $this->geonetworkService->actionMetadataInSimpleDownloadService(
                                                    $metadataUuid,
                                                    "remove"
                                                );
                                            }

                                            // etape supplementaire pour les données vectorielle
                                            if ($dataType === 1 && (!$openData['isOpenData'] && $data->getOpenData() === true)) {
                                                $addInWms = $this->geonetworkService->actionMetadataInWxSService(
                                                    $metadataUuid,
                                                    $data->getTable(),
                                                    "add",
                                                    "wms"
                                                );
                                                if (array_key_exists('error', $addInWms)) {
                                                    return new JsonLDResponse(["error" => $addInWms['error']],
                                                        $addInWms['status'],
                                                        [],
                                                        false,
                                                        "Data");
                                                }
                                                $createMetadataWfs = $this->geonetworkService->metadataWfsService(
                                                    $data,
                                                    "create"
                                                );

                                                if (array_key_exists('error', $createMetadataWfs)) {
                                                    return new JsonLDResponse(["error" => $createMetadataWfs['error']],
                                                        $createMetadataWfs['status'],
                                                        [],
                                                        false,
                                                        "Data");
                                                }
                                                $wfsMetadataUuid = $createMetadataWfs['uuid'];
                                                $data->setWfsMetadataUuid($wfsMetadataUuid);
                                                $addInWfs = $this->geonetworkService->actionMetadataInWxSService(
                                                    $data->getWfsMetadataUuid(),
                                                    $data->getTable(),
                                                    "add",
                                                    "wfs"
                                                );
                                                if (array_key_exists('error', $addInWfs)) {
                                                    return new JsonLDResponse(["error" => $addInWfs['error']],
                                                        $addInWfs['status'],
                                                        [],
                                                        false,
                                                        "Data");
                                                }
                                                $addInSimpleDownload = $this->geonetworkService->actionMetadataInSimpleDownloadService(
                                                    $metadataUuid,
                                                    "add"
                                                );
                                                if (array_key_exists('error', $addInSimpleDownload)) {
                                                    return new JsonLDResponse(
                                                        ["error" => $addInSimpleDownload['error']],
                                                        $addInSimpleDownload['status'],
                                                        [],
                                                        false,
                                                        "Data"
                                                    );
                                                }
                                                $createMetadataWfs = $this->geonetworkService->metadataWfsService(
                                                    $data,
                                                    "create"
                                                );
                                                if (array_key_exists('error', $createMetadataWfs)) {
                                                    return new JsonLDResponse(["error" => $createMetadataWfs['error']],
                                                        $createMetadataWfs['status'],
                                                        [],
                                                        false,
                                                        "Data");
                                                }
                                                $wfsMetadataUuid = $createMetadataWfs['uuid'];

                                                $addConstraintInWfsMetadata = $this->geonetworkService->addServiceInMetadataServiceWfs(
                                                    $wfsMetadataUuid,
                                                    $metadataUuid,
                                                    $user
                                                );
                                                if (array_key_exists('error', $addConstraintInWfsMetadata)) {
                                                    return new JsonLDResponse(
                                                        ["error" => $addConstraintInWfsMetadata['error']],
                                                        $addConstraintInWfsMetadata['status'],
                                                        [],
                                                        false,
                                                        "Data"
                                                    );
                                                }
                                                $requestPatch = $this->adminService->requestPatchAdminLayer(
                                                    $data,
                                                    $dataType,
                                                    $wfsMetadataUuid
                                                );
                                                $patchLayer = $this->adminService->patchAdminLayer(
                                                    $layer['layer']['id'],
                                                    $requestPatch
                                                );
                                                if (array_key_exists('error', $patchLayer)) {
                                                    return new JsonLDResponse(["error" => $patchLayer['error']],
                                                        $patchLayer['status'],
                                                        [],
                                                        false,
                                                        "Data");
                                                }
                                            } elseif ($dataType === 1 && ($openData['isOpenData'] && $data->getOpenData() === false)) {
                                                $removeInWfs = $this->geonetworkService->actionMetadataInWxSService(
                                                    $metadataUuid,
                                                    $data->getTable(),
                                                    "remove",
                                                    "wfs"
                                                );
                                                $removeWfsInWfs = $this->geonetworkService->actionMetadataInWxSService(
                                                    $metadataUuid,
                                                    $data->getTable(),
                                                    "remove",
                                                    "wfs"
                                                );
                                                $removeInWms = $this->geonetworkService->actionMetadataInWxSService(
                                                    $metadataUuid,
                                                    $data->getTable(),
                                                    "remove",
                                                    "wms"
                                                );
                                                //mis en commentaire en attendant la sauvegarde de l'uuid dans admin
                                                $deleteMetadata = $this->geonetworkService->metadataWfsService(
                                                    $data,
                                                    "remove"
                                                );

                                                if (array_key_exists('error', $deleteMetadata)) {
                                                    return new JsonLDResponse(["error" => $deleteMetadata['error']],
                                                        $deleteMetadata['status'],
                                                        [],
                                                        false,
                                                        "Data");
                                                }
                                                // supprimer l'uuid dans admin
                                                $requestPatch = $this->adminService->requestPatchAdminLayer(
                                                    $data,
                                                    $dataType
                                                );
                                                $patchLayer = $this->adminService->patchAdminLayer(
                                                    $layer['layer']['id'],
                                                    $requestPatch
                                                );
                                            }
                                            $updateCatalogue = $this->featureCatalogueService->catalogueAttributeAction(
                                                $data,
                                                "update"
                                            );
                                            if (array_key_exists("error", $updateCatalogue)) {
                                                return new JsonLDResponse(["error" => $updateCatalogue['error']],
                                                    $updateCatalogue['status'],
                                                    [],
                                                    false,
                                                    "Data");
                                            }
                                            $apiFields = $this->adminService->apiField($data);
                                            if (array_key_exists("error", $apiFields)) {
                                                return new JsonLDResponse(["error" => $apiFields['error']],
                                                    $apiFields['status'],
                                                    [],
                                                    false,
                                                    "Data");
                                            }
                                            break;
                                    }


                                    $requestPatch = $this->adminCartoDataService->createRequestCurlPatch(
                                        $data,
                                        $data->getMetadataUuid(),
                                        $dataType
                                    );

                                    $patchData = $this->adminCartoDataService->curlPatchData(
                                        $data->getAdmincartoDataId(),
                                        $requestPatch
                                    );
                                    if (array_key_exists("error", $patchData)) {
                                        return new JsonLDResponse(["error" => $patchData['error']],
                                            $patchData['status'],
                                            [],
                                            false,
                                            "Data");
                                    }
                                }

                                $patchSubdomains = $this->adminService->updateAdminSubdomain(
                                    $data->getMetadataSheetId(),
                                    $data->getSubdomains()
                                );

                                if (array_key_exists("error", $patchSubdomains)) {
                                    return new JsonLDResponse(["error" => $patchSubdomains['error']],
                                        $patchSubdomains['status'],
                                        [],
                                        false,
                                        "Data");
                                }

                                $updateSdom = $this->geonetworkService->updateDomSDomMetadata($data);
                                if (array_key_exists("error", $updateSdom)) {
                                    return new JsonLDResponse(["error" => $updateSdom['error']],
                                        $updateSdom['status'],
                                        [],
                                        false,
                                        "Data");
                                }

                                $publishXml = $this->geonetworkService->publishXml($data->getMetadataUuid());
                            } else {
                                return new JsonLDResponse(
                                    ["error" => 'Choississez au moins un sous domaines sur lequel vous avez des droits'],
                                    401,
                                    [],
                                    false,
                                    "Data"
                                );
                            }

                            if ($data->getOpenData() === true) {
                                $this->geonetworkService->addContactForOpenData($data->getMetadataUuid(), $user, $data);
                            }
                            break;
                        case "api_data_recreate_data_sets_item":
                            $getDataRights = $this->adminService->controlsMetadataRight($data);
                            if (isset($getDataRights['traitement']) && $getDataRights['traitement'] === true) {
                                //update datasets
                                $files = $this->entityManager->getRepository(DataFile::class)->findBy(['data' => $data]
                                );

                                if (preg_match(
                                        '/\/api\/lex_layer_types\/(\d+)/',
                                        $data->getType(),
                                        $dataTypeId
                                    ) !== 0) {
                                    $dataType = (int)$dataTypeId[1];
                                } else {
                                    foreach ($files as $file) {
                                        $this->entityManager->remove($file);
                                    }

                                    return new JsonLDResponse(
                                        ["error" => "Le type de donnée envoyée n'a pas été renseigné"],
                                        400,
                                        [],
                                        false,
                                        "Data"
                                    );
                                }
                                $recreateData = $this->adminCartoDataService->curlRecreateDataSet($data, $files);
                                if (array_key_exists("error", $recreateData)) {
                                    return new JsonLDResponse(["error" => $recreateData['error']],
                                        $recreateData['status'],
                                        [],
                                        false,
                                        "Data");
                                }

                                $data->setFields($recreateData['data']['Data']['fields']);
                                $response = $this->featureCatalogueService->catalogueAttributeAction($data, "recreate");
                                if (array_key_exists("error", $response)) {
                                    return new JsonLDResponse(["error" => $response['error']],
                                        $response['status'],
                                        [],
                                        false,
                                        "Data");
                                }
                                $status = $this->entityManager->getRepository(Status::class)->findOneBy(['id' => 4]);
                                $data->setStatus($status);
                            } else {
                                return new JsonLDResponse(
                                    ["error" => 'Vos droits ne vous permettent pas de modifier cette métadonnée'],
                                    401,
                                    [],
                                    false,
                                    "Data"
                                );
                            }
                            break;
                        case "api_data_featureCatalogue_item":
                            $getDataRights = $this->adminService->controlsMetadataRight($data);
                            if (isset($getDataRights['traitement']) && $getDataRights['traitement'] === true) {
                                $response = $this->featureCatalogueService->catalogueAttributeAction($data, "update");
                                if (array_key_exists("error", $response)) {
                                    return new JsonLDResponse(["error" => $response['error']],
                                        $response['status'],
                                        [],
                                        false,
                                        "Data");
                                }
                            } else {
                                return new JsonLDResponse(
                                    ["error" => 'Vos droits ne vous permettent pas de modifier cette métadonnée'],
                                    401,
                                    [],
                                    false,
                                    "Data"
                                );
                            }
                            break;
                    }
                }

                $this->entityManager->persist($data);
                $this->entityManager->flush();
                $this->entityManager->refresh($data); // Initialize Collection values

                if ($data->getAdmincartoDataId() !== null) {
                    return $this->dataProvider->getItem(Data::class, $data->getId());
                }

                return $data;
            }
        }

        return new JsonLDResponse(["error" => 'Unauthorized user to add data'], 401, [], false, "Data");
    }

    /**
     * @throws Exception
     */
    public function remove($data, array $context = []): JsonLDResponse
    {
        ##TODO:verifyRigth
        /** @var Data $data */
        if ($data !== null) {
            $user = $this->security->getUser()->getUsername();
            $this->logger->info('get user', ['user' => $user]);
            if ($user !== $this->parameterBag->get('PHPCLI_DEFAULT_LOGIN')) {
                $getDataRights = $this->adminService->controlsMetadataRight($data);
            } else {
                $getDataRights['traitement'] = true;
            }
            if ($getDataRights['traitement'] === true) {
                if ($data->getAdmincartoDataId() !== null) {
                    // retirer les réferences des services si elle y est et supprimer son catalogue d'attribut et sa carte wfs_

                    if ($user !== $this->parameterBag->get('PHPCLI_DEFAULT_LOGIN')) {
                        $deleteReference = $this->geonetworkService->deleteReference($data);
                        if (array_key_exists("error", $deleteReference)) {
                            return new JsonLDResponse(["error" => $deleteReference['error']],
                                $deleteReference['status'],
                                [],
                                false,
                                "Data");
                        }
                        $deleteMetadata = $this->geonetworkService->deletingAssociatedFiles($data);
                        $removedAdminCarto = $this->adminCartoDataService->curlDeleteData($data->getAdmincartoDataId());
                        if (array_key_exists("error", $removedAdminCarto)) {
                            return new JsonLDResponse(["error" => $removedAdminCarto['error']],
                                $removedAdminCarto['status'],
                                [],
                                false,
                                "Data");
                        }
                        if ($data->getMetadataSheetId() !== null) {
                            $removedAdmin = $this->adminService->curlDeleteMetadataSheet($data->getMetadataSheetId());
                            if (array_key_exists("error", $removedAdmin)) {
                                return new JsonLDResponse(["error" => $removedAdmin['error']],
                                    $removedAdmin['status'],
                                    [],
                                    false,
                                    "Data");
                            }
                        }
                        $MetadataDeletion = $this->geonetworkService->deleteMetadata($data->getMetadataUuid());
                        if (array_key_exists("error", $MetadataDeletion)) {
                            return new JsonLDResponse(["error" => $MetadataDeletion['error']],
                                $MetadataDeletion['status'],
                                [],
                                false,
                                "Data");
                        }
                    }
                }
                $this->logger->info('get data to delete', ['data' => $data]);

                $this->entityManager->remove($data);
                $this->entityManager->flush();

                return new JsonLDResponse(["data" => 'delete success'],
                    204,
                    [],
                    false,
                    "Data");
            }
        }

        return new JsonLDResponse(["error" => 'Unauthorized user to delete this data'], 401, [], false, "Data");
    }
}
