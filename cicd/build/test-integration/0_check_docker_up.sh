#!/bin/sh
set -e

# Source env file
SCRIPT=`readlink -f $0`
SCRIPTDIR=`dirname $SCRIPT`
SCRIPTNAME=`basename $SCRIPT`
. $SCRIPTDIR/env.sh

echo "Waiting 30sec for the docker to be up"
sleep 30
! docker-compose --project-directory ${REMOTE_PROJECT} ps | grep exited
