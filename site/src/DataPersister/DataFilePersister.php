<?PHP

declare(strict_types=1);

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Entity\DataFile;
use App\Entity\Status;
use App\Response\JsonLDResponse;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Security\Core\Security;

final class DataFilePersister implements ContextAwareDataPersisterInterface
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private ParameterBagInterface $parameterBag,
        private Security $security
    ) {
    }

    public function supports($data, array $context = []): bool
    {
        return ($data instanceof DataFile);
    }

    public function persist($data, array $context = [])
    {
        if(get_class($data->getData()) === "App\Response\JsonLDResponse"){
            $content = json_decode($data->getData()->getContent());
            return new JsonLDResponse(["error" => $content->error], 401, [], false, "DataFiles");
        }
        if ($data->getData()->getUserId() === $this->security->getUser()->getId()) {
            $ifExist = $this->entityManager->getRepository(DataFile::class)->findOneBy(['data' => $data->getData()]);
            if ($ifExist === null) {
                $data->getData()->setStatus(
                    $this->entityManager->getRepository(Status::class)->findOneById(2)
                );
            }
            $this->entityManager->persist($data);
            $this->entityManager->flush();
            $this->entityManager->refresh($data);

            return $data;
        }
        return new JsonLDResponse(["error" => 'Unauthorized user to add data'], 401, [], false, "DataFiles");
    }

    public function remove($data, array $context = [])
    {
        $this->entityManager->remove($data);
        if (file_exists($this->parameterBag->get('file_dir_temp') . $data->getFilePath())) {
            unlink($this->parameterBag->get('file_dir_temp') . $data->getFilePath());
        }
        $this->entityManager->flush();
    }
}
