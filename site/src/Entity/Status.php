<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(
    collectionOperations: [
        'get'
    ],
    itemOperations: ['get'],
    normalizationContext: ['groups' => ['GetStatus']],
)]
#[ORM\Entity]
#[ORM\Table(name: 'status', schema: 'contrib')]
#[ORM\HasLifecycleCallbacks]
class Status
{
    /**
     * @var int
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private int $id;


    /**
     * @var string
     */
    #[Groups(['GetStatus'])]
    #[ORM\Column(name: 'status', type: 'string', length: 50, nullable: false)]
    #[Assert\Type('string')]
    private string $status;


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Status
     */
    public function setId(int $id): Status
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return Status
     */
    public function setStatus(string $status): Status
    {
        $this->status = $status;
        return $this;
    }




}