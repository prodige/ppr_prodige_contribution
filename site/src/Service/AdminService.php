<?php

namespace App\Service;

use App\Entity\Data;
use Exception;
use Prodige\ProdigeBundle\Services\CurlUtilities;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Prodige\ProdigeBundle\Services\AdminClientService;

class AdminService
{
    public function __construct(
        private ParameterBagInterface $parameterBag,
        private AdminCartoDataService $adminCartoDataService,
        private AdminClientService $adminClientService,
        private LoggerInterface $logger
    ) {
    }

    /**
     * @param int $uuid
     * @param array $subdomains
     * @return array
     * @throws Exception
     */
    public function postAdminMetadataSheet(int $uuid, array $subdomains): array
    {
        $this->logger->info('Post metadatasheet to admin');
        $url = $this->parameterBag->get('PRODIGE_URL_ADMIN').'/api/metadata_sheets';
        $iriSubdomains = [];
        foreach ($subdomains as $subdomain) {
            $iriSubdomains[] = "/api/subdomains/".$subdomain;
        }
        $request = ['publicMetadataId' => $uuid, 'subdomains' => $iriSubdomains];

        $responseJson = $this->adminClientService->curlAdminWithAdmincli($url, 'POST', $request);

        if ($responseJson === null) {
            $this->logger->error('curl metadatasheet error ', ['error' => 'connexion failed']);

            return ["error" => 'connexion failed', "status" => 409];
        }
        if (is_array($responseJson) && array_key_exists("error", $responseJson)) {
            return ["error" => $responseJson['error'], "status" => 409];
        }

        return ["data" => $responseJson, "status" => 200];
    }

    /**
     * @param Data $data
     * @param int $metadataSheetId
     * @param $type
     * @return array
     */
    public function createRequestAdminLayer(Data $data, int $metadataSheetId, $type): array
    {
        /** @var Data $data */
        $request = [
            "name" => $data->getTitle(),
            "description" => $data->getAbstract(),
            "storagePath" => $this->adminCartoDataService->formatTableName($data->getTable()),
            "wms" => false,
            "wfs" => false,
            "download" => $data->getOpenData(),
            "restriction" => false,
            "exclusiveRestriction" => false,
            "bufferRestriction" => 0,
            "viewable" => true,
            "assignedExtraction" => false,
            "fieldAssignedExtraction" => "",
            "lastImport" => date("Y-m-d H:i:s"),
            "sheetMetadata" => "api/metadata_sheets/".$metadataSheetId,
            "lexLayerType" => $type,
        ];

        if ($type === "/api/lex_layer_types/1") {
            $request['wms'] = $data->getOpenData();
            $request['wfs'] = $data->getOpenData();
        }

        return $request;
    }


    /**
     * @param array $request
     * @return array
     * @throws Exception
     */
    public function postLayerAdmin(array $request): array
    {
        $this->logger->info('Post layer to admin');
        $url = $this->parameterBag->get('PRODIGE_URL_ADMIN').'/api/layers';

        $responseJson = $this->adminClientService->curlAdminWithAdmincli($url, 'POST', $request);

        if ($responseJson === null) {
            $this->logger->error('curl postLayer error ', ['error' => 'admin ne répond pas']);

            return ["error" => "Erreur lors de l'enregistrement du layer", "status" => 409];
        }
        if (is_array($responseJson) && array_key_exists("error", $responseJson)) {
            return ["error" => $responseJson['error'], "status" => 404];
        }

        return ["data" => $responseJson, "status" => 200];
    }

    /**
     * @param Data $data
     * @param int $dataType
     * @param string|null $metadataWfsUuid
     * @return array
     */
    public function requestPatchAdminLayer(Data $data, int $dataType, string $metadataWfsUuid = null): array
    {
        $request = [
            'restriction' => false,
            'exclusiveRestriction' => false,
            'bufferRestriction' => 0,
        ];
        if ($data->getTitle()) {
            $request['name'] = $data->getTitle();
        }
        if ($data->getAbstract()) {
            $request['description'] = $data->getAbstract();
        }
        if ($data->getTable()) {
            $request['storagePath'] = $this->adminCartoDataService->formatTableName($data->getTable());
        }
        if ($data->getOpenData() !== null) {
            $request['wms'] = $data->getOpenData();
            $request['wfs'] = $data->getOpenData();
            $request['download'] = $data->getOpenData();
            $request['wfsMetadataUuid'] = $metadataWfsUuid;
        }

        if ($dataType === 1) {
            if ($data->getWms() !== null) {
                $request['wms'] = $data->getWFS();
            }
            if ($data->getWFS() !== null) {
                $request['wfs'] = $data->getWFS();
            }
        }

        return $request;
    }

    /**
     * @param string $metadataSheet
     * @return array
     */
    public function getMetasheetId(string $metadataSheet): array
    {
        if (preg_match('/\/api\/metadata_sheets\/(\d+)/', $metadataSheet, $metadataSheetId) !== false) {
            return ['metadataSheetId' => (int)$metadataSheetId[1], 'status' => 200];
        }

        return ["error" => "récupération de l'id impossible", 'status' => 404];
    }

    /**
     * @param int $layerId
     * @param array $request
     * @return array
     * @throws Exception
     */
    public function patchAdminLayer(int $layerId, array $request): array
    {
        $this->logger->info('Patch layer to admin');
        $url = $this->parameterBag->get('PRODIGE_URL_ADMIN').'/api/layers/'.$layerId;

        $responseJson = $this->adminClientService->curlAdminWithAdmincli($url, 'PATCH', $request);

        if ($responseJson === null) {
            $this->logger->error('curl layer patch error ', ['error' => "Le serveur d'admin ne répond pas"]);

            return ["error" => "Le serveur d'admin ne répond pas", "status" => 409];
        }
        if (is_array($responseJson) && array_key_exists("error", $responseJson)) {
            return ["error" => $responseJson["error"], 'status' => 404];
        }

        return ["data" => $responseJson, 'status' => 200];
    }

    /**
     * @param Data $data
     * @param array|null $dataFiles
     * @return array
     * @throws Exception
     */
    public function dataType(Data $data, array $dataFiles = null): array
    {
        $this->logger->info('Get subdomains from admin');
        if ($dataFiles) {
            $tableName = $dataFiles['Data']['table'];
        } else {
            $tableName = $data->getTable();
        }

        $layer = $this->getAdminLayerIdByTableName($tableName);

        if (array_key_exists('error', $layer)) {
            return ['error' => $layer['error'], 'status' => 404];
        }

        $layerId = $layer['layer']['id'];
        $url = $this->parameterBag->get('PRODIGE_URL_ADMIN').'/api/layers/'.$layerId;

        $layerJson = $this->adminClientService->curlAdminWithAdmincli($url, 'GET');

        if ($layerJson === null) {
            $this->logger->error('curl get layer error ', ['error' => $layer]);

            return ["error" => $layer, "status" => 409];
        }
        if ($layerJson["@type"] === "hydra:Error") {
            return ["error" => $layerJson["hydra:description"], "status" => 404];
        }

        if (array_key_exists("lexLayerType", $layerJson) && preg_match(
                '/\/api\/lex_layer_types\/(\d+)/',
                $layerJson["lexLayerType"]["@id"],
                $dataTypeId
            )) {
            return [
                "data" => (int)$dataTypeId[1],
                "uri" => $layerJson["lexLayerType"]["@id"],
                "type" => $layerJson["lexLayerType"]["name"],
                "status" => 200,
            ];
        } else {
            return ["error" => "récupération du type de donnée impossible", "status" => 404];
        }
    }

    /**
     * @param string $tableName
     * @return array
     * @throws Exception
     */
    public function getAdminLayerIdByTableName(string $tableName): array
    {
        $this->logger->info('Get layers from admin by table_name');
        $url = $this->parameterBag->get(
                'PRODIGE_URL_ADMIN'
            ).'/api/layers?page=1&itemsPage=30&storagePath='.$tableName;

        $layersJson = $this->adminClientService->curlAdminWithAdmincli($url, 'GET');

        if ($layersJson === null) {
            $this->logger->error('curl getLayer error ', ['error' => $layersJson]);

            return ["error" => $layersJson, "status" => 409];
        }

        if ($layersJson["@type"] === "hydra:Error") {
            return ["error" => $layersJson["hydra:description"], 'status' => 404];
        }

        if (array_key_exists(0, $layersJson['hydra:member'])) {
            if ($layersJson["@id"] === "/api/layers") {
                return ['layer' => $layersJson['hydra:member'][0], 'status' => 200];
            }
        }

        return ["error" => "récupération de l'id impossible", 'status' => 404];
    }

    /**
     * @param int $metadatasheetId
     * @param array $subdomains
     * @return array
     * @throws Exception
     */
    public function updateAdminSubdomain(int $metadatasheetId, array $subdomains): array
    {
        $this->logger->info('Put subdomain to admin');
        $url = $this->parameterBag->get('PRODIGE_URL_ADMIN').'/api/metadata_sheets/'.$metadatasheetId;
        $iriSubdomains = [];
        foreach ($subdomains as $subdomain) {
            $iriSubdomains[] = "/api/subdomains/".$subdomain;
        }
        $request = ['subdomains' => $iriSubdomains];

        $responseJson = $this->adminClientService->curlAdminWithAdmincli($url, 'PATCH', $request);

        if ($responseJson === null) {
            $this->logger->error('curl patch subdomain error ', ['error' => "Le serveur d'admin ne répond pas"]);

            return ["error" => "Le serveur d'admin ne répond pas", "status" => 409];
        }
        if (is_array($responseJson) && array_key_exists("error", $responseJson)) {
            return ["error" => $responseJson["error"], "status" => 404];
        }

        return ["data" => $responseJson, "status" => 200];
    }

    /**
     * @param int $metadataSheetId
     * @return array
     * @throws Exception
     */
    public function getSubdomainsById(int $metadataSheetId): array
    {
        $subdomains = [];
        $this->logger->info('Get subdomains from admin');
        $url = $this->parameterBag->get('PRODIGE_URL_ADMIN').'/api/metadata_sheets/'.$metadataSheetId;

        $metadataSheetJson = $this->adminClientService->curlAdminWithAdmincli($url, 'GET');

        if ($metadataSheetJson === null) {
            $this->logger->error('curl get subdomains error ', ['error' => "Le serveur d'admin ne répond pas"]);

            return ["error" => "Le serveur d'admin ne répond pas", "status" => 409];
        }
        if ($metadataSheetJson["@type"] === "hydra:Error") {
            return ["error" => $metadataSheetJson["hydra:description"], "status" => 404];
        }
        foreach ($metadataSheetJson["subdomains"] as $subdomain) {
            $subdomains[] = $subdomain['id'];
        }

        return ["data" => $subdomains, "status" => 200];
    }

    /**
     * @param int $layerId
     * @return array
     * @throws Exception
     */
    public function getMetadataWfsUuid(int $layerId): array
    {
        $layer = $this->getLayerInfo($layerId);
        if (array_key_exists('wfsMetadataUuid', $layer['data'])) {
            return ['wfsMetadataUuid' => $layer['data']['wfsMetadataUuid'], "status" => 200];
        }

        return ['wfsMetadataUuid' => null, "status" => 200];
    }

    /**
     * @param int $layerId
     * @return array
     * @throws Exception
     */
    public function getLayerInfo(int $layerId): array
    {
        $this->logger->info('Get layer from admin');
        $url = $this->parameterBag->get('PRODIGE_URL_ADMIN').'/api/layers/'.$layerId;

        $layerJson = $this->adminClientService->curlAdminWithAdmincli($url, 'GET');

        if ($layerJson === null) {
            $this->logger->error('curl get layer error ', ['error' => 'serveur injoignable']);

            return ["error" => 'serveur injoignable', "status" => 409];
        }
        if ($layerJson["@type"] === "hydra:Error") {
            return ["error" => $layerJson["hydra:description"], "status" => 404];
        }

        return ["data" => $layerJson, "status" => 200];
    }

    /**
     * @param array $layerJson
     * @return array
     */
    public function getWmsWfs(array $layerJson): array
    {
        return [
            'openData' => $layerJson['wms'],
        ];
    }

    /**
     * @param int $metadataSheetId
     * @return array
     * @throws Exception
     */
    public function curlDeleteMetadataSheet(int $metadataSheetId): array
    {
        $this->logger->info('Delete vers admin');
        $url = $this->parameterBag->get('PRODIGE_URL_ADMIN').'/api/metadata_sheets/'.$metadataSheetId;

        $deleteInfoJson = $this->adminClientService->curlAdminWithAdmincli($url, 'DELETE');

        if ($deleteInfoJson) {
            return ["data" => "The data ".$metadataSheetId." has been deleted", "status" => 200];
        }

        $this->logger->error('curl delete Data error ', ['error' => $deleteInfoJson]);

        return ["error" => 'erreur lors de la suppression de la métadonnée', "status" => 409];
    }

    /**
     * @param int $layerId
     * @return array
     * @throws Exception
     */
    public function isOpenData(int $layerId): array
    {
        $layer = $this->getLayerInfo($layerId);

        if (($layer['data']['lexLayerType']['@id'] === '/api/lex_layer_types/1') && ($layer['data']['wms'] && $layer['data']['wfs'] === true)) {
            return ['isOpenData' => true, 'status' => 200];
        }
        if (($layer['data']['lexLayerType']['@id'] === '/api/lex_layer_types/3') && $layer['data']['download'] === true) {
            return ['isOpenData' => true, 'status' => 200];
        }

        return ['isOpenData' => false, 'status' => 200];
    }

    public function controlsMetadataRight(Data $data)
    {
        $this->logger->info('Get right traitement from admin');

        $traitementRight = $this->adminClientService->verifyRights("GLOBAL_RIGHTS");

        if ($traitementRight === null) {
            $this->logger->error('curl getUser error ', ['error' => 'Admin ne répond pas']);

            return ["error" => 'Admin ne répond pas', "status" => 409];
        }

        if ($traitementRight === false) {
            return ["error" => 'Traitement not found', 'status' => 404];
        }
        if ($data->getSubdomains() !== null) {
            if (isset($traitementRight['right']['MANAGED_SUBDOMAINS'])) {
                foreach ($traitementRight['right']['MANAGED_SUBDOMAINS'] as $acceptedDomain) {
                    if (in_array($acceptedDomain['id'], $data->getSubdomains(), true)) {
                        return ["traitement" => true, 'status' => 200];
                    }
                }
            }
        }

        return ["traitement" => false, 'status' => 200];
    }

    /**
     * @param mixed $organization
     * @return array
     */
    public function getStructureUuid(mixed $organization): array
    {
        $this->logger->info('Get Structure from admin by name');
        $url = $this->parameterBag->get(
                'PRODIGE_URL_ADMIN'
            ).'/api/structures?name='.urlencode($organization);

        $structure = $this->adminClientService->curlAdminWithAdmincli($url, 'GET');

        if (array_key_exists(0, $structure['hydra:member'])) {
            if ($structure["@id"] === "/api/structures") {
                return ['structureUuid' => $structure['hydra:member'][0]['referenceGn'], 'status' => 200];
            }
        }

        return ["error" => "récupération de l'uuid impossible", 'status' => 404];
    }

    /**
     * @param mixed $user
     * @return array
     */
    public function getStructureContact(mixed $user): array
    {
        $userInfo = $this->getUserFromAdmin($user->getId());

        if (array_key_exists('user', $userInfo)) {
            foreach ($userInfo['user']['structures'] as $struct) {
                $structureContact['mail'] = $struct['email'];
                $structureContact['organization'] = $struct['name'];
                $structureContact['role'] = null;
                $structureContact['type'] = "link";
            }
            if (!isset($structureContact)) {
                $structureContact = [];
            }

            return $structureContact;
        }

        return ['error' => 'User not found', 'status' => 404];
    }

    public function getUserFromAdmin(int $id): array
    {
        $this->logger->info('Get Users from admin by id');
        $url = $this->parameterBag->get(
                'PRODIGE_URL_ADMIN'
            ).'/api/users/'.$id;

        $user = $this->adminClientService->curlAdminWithAdmincli($url, 'GET');

        if ($user === null) {
            $this->logger->error('curl getUser error ', ['error' => 'Admin ne répond pas']);

            return ["error" => 'Admin ne répond pas', "status" => 409];
        }

        if ($user === false) {
            return ["error" => 'User not found', 'status' => 404];
        }

        $userInfo['id'] = $user['id'];
        $userInfo['name'] = $user['name'];
        $userInfo['firstName'] = $user['firstName'];
        $userInfo['email'] = $user['email'];
        $userInfo['referenceGn'] = $user['referenceGn'];
        $userInfo['structures'] = $user['structures'];

        return ["user" => $userInfo, 'status' => 200];
    }

    /**
     * @param Data $data
     * @return array
     */
    public function apiField(Data $data): array
    {
        $this->logger->info('Add API field');
        $url = $this->parameterBag->get(
                'PRODIGE_URL_ADMIN'
            ).'/api/layers?storagePath='.$data->getTable();

        $layer = $this->adminClientService->curlAdminWithAdmincli($url, 'GET');
        if ($layer !== false && array_key_exists('hydra:member', $layer)) {
            if (array_key_exists(0, $layer['hydra:member'])) {
                if ($layer["@id"] === "/api/layers") {
                    $layer = $layer['hydra:member'][0];
                    if ($data->getOpenData() === true) {
                        $urlField = $this->parameterBag->get(
                                'PRODIGE_URL_ADMIN'
                            ).'/api/layers/'.$layer['id'].'/fields';
                        $getFields = $this->adminClientService->curlAdminWithAdmincli($urlField, 'GET');

                        if ($getFields !== false && array_key_exists('dbFields', $getFields)) {
                            $fields = $getFields['dbFields'];

                            $request['apiLayer'] = true;
                            $apiFields = [];
                            foreach ($fields as $key => $value) {
                                if ($key !== 'the_geom') {
                                    $apiFields[] = $key;
                                }
                            }

                            $request['apiFields'] = $apiFields;
                        } else {
                            return ["error" => "getFields error", 'status' => 404];
                        }
                    } else {
                        $request['apiLayer'] = false;
                        $request['apiFields'] = null;
                    }

                    $url = $this->parameterBag->get(
                            'PRODIGE_URL_ADMIN'
                        ).'/api/layers/'.$layer['id'];

                    $patchLayer = $this->adminClientService->curlAdminWithAdmincli($url, 'PATCH', $request);

                    return $patchLayer;
                }
            }
        }

        return ["error" => "layer not found", 'status' => 404];
    }


}
