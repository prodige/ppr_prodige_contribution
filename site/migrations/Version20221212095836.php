<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221212095836 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE contrib.status (id SERIAL NOT NULL, status VARCHAR(50) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE contrib.data (id SERIAL NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, admincarto_data_id INT DEFAULT NULL, user_id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE contrib.data_file ADD data_id INT NOT NULL');
        $this->addSql('ALTER TABLE contrib.data_file DROP contribution_id');
        $this->addSql('DROP TABLE contrib.contribution');
        $this->addSql('ALTER TABLE contrib.data_file ADD CONSTRAINT FK_E5131CD237F5A13C FOREIGN KEY (data_id) REFERENCES contrib.data (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_E5131CD237F5A13C ON contrib.data_file (data_id)');
        $this->addSql("INSERT INTO contrib.status (id, status) VALUES (1,'created'),(2,'files completed'),(3,'datafields created'),(4,'creation completed')");
    }

    public function down(Schema $schema): void
    {
      //  // this down() migration is auto-generated, please modify it to your needs
        //$this->addSql('ALTER TABLE contrib.data DROP CONSTRAINT IDX_E5131CD237F5A13C');
        $this->addSql('ALTER TABLE contrib.data_file DROP CONSTRAINT FK_E5131CD237F5A13C');
        $this->addSql('Drop table contrib.status');
        $this->addSql('Drop table contrib.data');
    }
}
