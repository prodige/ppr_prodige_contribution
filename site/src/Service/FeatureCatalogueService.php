<?php

namespace App\Service;

use App\Entity\Data;
use Prodige\ProdigeBundle\Controller\BaseController;
use Prodige\ProdigeBundle\Services\GeonetworkInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class FeatureCatalogueService
{

    public function __construct(
        private ParameterBagInterface $parameterBag,
        private GeonetworkService $geonetworkService,
        private BaseController $baseController
    ) {
    }

    /**
     * @param Data $data
     * @param string $mode
     * @return array
     * @throws \DOMException
     */
    public function catalogueAttributeAction(Data $data, string $mode): array
    {
        $geonetwork = new GeonetworkInterface($this->parameterBag->get('PRO_GEONETWORK_URLBASE'), 'srv/fre/');
        switch ($mode) {
            case "create":
                $templateUuid = PRO_FEATURE_CATALOGUE_METADATA_UUID;
                $templateXml = $this->geonetworkService->getXmlInfo($geonetwork, $templateUuid,'create');
                if (array_key_exists('error', $templateXml)) {
                    return ["error" => $templateXml['error'], "status" => $templateXml['status']];
                }
                //on cree une nouvelle fiche
                $newCatalaogue = $this->geonetworkService->createXml($templateXml['data']);
                $newCatalogueUuid = $newCatalaogue['uuid'];
                //on la lie à la metadonnée
                $addLink = $geonetwork->linkFeatureCatalog($data->getMetadataUuid(), $newCatalogueUuid);
                //on la publie
                $published = $geonetwork->metadataPrivileges("update", $newCatalogueUuid, array("_1_0" => "on"));
                //on met à jours les données dans le nouveau catalogue
                $updateCatalogue = $this->updateCatalogue($data, $newCatalogueUuid, "update");
                break;
            case "update":
            case "recreate":
                $response = $geonetwork->getMetadataRelations($data->getMetadataUuid(), "fcats", "json");
                $ifExist = json_decode($response, JSON_OBJECT_AS_ARRAY)['fcats'];
                if ($ifExist === null) {
                    return ['error' => "il n'y a pas de catalogue associé", 'status' => 404];
                }

                foreach ($ifExist as $relation) {
                    if ($relation['mdType'] !== null) {
                        $catalogueUuid = $relation['id'];
                    }
                }
                if (!isset($catalogueUuid)) {
                    return [
                        'error' => "la récupération de l'identifiant du catalogues associés a échoué",
                        'status' => 404
                    ];
                }
                if ($mode === "update") {
                    $updateCatalogue = $this->updateCatalogue($data, $catalogueUuid, "replace");
                } else {
                    $updateCatalogue = $this->updateCatalogue($data, $catalogueUuid, "recreate");
                }
                break;
        }
        $this->baseController->getGeonetworkInterface(false);
        if (isset($updateCatalogue) && array_key_exists("error", $updateCatalogue)) {
            return ['error' => $updateCatalogue['error'], 'status' => $updateCatalogue['status']];
        }
        return ['data' => "Data mise à jour", "uuid" => $updateCatalogue['uuid'],"status" => 200];
    }

    /**
     * @param Data $data
     * @param string $catalogueUuid
     * @param string $mode
     * @return array|string[]
     * @throws \DOMException
     */
    public function updateCatalogue(Data $data, string $catalogueUuid, string $mode): array
    {
        $geonetwork = new GeonetworkInterface($this->parameterBag->get('PRO_GEONETWORK_URLBASE'), 'srv/fre/');
        $catalogueXml = $this->geonetworkService->getXmlInfo($geonetwork, $catalogueUuid);
        $tabChampsTableToAdd = $data->getFields();
        $version = "1.0";
        $encoding = "UTF-8";
        $doc = new \DOMDocument($version, $encoding);
        if ($doc->loadXML($catalogueXml['data'])) {
            $xpath = new \DOMXPath($doc);

            if ($data->getTitle()) {
                $fieldOfApplication = $xpath->query("//gfc:fieldOfApplication/gco:CharacterString");
                $fieldOfApplication->item(0)->nodeValue = $data->getTitle();
                $fcat_MD_DataIdentification = $data->getTitle() . " ( Catalogue d'attributs )";
            } else {
                $fcat_MD_DataIdentification = "Catalogue d'attributs - Métadonnée " . $catalogueUuid;
            }

            // modifie la date du catalogue
            $versionDate = $doc->getElementsByTagName('versionDate')->item(0);
            $Date = $versionDate->getElementsByTagName('Date')->item(0);
            $Date || ($Date = $versionDate->getElementsByTagName('DateTime')->item(0));
            $Date->nodeValue = date('Y-m-d') . "T" . date('G:i:s');

            // modifie le nom du catalogue
            $name = $doc->getElementsByTagName('name')->item(0);
            $name = $name->getElementsByTagName('CharacterString')->item(0);
            $name->nodeValue = $fcat_MD_DataIdentification;

            // modifie le nom de la propriété
            if ($data->getTable()) {
                $FC_FeatureType = $doc->getElementsByTagName('FC_FeatureType')->item(0);
                $typeName = $FC_FeatureType->getElementsByTagName('typeName')->item(0);
                $LocalName = $typeName->getElementsByTagName('LocalName')->item(0);
                $LocalName->nodeValue = "Table";
            }
            switch ($mode) {
                case "replace" :
                    // recupere toutes les balises de champ
                    $FC_FeatureType = $doc->getElementsByTagName('FC_FeatureType')->item(0);
                    $tab_carrierOfCharacteristics = $FC_FeatureType->getElementsByTagName('carrierOfCharacteristics');
                    $domElemsToRemove = array();
                    foreach ($tab_carrierOfCharacteristics as $carrierOfCharacteristics) {
                        $domElemsToRemove[] = $carrierOfCharacteristics;
                    }

                    foreach ($domElemsToRemove as $domElement) {
                        $domElement->parentNode->removeChild($domElement);
                    }
                    // crée les nouveaux champs
                    $doc = $this->addChamp($doc, $data->getFields())['data'];
                    break;

                case "update" :
                    $FC_FeatureType = $doc->getElementsByTagName('FC_FeatureType')->item(0);
                    $tab_carrierOfCharacteristics = $FC_FeatureType->getElementsByTagName('carrierOfCharacteristics');
                    $domElemsToRemove = array();
                    foreach ($tab_carrierOfCharacteristics as $carrierOfCharacteristics) {
                        $bChampCatalogueFound = false;
                        $LocalName = $carrierOfCharacteristics->getElementsByTagName('LocalName')->item(0);
                        foreach ($tabChampsTableToAdd as $range => $key) {
                            if ($key['name'] === $LocalName->nodeValue) {
                                $valueType = $carrierOfCharacteristics->getElementsByTagName('valueType')->item(0);
                                $CharacterString = $valueType->getElementsByTagName('CharacterString')->item(0);
                                $CharacterString->nodeValue = $key['type'];
                                $bChampCatalogueFound = true;
                                unset($tabChampsTableToAdd[$key['name']]);
                                continue;
                            }
                        }
                        if (!$bChampCatalogueFound) {
                            $domElemsToRemove[] = $carrierOfCharacteristics;
                        }
                    }
                    // supprime les champs du catalogue d'attributs qui ne sont pas présents dans les champs des paramètres
                    foreach ($domElemsToRemove as $domElement) {
                        $domElement->parentNode->removeChild($domElement);
                    }
                    // crée les nouveaux champs
                    $doc = $this->addChamp($doc, $data->getFields())['data'];
                    break;
                case "recreate":
                    $FC_FeatureType = $doc->getElementsByTagName('FC_FeatureType')->item(0);
                    $tab_carrierOfCharacteristics = $FC_FeatureType->getElementsByTagName('carrierOfCharacteristics');
                    $isPresent = array();
                    foreach ($tab_carrierOfCharacteristics as $carrierOfCharacteristics) {
                        $memberName = $carrierOfCharacteristics->getElementsByTagName('LocalName');
                        $value = ltrim($memberName->item(0)->nodeValue);
                        $value = rtrim($value);
                        $isPresent[] = $value;
                    }
                    $newItems = [];
                    foreach ($data->getFields() as $field) {
                        if (!in_array($field['name'], $isPresent, true)) {
                            $newItems[] = $field;
                        }
                    }
                    $doc = $this->addChamp($doc, $newItems)['data'];
                    break;
            }
            $newData = $doc->saveXML();
            $this->baseController->getGeonetworkInterface(false);
            return $this->geonetworkService->patchXml($newData, $catalogueUuid);
        } else {
            return ['error' => 'chargement du xml impossible'];
        }
    }

    /**
     * @param \DOMDocument $doc
     * @param array $fields
     * @return array
     * @throws \DOMException
     */
    public function addChamp(\DOMDocument $doc, array $fields): array
    {
        foreach ($fields as $range => $champ) {
            $FC_FeatureType = $doc->getElementsByTagName('FC_FeatureType')->item(0);
            $carrierOfCharacteristics = $doc->createElement('gfc:carrierOfCharacteristics');
            $FC_FeatureType->appendChild($carrierOfCharacteristics);
            $FC_FeatureAttribute = $doc->createElement('gfc:FC_FeatureAttribute');
            $carrierOfCharacteristics->appendChild($FC_FeatureAttribute);
            // ajoute le nom du champ
            $memberName = $doc->createElement('gfc:memberName');
            $FC_FeatureAttribute->appendChild($memberName);
            $LocalName = $doc->createElement('gco:LocalName');
            $LocalName->setAttribute('xmlns:gco', 'http://www.isotc211.org/2005/gco');
            $LocalName->nodeValue = $champ['name'];
            $memberName->appendChild($LocalName);
            // ajoute la description
            $definition = $doc->createElement('gfc:definition');
            $FC_FeatureAttribute->appendChild($definition);
            $CharacterString = $doc->createElement('gco:CharacterString');
            $CharacterString->setAttribute('xmlns:gco', 'http://www.isotc211.org/2005/gco');
            $CharacterString->nodeValue = $champ['description'];
            $definition->appendChild($CharacterString);
            // ajoute les cardinalités
            $cardinality = $doc->createElement('gfc:cardinality');
            $FC_FeatureAttribute->appendChild($cardinality);
            $Multiplicity = $doc->createElement('gco:Multiplicity');
            $Multiplicity->setAttribute('xmlns:gco', 'http://www.isotc211.org/2005/gco');
            $cardinality->appendChild($Multiplicity);
            $range = $doc->createElement('gco:range');
            $Multiplicity->appendChild($range);
            $MultiplicityRange = $doc->createElement('gco:MultiplicityRange');
            $range->appendChild($MultiplicityRange);
            $lower = $doc->createElement('gco:lower');
            $MultiplicityRange->appendChild($lower);
            $Integer = $doc->createElement('gco:Integer');
            $Integer->nodeValue = "0";
            $lower->appendChild($Integer);
            $upper = $doc->createElement('gco:upper');
            $MultiplicityRange->appendChild($upper);
            $UnlimitedInteger = $doc->createElement('gco:UnlimitedInteger');
            $UnlimitedInteger->setAttribute('isInfinite', 'true');
            $UnlimitedInteger->setAttribute('xsi:nil', 'true');
            $upper->appendChild($UnlimitedInteger);
            // unité de mesure
            $valueMeasurementUnit = $doc->createElement('gfc:valueMeasurementUnit');
            $FC_FeatureAttribute->appendChild($valueMeasurementUnit);
            $UnitDefinition = $doc->createElement('gml:UnitDefinition');
            $UnitDefinition->setAttribute('xmlns:gml', 'http://www.opengis.net/gml');
            $UnitDefinition->setAttribute('gml:id', 'null');
            $valueMeasurementUnit->appendChild($UnitDefinition);
            $identifier = $doc->createElement('gml:identifier');
            $identifier->setAttribute('codeSpace', 'unknown');
            $UnitDefinition->appendChild($identifier);
            // type des champs
            $valueType = $doc->createElement('gfc:valueType');
            $FC_FeatureAttribute->appendChild($valueType);
            $TypeName = $doc->createElement('gco:TypeName');
            $TypeName->setAttribute('xmlns:gco', 'http://www.isotc211.org/2005/gco');
            $valueType->appendChild($TypeName);
            $aName = $doc->createElement('gco:aName');
            $TypeName->appendChild($aName);
            $CharacterString->setAttribute('xmlns:gco', 'http://www.isotc211.org/2005/gco');
            $CharacterString = $doc->createElement('gco:CharacterString');
            $CharacterString->nodeValue = $champ['type'];
            $aName->appendChild($CharacterString);
        }

        return ['data' => $doc, "status" => 200];
    }

    /**
     * @param Data $data
     * @param array $dataInfo
     * @return array|void
     */
    public function getDescription(Data $data, array $dataInfo)
    {
        $geonetwork = new GeonetworkInterface($this->parameterBag->get('PRO_GEONETWORK_URLBASE'), 'srv/fre/');
        $response = $geonetwork->getMetadataRelations($data->getMetadataUuid(), null, "json", 100);
        $ifExist = json_decode($response, JSON_OBJECT_AS_ARRAY)['fcats'];

        if ($ifExist === null) {
            return ['error' => "il n'y a pas de catalogue associé", 'status' => 404];
        }
        foreach ($ifExist as $relation) {
            if ($relation['mdType'] !== null) {
                $catalogueUuid = $relation['id'];
            }
        }
        if (!isset($catalogueUuid)) {
            return [
                'error' => "la récupération de l'identifiant du catalogues associés a échoué",
                'status' => 404
            ];
        }

        $catalogueXml = $this->geonetworkService->getXmlInfo($geonetwork, $catalogueUuid);
        $version = "1.0";
        $encoding = "UTF-8";
        $doc = new \DOMDocument($version, $encoding);
        if ($doc->loadXML($catalogueXml['data'])) {
            $xpath = new \DOMXPath($doc);
            $FC_FeatureType = $doc->getElementsByTagName('FC_FeatureType')->item(0);
            $tab_carrierOfCharacteristics = $FC_FeatureType->getElementsByTagName('carrierOfCharacteristics');
            $items = [];
            foreach ($tab_carrierOfCharacteristics as $carrierOfCharacteristics) {
                $name = $carrierOfCharacteristics->getElementsByTagName('LocalName');
                $description = $carrierOfCharacteristics->getElementsByTagName('CharacterString');
                $nameValue = ltrim($name->item(0)->nodeValue);
                $nameValue = rtrim($nameValue);
                $descriptionValue = ltrim($description->item(0)->nodeValue);
                $descriptionValue = rtrim($descriptionValue);
                $items[$nameValue] = $descriptionValue;
            }
            $i = 0;
            foreach ($dataInfo['fields'] as $file) {
                foreach ($items as $key => $value) {
                    if ($file['name'] === $key) {
                        if($value !== ""){
                            $dataInfo['fields'][$i]['description'] = $value;
                        }else{
                            $dataInfo['fields'][$i]['description'] = $key;
                        }
                    }
                }
                if(empty($items)){
                    $dataInfo['fields'][$i]['description'] =  $dataInfo['fields'][$i]['name'];
                }
                $i++;
            }

            return $dataInfo;
        }
    }
}
