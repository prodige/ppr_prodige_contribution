<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Response\JsonLDResponse;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @Vich\Uploadable
 */
#[ApiResource(
    collectionOperations: [
        'get',
        'post' => [
            'input_formats' => [
                'multipart' => ['multipart/form-data']
            ],
            'denormalization_context' => ['groups' => ['InputDataFile']],
            'openapi_context' => [
                'summary' => 'Allows you to upload files for resource creation',
                'description' => 'Download a file and link it with the data sent as api/data/00',
            ]
        ],

    ],
    itemOperations: ['get', 'delete'],
    normalizationContext: ['groups' => ['GetDataFile']]
)]
#[ORM\Table(name: 'data_file', schema: 'contrib')]
#[ORM\Entity]
#[Vich\Uploadable]
#[ORM\HasLifecycleCallbacks]
class DataFile
{

    #[ApiProperty(types: ['https://schema.org/contentUrl'])]
    #[Groups(['GetDataFile'])]
    public ?string $contentUrl = null;

    #[ORM\Column(name: "file_path", type: 'string', nullable: true)]
    public ?string $filePath = null;

    /**
     * @Vich\UploadableField(mapping="data_files", fileNameProperty="file_path")
     */
    #[Groups(['InputDataFile'])]
    #[Assert\NotNull]
    public ?File $file = null;

    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[Groups(['InputDataFile', 'GetDataFile'])]
    private int $id;

    #[ORM\ManyToOne(targetEntity: Data::class, inversedBy: 'datafiles')]
    #[ORM\JoinColumn(name: 'data_id', referencedColumnName: 'id', nullable: false)]
    #[Assert\NotNull]
    #[Groups(['InputDataFile','GetDataFile'])]
    private Data|JsonLDResponse $data;

    #[ORM\Column(type: 'datetime')]
    private ?\DateTimeInterface $updatedAt = null;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return DataFile
     */
    public function setId(int $id): DataFile
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getContentUrl(): ?string
    {
        return $this->contentUrl;
    }

    /**
     * @param string|null $contentUrl
     * @return DataFile
     */
    public function setContentUrl(?string $contentUrl): DataFile
    {
        $this->contentUrl = $contentUrl;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFilePath(): ?string
    {
        return $this->filePath;
    }

    /**
     * @param string|null $filePath
     * @return DataFile
     */
    public function setFilePath(?string $filePath): DataFile
    {
        $this->filePath = $filePath;
        return $this;
    }

    /**
     * @return File|null
     */
    public function getFile(): ?File
    {
        return $this->file;
    }

    /**
     * @param File|null $file
     */
    public function setFile(?File $file = null): void
    {
        $this->file = $file;

        if (null !== $file) {
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getData(): JsonLDResponse|Data
    {
        return $this->data;
    }

    public function setData(JsonLDResponse|Data $data): void
    {
        $this->data = $data;
    }


    /**
     * @param ExecutionContextInterface $context
     */
    #[Assert\Callback]
    public function validate(ExecutionContextInterface $context)
    {
        if (!in_array($this->file->getMimeType(), [
                'application/json',
                'application/vnd',
                'application/x-sqlite3',
                'application/vnd.ms-excel',
                'application/vnd.oasis.opendocument.spreadsheet',
                'text/plain',
                'text/csv',
                'text/xml',
                'application/octet-stream',
                'text/plain',
                'application/x-dbf',
                'application/vnd.commonspace',
                'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
            ]) || ($this->file->getMimeType() === 'application/octet-stream' && !in_array(
                    strtolower(pathinfo($this->file->getClientOriginalName(), PATHINFO_EXTENSION)),
                    [
                        'shp',
                        'shx',
                        'dbf',
                        'mif',
                        'mid',
                        'tab',
                        'id',
                        'dat',
                        'map',
                        'gxt',
                        'json',
                        'gml',
                        'gmt',
                        'kml',
                        'csv',
                        'xlsx',
                        'ods',
                        'mid',
                        'id',
                        'ind',
                        'map',
                        'dat',
                        'gpkg'
                    ]
                )) || ($this->file->getMimeType() === 'text/plain' && !in_array(
                    strtolower(pathinfo($this->file->getClientOriginalName(), PATHINFO_EXTENSION)),
                    ['qpj', 'prj', 'cpg', 'cst','csv','json','tab', 'gxt']
                )) || ($this->file->getMimeType() === 'application/x-dbf' && !in_array(
                    strtolower(pathinfo($this->file->getClientOriginalName(), PATHINFO_EXTENSION)),
                    ['dbf', 'dat']
                ))|| ($this->file->getMimeType() === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' && !in_array(
                    strtolower(pathinfo($this->file->getClientOriginalName(), PATHINFO_EXTENSION)),
                    ['xlsx']
                )) || ($this->file->getMimeType() === 'application/vnd.ms-excel' && !in_array(
                    strtolower(pathinfo($this->file->getClientOriginalName(), PATHINFO_EXTENSION)),
                    ['xlsx']
                ))) {
            $context
                ->buildViolation('Wrong file type :' . pathinfo($this->file->getClientOriginalName(), PATHINFO_EXTENSION))
                ->atPath('filePath')
                ->addViolation();
        }
    }
}
