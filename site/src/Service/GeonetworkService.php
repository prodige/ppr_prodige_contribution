<?php

namespace App\Service;

use App\Entity\Data;
use Doctrine\DBAL\Connection;
use Doctrine\Persistence\ManagerRegistry;
use Prodige\ProdigeBundle\Controller\BaseController;
use Prodige\ProdigeBundle\Controller\UpdateDomSdomController;
use Prodige\ProdigeBundle\Services\CurlUtilities;
use Prodige\ProdigeBundle\Services\GeonetworkInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Prodige\ProdigeBundle\Services\AdminClientService;

class GeonetworkService
{
    private Connection $connection;

    public function __construct(
        private ParameterBagInterface $parameterBag,
        private ManagerRegistry $doctrine,
        private AdminService $adminService,
        private AdminClientService $adminClientService,
        private LoggerInterface $logger,
        private ContainerInterface $container,
        private AdminCartoDataService $adminCartoDataService,
        private BaseController $baseController
    ) {
        $this->connection = $doctrine->getConnection('prodige');
    }

    /**
     * @param $data
     * @param $mode
     * "CREATE" or "UPDATE"
     * @param $type
     * @param null $extent
     * @return array|void
     * In CREATE mode, record a new xml (post)
     * In UPDATE mode, modify the existing xml according to the received data (patch)
     */
    public function XmlGeonetworkAction($data, $user, $mode, $type, ?array $extent = null)
    {
        $geonetwork = new GeonetworkInterface($this->parameterBag->get('PRO_GEONETWORK_URLBASE'), 'srv/fre/');

        $version = "1.0";
        $metadata_doc = new \DOMDocument($version, "UTF-8");
        $entete = "<?xml version=\"" . $version . "\" encoding=\"UTF-8\"?>\n";
        //setting
        switch ($mode) {
            case "CREATE":
                if ($type === 1) {
                    $metadata_doc = new \DOMDocument($version, "UTF-8");
                    $proMetadataUuid = $data->getMetadataUuid() ?? PRO_GEO_METADATA_UUID;
                    $xml = $this->getXmlInfo(
                        $geonetwork,
                        $proMetadataUuid,
                        'create'
                    );

                    $metadata_data = $xml['data'];
                    if (array_key_exists('error', $xml)) {
                        return ['error' => $xml['error'], 'status' => 404];
                    }
                } elseif ($type === 3) {
                    $metadata_doc = new \DOMDocument($version, "UTF-8");
                    $proMetadataUuid = $data->getMetadataUuid() ?? PRO_NONGEO_METADATA_UUID;
                    $xml = $this->getXmlInfo(
                        $geonetwork,
                        $proMetadataUuid,
                        'create'
                    );
                    $metadata_data = $xml['data'];
                    if (array_key_exists('error', $xml)) {
                        return ['error' => $xml['error'], 'status' => 404];
                    }
                }

                break;
            case "UPDATE":
                $metadata_doc = new \DOMDocument($version, "UTF-8");
                $xml = $this->getXmlInfo($geonetwork, $data->getMetadataUuid());
                $metadata_data = $xml['data'];
                if (array_key_exists('error', $xml)) {
                    return ['error' => $xml['error'], 'status' => 404];
                }
                break;
        }

        if (@$metadata_doc->loadXML($metadata_data) !== false) {
            $xpath = new \DOMXPath($metadata_doc);
            /** @var Data $data */
            // le titre
            if ($data->getTitle()) {
                $objXPathTitle = @$xpath->query(
                    "/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:citation/gmd:CI_Citation/gmd:title/gco:CharacterString"
                );
                if ($objXPathTitle->length > 0) {
                    $element = $objXPathTitle->item(0);
                    $element->nodeValue = $data->getTitle();
                }
            }
            // l'abstract
            if ($data->getAbstract()) {
                $objXPathAbstract = @$xpath->query(
                    "/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:abstract/gco:CharacterString"
                );
                if ($objXPathAbstract->length > 0) {
                    $element = $objXPathAbstract->item(0);
                    $element->nodeValue = $data->getAbstract();
                }
            }

            // le lineage
            if ($data->getLineage() || $data->getLineage() === null) {
                // le lineage
                $objXPathLineage = @$xpath->query(
                    "/gmd:MD_Metadata/gmd:dataQualityInfo/gmd:DQ_DataQuality/gmd:lineage"
                );
                $element = $objXPathLineage->item(0) ? $objXPathLineage->item(0)->getElementsByTagName(
                    'CharacterString'
                ) : null;
                if ($objXPathLineage->length > 0 && $element !== null && $element->length !== 0) {
                    $element->item(0)->nodeValue = $data->getLineage();
                } else {
                    if ($element !== null && $element->length === 0) {
                        $objXPathQuality = @$xpath->query(
                            "/gmd:MD_Metadata/gmd:dataQualityInfo/gmd:DQ_DataQuality/gmd:lineage"
                        );
                        $c = count($objXPathQuality);

                        if ($c !== 0 && $objXPathQuality->item(0)->childNodes->length !== 0) {
                            for ($i = 0; $i <= $c - 1; $i++) {
                                $item = $objXPathQuality->item($i);
                                $nodeMother = $xpath->query(
                                    "/gmd:MD_Metadata"
                                );
                                $node = $nodeMother->item(0);
                                $node->removeChild($item);
                            }
                        }
                    }
                    $lineageFragment = '<tag xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns:gco="http://www.isotc211.org/2005/gco">                                         
                                                    <gmd:lineage>
                                                       <gmd:LI_Lineage>
                                                          <gmd:statement gco:nilReason="missing">
                                                             <gco:CharacterString>' . $data->getLineage() . '</gco:CharacterString>
                                                          </gmd:statement>
                                                          <gmd:source>
                                                             <gmd:LI_Source>
                                                                <gmd:description gco:nilReason="missing">
                                                                   <gco:CharacterString></gco:CharacterString>
                                                                </gmd:description>
                                                             </gmd:LI_Source>
                                                          </gmd:source>
                                                       </gmd:LI_Lineage>
                                                    </gmd:lineage>
                                        </tag>';
                    $lineageDoc = new \DOMDocument();
                    $lineageDoc->loadXML($lineageFragment);
                    $nodeToImport = $lineageDoc->getElementsByTagName("lineage")->item(0);
                    $dataQuality = @$xpath->query(
                        "/gmd:MD_Metadata/gmd:dataQualityInfo/gmd:DQ_DataQuality"
                    );
                    if ($dataQuality && $dataQuality->length > 0) {
                        $newNodeDesc = $metadata_doc->importNode($nodeToImport, true);
                        $dataQuality->item(0)->appendChild($newNodeDesc);
                    }
                }
            }

            if ($data->getThematiqueISO()) {
                $objXPathTopicCategory = @$xpath->query(
                    "/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:topicCategory"
                );

                if ($objXPathTopicCategory->length > 0) {
                    $objXPathTopicCategoryCode = @$xpath->query(
                        "/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:topicCategory/gmd:MD_TopicCategoryCode"
                    );
                    if($objXPathTopicCategoryCode->length > 0){
                        $element = $objXPathTopicCategoryCode->item(0);
                        $element->nodeValue = $data->getThematiqueISO();
                    }else{
                        $codeFragment = '<tag xmlns:gmd="http://www.isotc211.org/2005/gmd">                                                
                                            <gmd:MD_TopicCategoryCode>' . $data->getThematiqueISO() . '</gmd:MD_TopicCategoryCode>                                      
                                         </tag>';

                        $thematiqueISODoc = new \DOMDocument();
                        $thematiqueISODoc->loadXML($codeFragment);
                        $nodeToImport = $thematiqueISODoc->getElementsByTagName("MD_TopicCategoryCode")->item(0);

                        if ($objXPathTopicCategory && $objXPathTopicCategory->length > 0) {
                            $newNodeDesc = $metadata_doc->importNode($nodeToImport, true);
                            $objXPathTopicCategory->item(0)->append($newNodeDesc);
                        }
                    }
                } else {
                    $thematiqueISOFragment = '<tag xmlns:gmd="http://www.isotc211.org/2005/gmd"> 
                                                 <gmd:topicCategory>
                                                    <gmd:MD_TopicCategoryCode>' . $data->getThematiqueISO() . '</gmd:MD_TopicCategoryCode>
                                                 </gmd:topicCategory>
                                              </tag>';

                    $thematiqueISODoc = new \DOMDocument();
                    $thematiqueISODoc->loadXML($thematiqueISOFragment);
                    $nodeToImport = $thematiqueISODoc->getElementsByTagName("topicCategory")->item(0);

                    $nodeMother = @$xpath->query(
                        "/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification"
                    );

                    $lastNode = $nodeMother->item(0)->getElementsByTagName("extent");

                    if ($lastNode && $lastNode->length > 0) {
                        $newNodeDesc = $metadata_doc->importNode($nodeToImport, true);
                        $nodeMother->item(0)->insertBefore($newNodeDesc, $lastNode->item(0));
                    }
                }
            }

            if ($data->getMdContrainte()) {
                $objXPathMdConstraintes = @$xpath->query(
                    "/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:resourceConstraints/gmd:MD_LegalConstraints/gmd:useLimitation/gco:CharacterString"
                );

                if ($objXPathMdConstraintes->length > 0) {
                    $objXPathMdConstraintes->item(0)->nodeValue = $data->getMdContrainte();
                } else {
                    $mdConstraintFragment = '<tag xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns:gco="http://www.isotc211.org/2005/gco"> 
                                               <gmd:resourceConstraints>
                                                  <gmd:MD_LegalConstraints>
                                                    <gmd:useLimitation>
                                                       <gco:CharacterString>' . $data->getMdContrainte() . '</gco:CharacterString>
                                                     </gmd:useLimitation>
                                                   </gmd:MD_LegalConstraints>
                                                 </gmd:resourceConstraints>
                                             </tag>';

                    $mdConstraintDoc = new \DOMDocument();
                    $mdConstraintDoc->loadXML($mdConstraintFragment);
                    $nodeToImport = $mdConstraintDoc->getElementsByTagName("resourceConstraints")->item(0);
                    $medataEndDoc = @$xpath->query(
                        "/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification"
                    );
                    if ($medataEndDoc && $medataEndDoc->length > 0) {
                        $newNodeDesc = $metadata_doc->importNode($nodeToImport, true);
                        $medataEndDoc->item(0)->append($newNodeDesc);
                    }
                }
            }

            if ($data->getMdMaintenance()) {
                $objXPathMdConstraintes = @$xpath->query(
                    "/gmd:MD_Metadata/gmd:metadataMaintenance/gmd:MD_MaintenanceInformation/gmd:maintenanceAndUpdateFrequency/gmd:MD_MaintenanceFrequencyCode"
                );

                if ($objXPathMdConstraintes->length > 0) {
                    $attributes = $objXPathMdConstraintes->item(0)->attributes;
                    foreach ($attributes as $attribute) {
                        if ($attribute->nodeName === "codeListValue") {
                            $attribute->nodeValue = $data->getMdMaintenance();
                        }
                    }
                } else {
                    $mdMaintenanceFragment = '<tag xmlns:gmd="http://www.isotc211.org/2005/gmd"> 
                                                <gmd:metadataMaintenance>
                                                   <gmd:MD_MaintenanceInformation>
                                                      <gmd:maintenanceAndUpdateFrequency>
                                                          <gmd:MD_MaintenanceFrequencyCode codeList="http://standards.iso.org/iso/19139/resources/gmxCodelists.xml#MD_MaintenanceFrequencyCode"
                                                             codeListValue="' . $data->getMdMaintenance() . '"/>
                                                          </gmd:maintenanceAndUpdateFrequency>
                                                   </gmd:MD_MaintenanceInformation>
                                                </gmd:metadataMaintenance>
                                             </tag>';

                    $mdMaintenanceDoc = new \DOMDocument();
                    $mdMaintenanceDoc->loadXML($mdMaintenanceFragment);
                    $nodeToImport = $mdMaintenanceDoc->getElementsByTagName("metadataMaintenance")->item(0);
                    $medataEndDoc = @$xpath->query(
                        "/gmd:MD_Metadata"
                    );
                    if ($medataEndDoc && $medataEndDoc->length > 0) {
                        $newNodeDesc = $metadata_doc->importNode($nodeToImport, true);
                        $medataEndDoc->item(0)->append($newNodeDesc);
                    }
                }
            }

            if ($user) {
                if (!array_key_exists('structures', $user) || empty($user['structures'])) {
                    $objXPathStructureName = @$xpath->query(
                        "/gmd:MD_Metadata/gmd:contact/gmd:CI_ResponsibleParty/gmd:organisationName"
                    );
                    $c = count($objXPathStructureName);

                    if ($c !== 0) {
                        for ($i = 0; $i <= $c - 1; $i++) {
                            $item = $objXPathStructureName->item($i);
                            $nodeMother = $xpath->query(
                                "/gmd:MD_Metadata/gmd:contact/gmd:CI_ResponsibleParty"
                            );
                            $node = $nodeMother->item(0);
                            $node->removeChild($item);
                        }
                    }

                    $structurePhone = @$xpath->query(
                        "/gmd:MD_Metadata/gmd:contact/gmd:CI_ResponsibleParty/gmd:contactInfo/gmd:CI_Contact/gmd:phone"
                    );
                    $c = count($structurePhone);

                    if ($c !== 0) {
                        for ($i = 0; $i <= $c - 1; $i++) {
                            $item = $structurePhone->item($i);
                            $nodeMother = $xpath->query(
                                "/gmd:MD_Metadata/gmd:contact/gmd:CI_ResponsibleParty/gmd:contactInfo/gmd:CI_Contact"
                            );
                            $node = $nodeMother->item(0);
                            $node->removeChild($item);
                        }
                    }

                    $structureAddress = @$xpath->query(
                        "/gmd:MD_Metadata/gmd:contact/gmd:CI_ResponsibleParty/gmd:contactInfo/gmd:CI_Contact/gmd:address/gmd:CI_Address/gmd:deliveryPoint"
                    );

                    $c = count($structureAddress);

                    if ($c !== 0) {
                        for ($i = 0; $i <= $c - 1; $i++) {
                            $item = $structureAddress->item($i);
                            $nodeMother = $xpath->query(
                                "/gmd:MD_Metadata/gmd:contact/gmd:CI_ResponsibleParty/gmd:contactInfo/gmd:CI_Contact/gmd:address/gmd:CI_Address"
                            );
                            $node = $nodeMother->item(0);
                            $node->removeChild($item);
                        }
                    }

                    $structureZipCode = @$xpath->query(
                        "/gmd:MD_Metadata/gmd:contact/gmd:CI_ResponsibleParty/gmd:contactInfo/gmd:CI_Contact/gmd:address/gmd:CI_Address/gmd:postalCode"
                    );

                    $c = count($structureZipCode);

                    if ($c !== 0) {
                        for ($i = 0; $i <= $c - 1; $i++) {
                            $item = $structureZipCode->item($i);
                            $nodeMother = $xpath->query(
                                "/gmd:MD_Metadata/gmd:contact/gmd:CI_ResponsibleParty/gmd:contactInfo/gmd:CI_Contact/gmd:address/gmd:CI_Address"
                            );
                            $node = $nodeMother->item(0);
                            $node->removeChild($item);
                        }
                    }

                    $structureCity = @$xpath->query(
                        "/gmd:MD_Metadata/gmd:contact/gmd:CI_ResponsibleParty/gmd:contactInfo/gmd:CI_Contact/gmd:address/gmd:CI_Address/gmd:city"
                    );

                    $c = count($structureCity);

                    if ($c !== 0) {
                        for ($i = 0; $i <= $c - 1; $i++) {
                            $item = $structureCity->item($i);
                            $nodeMother = $xpath->query(
                                "/gmd:MD_Metadata/gmd:contact/gmd:CI_ResponsibleParty/gmd:contactInfo/gmd:CI_Contact/gmd:address/gmd:CI_Address"
                            );
                            $node = $nodeMother->item(0);
                            $node->removeChild($item);
                        }
                    }

                    $structureCountry = @$xpath->query(
                        "/gmd:MD_Metadata/gmd:contact/gmd:CI_ResponsibleParty/gmd:contactInfo/gmd:CI_Contact/gmd:address/gmd:CI_Address/gmd:country"
                    );
                    $c = count($structureCountry);

                    if ($c !== 0) {
                        for ($i = 0; $i <= $c - 1; $i++) {
                            $item = $structureCountry->item($i);
                            $nodeMother = $xpath->query(
                                "/gmd:MD_Metadata/gmd:contact/gmd:CI_ResponsibleParty/gmd:contactInfo/gmd:CI_Contact/gmd:address/gmd:CI_Address"
                            );
                            $node = $nodeMother->item(0);
                            $node->removeChild($item);
                        }
                    }

                    $userIdentifier = @$xpath->query(
                        "/gmd:MD_Metadata/gmd:contact/gmd:CI_ResponsibleParty/gmd:individualName"
                    );
                    if ($userIdentifier->length > 0) {
                        $userIdentifier = @$xpath->query(
                            "/gmd:MD_Metadata/gmd:contact/gmd:CI_ResponsibleParty/gmd:individualName/gco:CharacterString"
                        );
                        $element = $userIdentifier->item(0);
                        $element->nodeValue = $user['name'] . ' ' . $user['firstName'];
                    } else {
                        $strXmlStructureName = '<tag xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns:gco="http://www.isotc211.org/2005/gco">
                                        <gmd:individualName>
                                           <gco:CharacterString>' . $user['name'] . ' ' . $user['firstName'] . '</gco:CharacterString>
                                        </gmd:individualName>
                                        </tag>';

                        $structureDoc = new \DOMDocument();
                        $structureDoc->loadXML($strXmlStructureName);
                        $nodeToImport = $structureDoc->getElementsByTagName("individualName")->item(0);
                        $responsability = @$xpath->query(
                            "/gmd:MD_Metadata/gmd:contact/gmd:CI_ResponsibleParty"
                        );
                        if ($responsability && $responsability->length > 0) {
                            $newNodeDesc = $metadata_doc->importNode($nodeToImport, true);
                            $responsability->item(0)->appendChild($newNodeDesc);
                        }
                    }
                    $userMail = @$xpath->query(
                        "/gmd:MD_Metadata/gmd:contact/gmd:CI_ResponsibleParty/gmd:contactInfo/gmd:CI_Contact/gmd:address/gmd:CI_Address/gmd:electronicMailAddress/gco:CharacterString"
                    );
                    if ($userMail->length > 0) {
                        $element = $userMail->item(0);
                        $element->nodeValue = $user['email'];
                    }
                } else {
                    //on verifie si un contact existe
                    $objXPathContactExist = @$xpath->query("/gmd:MD_Metadata/gmd:contact");
                    $nodeContactMother = @$xpath->query("/gmd:MD_Metadata");

                    if ($objXPathContactExist->length > 0) {
                        $node = $nodeContactMother->item(0);
                        $node->removeChild($objXPathContactExist->item(0));
                    }

                    //on cherche uuid de la structure.
                    $structureUuid = $user['structures'][0]['referenceGn'];

                    $newTagStruture = '<tag xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns:xlink="http://www.w3.org/1999/xlink">
                                            <gmd:contact xlink:href="local://srv/api/registries/entries/' . $structureUuid . '?lang=fre&amp;process=gmd:role/gmd:CI_RoleCode/@codeListValue~author&amp;schema=iso19139" />
                                           </tag> ';

                    $structureDoc = new \DOMDocument();
                    $structureDoc->loadXML($newTagStruture);
                    $nodeToImport = $structureDoc->getElementsByTagName("contact")->item(0);


                    $nodeMother = @$xpath->query(
                        "/gmd:MD_Metadata"
                    );


                    $lastNode = @$xpath->query("/gmd:MD_Metadata/gmd:dateStamp");

                    if ($lastNode && $lastNode->length > 0) {
                        $newNodeDesc = $metadata_doc->importNode($nodeToImport, true);
                        $nodeMother->item(0)->insertBefore($newNodeDesc, $lastNode->item(0));
                    }
                }
            }

            if ($data->getContact()) {
                //on enleve tous les items
                $objXPathContact = @$xpath->query(
                    "/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:pointOfContact"
                );
                $c = count($objXPathContact);

                if ($c !== 0) {
                    for ($i = 0; $i <= $c - 1; $i++) {
                        $item = $objXPathContact->item($i);
                        $nodeMother = $xpath->query(
                            "/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification"
                        );
                        $node = $nodeMother->item(0);
                        $node->removeChild($item);
                    }
                }

                foreach ($data->getContact() as $contact) {
                    $metadata_doc = $this->addContact($metadata_doc, $contact, $user);
                }
            }

            //equivalentScale
            if ($data->getEquivalentScale()) {
                $objXPathEquivalentScale = @$xpath->query(
                    "/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:spatialResolution/gmd:MD_Resolution/gmd:equivalentScale/gmd:MD_RepresentativeFraction/gmd:denominator/gco:Integer"
                );
                if ($objXPathEquivalentScale->length > 0) {
                    $element = $objXPathEquivalentScale->item(0);
                    $element->nodeValue = $data->getEquivalentScale();
                }
            }
            // les extents
            if ($extent !== null) {
                $extentsValue = $extent;
            } else {
                $extentsValue = $data->getExtent();
            }
            if ($extentsValue !== null) {
                foreach ($extentsValue as $key => $value) {
                    $path = "/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:extent/gmd:EX_Extent/gmd:geographicElement/gmd:EX_GeographicBoundingBox";
                    switch ($key) {
                        case "xmin":
                            $path .= "/gmd:westBoundLongitude";
                            break;
                        case "xmax":
                            $path .= "/gmd:eastBoundLongitude";
                            break;
                        case "ymin":
                            $path .= "/gmd:southBoundLatitude";
                            break;
                        case "ymax":
                            $path .= "/gmd:northBoundLatitude";
                            break;
                    }
                    $objXPath = @$xpath->query(
                        $path . "/gco:Decimal"
                    );
                    if ($objXPath->length > 0) {
                        $element = $objXPath->item(0);
                        $element->nodeValue = $value;
                    }
                }
            }
            // le status
            if ($mode === "CREATE" && $data->getGeonetworkStatus()) {
                $statusFragment =
                    '<tag xmlns:gmd="http://www.isotc211.org/2005/gmd">
                    <gmd:status>
                        <gmd:MD_ProgressCode codeList="http://standards.iso.org/iso/19139/resources/gmxCodelists.xml#MD_ProgressCode" codeListValue="' . $data->getGeonetworkStatus(
                    ) . '"/>
                    </gmd:status>
                </tag>';
                $statusDoc = new \DOMDocument();
                $statusDoc->loadXML($statusFragment);
                $nodeToImport = $statusDoc->getElementsByTagName("status")->item(0);
                $identification = @$xpath->query(
                    "/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification"
                );
                if ($identification && $identification->length > 0) {
                    $newNodeDesc = $metadata_doc->importNode($nodeToImport, true);
                    $identification->item(0)->appendChild($newNodeDesc);
                }
            }
            //Création date
            $objXPathCreatedCode = @$xpath->query(
                "/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:citation/gmd:CI_Citation/gmd:date/gmd:CI_Date/gmd:dateType/gmd:CI_DateTypeCode"
            );
            foreach ($objXPathCreatedCode as $dateFragment) {
                if ($dateFragment->getAttribute('codeListValue') === "creation") {
                    $dateTag = $dateFragment->parentNode->parentNode->parentNode->getElementsByTagName("Date")->item(0);
                    $dateTag->nodeValue = $data->getCreatedAt()->format('Y-m-d');
                }
            }
            //mise à jours date
            switch ($mode) {
                case "CREATE":
                    $revisionDateFragment =
                        '<tag xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns:gco="http://www.isotc211.org/2005/gco">
                            <gmd:date>
                                 <gmd:CI_Date>
                                     <gmd:date>
                                         <gco:DateTime>' . $data->getUpdatedAt()->format('Y-m-d') . '</gco:DateTime>
                                     </gmd:date>
                                     <gmd:dateType>
                                         <gmd:CI_DateTypeCode codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#CI_DateTypeCode" codeListValue="revision"/>
                                     </gmd:dateType>
                                 </gmd:CI_Date>
                            </gmd:date>
                        </tag>';

                    $dateDoc = new \DOMDocument();
                    $dateDoc->loadXML($revisionDateFragment);
                    $nodeToImport = $dateDoc->getElementsByTagName("date")->item(0);
                    $identification = @$xpath->query(
                        "/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:citation/gmd:CI_Citation"
                    );
                    if ($identification && $identification->length > 0) {
                        $newNodeDesc = $metadata_doc->importNode($nodeToImport, true);
                        $identification->item(0)->appendChild($newNodeDesc);
                    }
                    break;
                case "UPDATE":
                    $objXPathUpdateCode = @$xpath->query(
                        "/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:citation/gmd:CI_Citation/gmd:date/gmd:CI_Date/gmd:dateType/gmd:CI_DateTypeCode"
                    );
                    foreach ($objXPathUpdateCode as $dateFragment) {
                        if ($dateFragment->getAttribute('codeListValue') === "revision") {
                            $dateFragment->nodeValue = $data->getUpdatedAt()->format('Y-m-d');
                        }
                    }
                    if ($data->getOpenData() === true) {
                        $addDownload = $this->addServiceInMetadata(
                            $metadata_doc,
                            $data,
                            $this->parameterBag->get(
                                'PRODIGE_URL_TELECHARGEMENT'
                            ) . "/download/" . $data->getMetadataUuid(),
                            "WWW:DOWNLOAD-1.0-http--download"
                        );
                    } else {
                        $metadata_doc = $this->removeServiceOnline(
                            $metadata_doc,
                            "DOWNLOAD"
                        );
                    }
                    // etape supplementaire pour les données vectorielle
                    if ($type === 1 && $data->getOpenData() === true) {
                        $addAtom = $this->addServiceInMetadata(
                            $metadata_doc,
                            $data,
                            $this->parameterBag->get(
                                'PRODIGE_URL_CATALOGUE'
                            ) . "/rss/atomfeed/atomdataset/" . $data->getMetadataUuid(),
                            "WWW:LINK-1.0-http--link",
                            "Téléchargement direct des données",
                        );
                        $addWms = $this->addServiceInMetadata(
                            $metadata_doc,
                            $data,
                            $this->parameterBag->get(
                                'PRODIGE_URL_DATACARTO'
                            ) . "/wms?service=WMS&request=GetCapabilities",
                            "OGC:WMS-1.1.1-http-get-map",
                            $data->getTable()
                        );
                        $addwfsUuid = $this->addServiceInMetadata(
                            $metadata_doc,
                            $data,
                            $this->parameterBag->get(
                                'PRODIGE_URL_DATACARTO'
                            ) . "/wfs/" . $data->getWfsMetadataUuid() . "?service=WFS&request=GetCapabilities",
                            "OGC:WFS-1.0.0-http-get-capabilities",
                            $data->getTable()
                        );
                        $addwfs = $this->addServiceInMetadata(
                            $metadata_doc,
                            $data,
                            $this->parameterBag->get(
                                'PRODIGE_URL_DATACARTO'
                            ) . "/wfs?service=WFS&request=GetCapabilities",
                            "OGC:WFS-1.0.0-http-get-capabilities",
                            $data->getTable()
                        );
                    } else {
                        $metadata_doc = $this->removeServiceOnline(
                            $metadata_doc,
                            "LINK",
                        );
                        $metadata_doc = $this->removeServiceOnline(
                            $metadata_doc,
                            "WMS"
                        );
                        $metadata_doc = $this->removeServiceOnline(
                            $metadata_doc,
                            "WFS",
                        );
                        $metadata_doc = $this->removeServiceOnline(
                            $metadata_doc,
                            "WFS",
                        );
                    }
                    break;
            }

            //les themes Keywords
            if ($data->getThemeKeyword()) {
                if ($mode === "UPDATE") {
                    $metadata_doc = $this->removeThesaurusFragment("descriptiveKeywords", "theme", $metadata_doc);
                } else {
                    $metadata_doc = $this->controleBaliseThesaurus($metadata_doc);
                }

                $themeFragment = $this->getThesaurusXml($geonetwork, $data->getThemeKeyword());

                if (is_array($themeFragment)) {
                    return ['error' => "Theme Thésaurus not found" . $themeFragment['error'], "status" => 404];
                }

                $metadata_doc = $this->insertThemeOrLocalisationKeyword($themeFragment, $metadata_doc, $type);
            }
            //les localisations Keywords
            if ($data->getLocalisationKeyword()) {
                if ($mode === "UPDATE") {
                    $metadata_doc = $this->removeThesaurusFragment("descriptiveKeywords", "place", $metadata_doc);
                }
                $localisationFragment = $this->getThesaurusXml($geonetwork, $data->getLocalisationKeyword());
                if (is_array($localisationFragment)) {
                    return ['error' => "Place Thésaurus not found" . $localisationFragment['error'], "status" => 404];
                }
                $metadata_doc = $this->insertThemeOrLocalisationKeyword($localisationFragment, $metadata_doc, $type);
            }

            //pour reformater le doc, suppression des espaces vides et réalignement des elements
            $metadata_doc->preserveWhiteSpace = false;

            $new_metadata_data = $metadata_doc->saveXML();
            $new_metadata_data = str_replace($entete, "", $new_metadata_data);
            $this->baseController->getGeonetworkInterface(false);
            switch ($mode) {
                case "CREATE":
                    return $this->createXml($new_metadata_data);
                case "UPDATE":
                    return $this->patchXml($new_metadata_data, $data->getMetadataUuid());
            }
        }
    }

    /**
     * @param $geonetwork
     * receives as input the connection to geonetwork and $data->getMetadataUuid()
     * @param $metadataUuid
     * @return array
     * return the xml registered in geonetwork
     */
    public function getXmlInfo($geonetwork, $metadataUuid, $mode = null): array
    {
        if ($mode === "create") {
            $this->baseController->getGeonetworkInterface(true);
        }

        $response = $geonetwork->get(
            'api/records/' . urlencode(
                $metadataUuid
            ) . "/formatters/xml",
            false,
        );
        if ($mode === "create") {
            $this->baseController->getGeonetworkInterface(false);
        }
        if ($response === "") {
            return ['error' => "xml not found", 'status' => 404];
        }
        return ['data' => $response, 'status' => 200];
    }

    /**
     * @param string $uuid
     * @return array
     */
    public function getContact(string $uuid): array
    {
        $geonetwork = new GeonetworkInterface($this->parameterBag->get('PRO_GEONETWORK_URLBASE'), 'srv/fre/');
        $response = $geonetwork->get(
            'api/records/' . urlencode($uuid) . '/editor?currTab=xml',
            false
        );
        $contacts = [];
        $metadata_load = new \DOMDocument("1.0", "UTF-8");
        if (@$metadata_load->loadHTML($response) !== false) {
            $metadata_decoded = new \DOMDocument("1.0", "UTF-8");
            if (@$metadata_decoded->loadXML($metadata_load->getElementById('xmleditor')->nodeValue) !== false) {
                $xpath = new \DOMXPath($metadata_decoded);
                $objXPathContact = @$xpath->query(
                    "/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:pointOfContact"
                );
                if ($objXPathContact->length > 0) {
                    foreach ($objXPathContact as $contact) {
                        //si il y a un attribut c'est que ce n'est pas une structure
                        if ($contact->getAttribute('xlink:href')) {
                            $match = str_replace('&schema=iso19139', '', $contact->getAttribute('xlink:href'));
                            $matchRole = explode('~', $match);
                            $role = $matchRole[1];
                            $match2 = explode('entries/', $match);
                            $match2 = explode('?', $match2[1]);
                            $sUuid = $match2[0];


                            if (isset($role, $sUuid)) {
                                //si c'est une structure on arrive ici
                                $geonetwork = new GeonetworkInterface(
                                    $this->parameterBag->get('PRO_GEONETWORK_URLBASE'),
                                    'srv/fre/'
                                );
                                $xmlStructure = $this->getXmlInfo($geonetwork, $sUuid);
                                $metadata_structure = new \DOMDocument("1.0", "UTF-8");
                                $metadata_structure->loadXML($xmlStructure['data']);
                                $xpath = new \DOMXPath($metadata_structure);

                                $objXPathStructure = @$xpath->query(
                                    "/gmd:CI_ResponsibleParty"
                                );
                                foreach ($objXPathStructure as $structure) {
                                    $organization = $structure->getElementsByTagName("organisationName")->item(
                                        0
                                    ) ? rtrim(
                                        ltrim($structure->getElementsByTagName("organisationName")->item(0)->nodeValue)
                                    ) : null;
                                    $objXPathStructureMail = @$xpath->query(
                                        "/gmd:CI_ResponsibleParty/gmd:contactInfo/gmd:CI_Contact/gmd:address/gmd:CI_Address/gmd:electronicMailAddress/gco:CharacterString"
                                    );

                                    $mail = $objXPathStructureMail->item(0) ? $objXPathStructureMail->item(
                                        0
                                    )->nodeValue : null;

                                    $role = $role ?? null;
                                    $type = "link";
                                }
                            }
                        } else {
                            $organization = rtrim(
                                ltrim($contact->getElementsByTagName("organisationName")->item(0)->nodeValue)
                            );
                            $mail = $contact->getElementsByTagName("electronicMailAddress")->item(0) ? rtrim(
                                ltrim($contact->getElementsByTagName("electronicMailAddress")->item(0)->nodeValue)
                            ) : null;
                            $role = $contact->getElementsByTagName("CI_RoleCode")->item(
                                0
                            ) ? $contact->getElementsByTagName(
                                "CI_RoleCode"
                            )->item(0)->getAttribute(
                                'codeListValue'
                            ) : null;
                            $type = "text";
                        }
                        $contacts[] = [
                            "mail" => $mail,
                            "organization" => $organization,
                            "role" => $role,
                            "type" => $type
                        ];
                    }
                }
            }
        }
        return $contacts;
    }

    /**
     * @param $metadata_doc
     * @param $contact
     * @return mixed
     */
    public function addContact(
        $metadata_doc,
        $contact,
        $user
    ): mixed {
        $xpath = new \DOMXPath($metadata_doc);

        if ($contact['type'] === "text") {
            $strXml = '
            <tag xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns:gco="http://www.isotc211.org/2005/gco">
                <gmd:pointOfContact>
                    <gmd:CI_ResponsibleParty>
                        <gmd:organisationName gco:nilReason="missing">
                           <gco:CharacterString>' . $contact['organization'] . '</gco:CharacterString>
                        </gmd:organisationName>
                        <gmd:positionName gco:nilReason="missing">
                           <gco:CharacterString/>
                        </gmd:positionName>
                        <gmd:contactInfo>
                           <gmd:CI_Contact>
                              <gmd:phone>
                                 <gmd:CI_Telephone>
                                    <gmd:voice gco:nilReason="missing">
                                       <gco:CharacterString/>
                                    </gmd:voice>
                                 </gmd:CI_Telephone>
                              </gmd:phone>
                              <gmd:address>
                                 <gmd:CI_Address>
                                    <gmd:electronicMailAddress>
                                        <gco:CharacterString>' . $contact['mail'] . '</gco:CharacterString>
                                    </gmd:electronicMailAddress>
                                 </gmd:CI_Address>
                              </gmd:address>
                           </gmd:CI_Contact>
                        </gmd:contactInfo>
                        <gmd:role>
                           <gmd:CI_RoleCode codeList="http://standards.iso.org/iso/19139/resources/gmxCodelists.xml#CI_RoleCode"
                                            codeListValue="' . $contact['role'] . '"/>
                        </gmd:role>
                    </gmd:CI_ResponsibleParty>
                </gmd:pointOfContact>
            </tag>';
        } elseif ($contact['type'] === "link") {
            if (!array_key_exists('structure', $user)) {
                $structureUuid = $this->adminService->getStructureUuid($contact['organization']);
            } else {
                $structureUuid = $user['structure']['referenceGn'];
            }

            $role = $contact['role'] ?? 'author';

            $strXml = '<tag xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <gmd:pointOfContact xlink:href="local://srv/api/registries/entries/'
                . $structureUuid['structureUuid'] .
                '?lang=fre&amp;process=gmd:role/gmd:CI_RoleCode/@codeListValue~' . $role . '&amp;schema=iso19139"/>\r\n
                </tag>';
        } else {
            return $metadata_doc;
        }

        //insertion of new contact tag
        $fragmentToImport = new \DOMDocument();
        $fragmentToImport->loadXML($strXml);

        // inserting tags in the right place
        $nodeToImport = $fragmentToImport->getElementsByTagName("pointOfContact")->item(0);

        $nodeMother = @$xpath->query(
            "/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification"
        );

        $lastNode = $nodeMother->item(0)->getElementsByTagName("resourceMaintenance");

        if ($lastNode->length === 0) {
            $lastNode = $nodeMother->item(0)->getElementsByTagName("graphicOverview");

            if ($lastNode->length === 0) {
                $lastNode = $nodeMother->item(0)->getElementsByTagName("descriptiveKeywords");
            }

            if ($lastNode->length === 0) {
                $lastNode = $nodeMother->item(0)->getElementsByTagName("spatialRepresentationType");
            }
        }

        if ($lastNode && $lastNode->length > 0) {
            $newNodeDesc = $metadata_doc->importNode($nodeToImport, true);
            $nodeMother->item(0)->insertBefore($newNodeDesc, $lastNode->item(0));
        }

        return $metadata_doc;
    }

    /**
     * @param $metadata_doc
     * @param Data $data
     * @param string $serviceUrl
     * @param string $serviceProtocole
     * @param string|null $serviceName
     * @param string $mode
     * @param string $version
     * @return array|\DOMDocument
     */
    public function addServiceInMetadata(
        $metadata_doc,
        Data $data,
        string $serviceUrl,
        string $serviceProtocole,
        string $serviceName = null,
        string $mode = "UPDATE",
        string $version = "1.0"
    ): \DOMDocument|array {
        if ($mode === "CREATE") {
            $geonetwork = new GeonetworkInterface($this->parameterBag->get('PRO_GEONETWORK_URLBASE'), 'srv/fre/');
            $metadata_doc = new \DOMDocument($version, "UTF-8");
            $xml = $this->getXmlInfo($geonetwork, $data->getMetadataUuid());
            $metadata_data = $xml['data'];
            if (array_key_exists('error', $xml)) {
                return ['error' => $xml['error'], 'status' => 404];
            }
            @$metadata_doc->loadXML($metadata_data);
        }
        if ($metadata_doc !== false) {
            $itAlreadyExists = $metadata_doc->getElementsByTagName("distributionInfo");
            if ($itAlreadyExists->length === 0) {
                $racine = $metadata_doc->getElementsByTagName("MD_Metadata");
                $createStrXML =
                    '<gmd:MD_Metadata xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns="http://www.isotc211.org/2005/gmd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:fra="http://www.cnig.gouv.fr/2005/fra" xmlns:gco="http://www.isotc211.org/2005/gco" xmlns:gts="http://www.isotc211.org/2005/gts" xmlns:gml="http://www.opengis.net/gml" xmlns:gfc="http://www.isotc211.org/2005/gfc" xmlns:gmx="http://www.isotc211.org/2005/gmx" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:gmi="http://www.isotc211.org/2005/gmi">
                                      <gmd:distributionInfo>
                                        <gmd:MD_Distribution>
                                          <gmd:distributionFormat>
                                            <gmd:MD_Format>
                                              <gmd:name>
                                                <gco:CharacterString>ZIP</gco:CharacterString>
                                              </gmd:name>
                                              <gmd:version gco:nilReason="missing">
                                                <gco:CharacterString/>
                                              </gmd:version>
                                            </gmd:MD_Format>
                                          </gmd:distributionFormat>
                                          <gmd:transferOptions>
                                            <gmd:MD_DigitalTransferOptions>
                                                 <gmd:onLine>
                                                   <gmd:CI_OnlineResource>
                                                     <gmd:linkage>
                                                       <gmd:URL>' . $serviceUrl . '</gmd:URL>
                                                     </gmd:linkage>
                                                     <gmd:protocol>
                                                       <gco:CharacterString>' . $serviceProtocole . '</gco:CharacterString>
                                                     </gmd:protocol>';
                if ($serviceName !== null) {
                    $nameStrXml =
                        '<gmd:name>
                               <gco:CharacterString>' . $serviceName . '</gco:CharacterString>
                         </gmd:name>';


                    $createStrXML .= $nameStrXml;
                }

                $endStrXml =
                    '</gmd:CI_OnlineResource>
                               </gmd:onLine>
                             </gmd:MD_DigitalTransferOptions>
                        </gmd:transferOptions>
                      </gmd:MD_Distribution>
                    </gmd:distributionInfo></gmd:MD_Metadata>';

                $createStrXML .= $endStrXml;

                $orgDoc = new \DOMDocument;
                $orgDoc->loadXML($createStrXML);
                $nodeToImport = $orgDoc->getElementsByTagName("distributionInfo")->item(0);
                $newNodeDesc = $metadata_doc->importNode($nodeToImport, true);
                $dataQualityInfo = $racine->item(0)->getElementsByTagName("dataQualityInfo");
                if ($dataQualityInfo && $dataQualityInfo->length > 0) {
                    $racine->item(0)->insertBefore($newNodeDesc, $dataQualityInfo->item(0));
                } else {
                    $racine->item(0)->appendChild($newNodeDesc);
                }

                $metadata_doc->preserveWhiteSpace = false;

                $new_metadata_data = $metadata_doc->saveXML();
                return $this->patchXml($new_metadata_data, $data->getMetadataUuid());
            }

            $isUpdate = $metadata_doc->getElementsByTagName("transferOptions");

            if ($isUpdate->length > 0) {
                $isExist = false;
                foreach ($metadata_doc->getElementsByTagName('onLine') as $item) {
                    if ($item->getElementsByTagName("URL")->item(0)->nodeValue === $serviceUrl) {
                        $isExist = $item;
                    }
                }
                if ($isExist === false) {
                    //si c'est un deuxième tour gérer l'inscription de ce qu'il reste de la balise à partir de transfert distribution
                    $addStrXML =
                        "<tag xmlns:gmd=\"http://www.isotc211.org/2005/gmd\" xmlns:gco=\"http://www.isotc211.org/2005/gco\">                                    
                        <gmd:onLine>
                            <gmd:CI_OnlineResource>
                                <gmd:linkage>
                                    <gmd:URL>" . htmlspecialchars($serviceUrl) . "</gmd:URL>
                                </gmd:linkage>
                            <gmd:protocol>
                                <gco:CharacterString>" . $serviceProtocole . "</gco:CharacterString>
                            </gmd:protocol>";

                    if ($serviceName !== null) {
                        $nameStrXml =
                            "<gmd:name>
                               <gco:CharacterString>" . $serviceName . "</gco:CharacterString>
                            </gmd:name>";

                        $addStrXML .= $nameStrXml;
                    }
                    $endStrXml =
                        "</gmd:CI_OnlineResource>
                            </gmd:onLine>                                        
                    </tag>";

                    $addStrXML .= $endStrXml;

                    $orgDoc = new \DOMDocument;
                    $orgDoc->loadXML($addStrXML);

                    $xpath = new \DOMXPath($metadata_doc);
                    $transfertOption = $xpath->query(
                        "/gmd:MD_Metadata/gmd:distributionInfo/gmd:MD_Distribution/gmd:transferOptions/gmd:MD_DigitalTransferOptions"
                    );
                    $nodeToImport = $orgDoc->getElementsByTagName("onLine")->item(0);
                    $newNodeDesc = $metadata_doc->importNode($nodeToImport, true);
                    $transfertOption->item(0)->appendChild($newNodeDesc);
                }
            }

            if ($mode === "CREATE") {
                $metadata_doc->preserveWhiteSpace = false;
                $new_metadata_data = $metadata_doc->saveXML();

                return $this->patchXml($new_metadata_data, $data->getMetadataUuid());
            }

            return $metadata_doc;
        }
        return [
            "error" => "Unable to create the metadata from the card template",
            "status" => 422
        ];
    }

    /**
     * @param $new_metadata_data
     * @param $metadataUuid
     * @return array
     */
    public function patchXml(
        $new_metadata_data,
        $metadataUuid,
        $id = null
    ): array {
        $this->baseController->getGeonetworkInterface(true);

        $geonetwork = new GeonetworkInterface($this->parameterBag->get('PRO_GEONETWORK_URLBASE'), 'srv/fre/');
        $params = [
            "template" => "n",
            "commit" => true,
            "tab" => "xml",
            "data" => $new_metadata_data
        ];

        $this->logger->info('patch metadata xml vers geonetwork');
        $jsonResp = $geonetwork->post(
            'api/records/' . urlencode($metadataUuid) . '/editor',
            $params,
            false
        );

        preg_match('/apiError/', $jsonResp, $response);
        if (count($response) === 0) {
            $return = [
                'message' => 'the xml modification has been successfully completed',
                'uuid' => $metadataUuid,
                "status" => 200
            ];
            if ($id !== null) {
                $return["id"] = $id;
            }
            $this->baseController->getGeonetworkInterface(false);
            return $return;
        }
        $this->logger->error('patch metadata xml vers geonetwork', ['error' => $response[1]]);
        $this->baseController->getGeonetworkInterface(false);
        return ['error' => $response[1], 'status' => 404];
    }

    /**
     * @param $metadata_doc
     * @param string $service
     * @return mixed
     */
    public function removeServiceOnline(
        $metadata_doc,
        string $service
    ): mixed {
        switch ($service) {
            case "LINK":
                $serviceProtocole = "WWW:LINK-1.0-http--link";
                break;
            case "DOWNLOAD":
                $serviceProtocole = "WWW:DOWNLOAD-1.0-http--download";
                break;
            case "WMS":
                $serviceProtocole = "OGC:WMS-1.1.1-http-get-map";
                break;
            case "WFS":
                $serviceProtocole = "OGC:WFS-1.0.0-http-get-capabilities";
                break;
        }

        $xpath = new \DOMXPath($metadata_doc);
        $ifExists = $metadata_doc->getElementsByTagName("distributionInfo");
        // en cas de retrait supprimer à partir du transfertDistribution
        if ($ifExists && $ifExists->length > 0) {
            foreach ($metadata_doc->getElementsByTagName('onLine') as $item) {
                if (isset($serviceProtocole) && $serviceProtocole === $item->getElementsByTagName("protocol")->item(
                        0
                    )->getElementsByTagName("CharacterString")->item(0)->nodeValue) {
                    $nodeMother = $xpath->query(
                        "/gmd:MD_Metadata/gmd:distributionInfo/gmd:MD_Distribution/gmd:transferOptions/gmd:MD_DigitalTransferOptions"
                    );
                    $node = $nodeMother->item(0);
                    $node->removeChild($item);
                }
            }
        }

        return $metadata_doc;
    }

    /**
     * @param $fragmentName
     * name of the tag to delete
     * @param $thesaurus
     * "place" or "theme"
     * @param $metadata_doc
     * the document to be modified
     * @return mixed
     * the modified document
     */
    public function removeThesaurusFragment(
        $fragmentName,
        $thesaurus,
        $metadata_doc
    ): mixed {
        $nodeToDelete = $metadata_doc->getElementsByTagName($fragmentName);
        foreach ($nodeToDelete as $fragment) {
            if ($fragment->getElementsByTagName("MD_KeywordTypeCode")->item(0) !== null) {
                if ($fragment->getElementsByTagName("MD_KeywordTypeCode")->item(0)->getAttribute(
                        'codeListValue'
                    ) === $thesaurus) {
                    if ($fragment->getElementsByTagName("Anchor") && $fragment->getElementsByTagName("Anchor")->item(
                            0
                        )) {
                        if ($fragment->getElementsByTagName("Anchor")->item(
                                0
                            )->childNodes->item(0)->data !== "geonetwork.thesaurus.external.theme.prodige") {
                            $nodeMother = $metadata_doc->getElementsByTagName("MD_DataIdentification");
                            $node = $nodeMother->item(0);
                            $node->removeChild($fragment);
                        }
                    }
                }
            }
        }

        return $metadata_doc;
    }

    /**
     * @param $metadata_doc
     * @return mixed
     */
    private
    function controleBaliseThesaurus(
        $metadata_doc
    ): mixed {
        $nodeToDelete = $metadata_doc->getElementsByTagName('descriptiveKeywords');
        foreach ($nodeToDelete as $fragment) {
            $nodeMother = $metadata_doc->getElementsByTagName("MD_DataIdentification");
            $node = $nodeMother->item(0);
            $node->removeChild($fragment);
        }

        return $metadata_doc;
    }

    /**
     * @param $geonetwork
     * @param $data
     * receives as input the connection to geonetwork and $data->getThemeKeyword()
     * or the connection to geonetwork and $data->getLocalisationKeyword()
     * @return mixed|void
     * return the corresponding xml fragment
     */
    public function getThesaurusXml(
        $geonetwork,
        $data
    ) {
        $urlGetXml = "api/registries/vocabularies/keyword?id=";
        $response = "";
        foreach ($data as $key => $item) {
            $url = 'api/registries/vocabularies/search?type=MATCH&thesaurus=' . $key . "&q=";
            $curlOptions = array(
                CURLOPT_HTTPHEADER => array(
                    'accept: application/json'
                )
            );
            $keywordListId = [];
            foreach ($item as $value) {
                $urlgetId = $url . rawurlencode($value) . "&lang=fre";
                $response = $geonetwork->get(
                    $urlgetId,
                    false,
                    $curlOptions
                );
                $getId = json_decode($response, JSON_FORCE_OBJECT)[0]["uri"];
                $keywordListId[] = $getId;
            }
            $i = count($keywordListId);
            $t = 0;
            foreach ($keywordListId as $value) {
                $urlGetXml .= rawurlencode($value);
                if ($t !== $i - 1) {
                    $urlGetXml .= ",";
                    $t++;
                }
            }
            $urlGetXml .= "&thesaurus=" . $key . "&lang=fre";
            $response = $geonetwork->get(
                $urlGetXml,
                false
            );
            preg_match('/apiError/', $response, $error);
            if (count($error) > 0) {
                preg_match('/<code>([A-Za-z].+)<\/code>/', $response, $messageError);
                return ['error' => $messageError[1], 'status' => 404];
            }
        }
        return $response;
    }

    /**
     * @param $fragment
     * @param $metadata_doc
     * allows you to insert the theme and location fragments in the xml
     * @param int $type
     * @return mixed
     */
    public function insertThemeOrLocalisationKeyword(
        $fragment,
        $metadata_doc,
        int $type
    ): mixed {
        $xpath = new \DOMXPath($metadata_doc);
        $keyword = substr($fragment, strlen('<?xml version="1.0" encoding="UTF-8"?>'));
        $newNode = '<tag xmlns:gmd="http://www.isotc211.org/2005/gmd" >';
        $newNode .= $keyword;
        $newNode .= "</tag>";
        //insertion of the keyword tag before the constrained resources
        $fragmentToImport = new \DOMDocument();
        $fragmentToImport->loadXML($newNode);

        // inserting tags in the right place
        $nodeToImport = $fragmentToImport->getElementsByTagName("descriptiveKeywords")->item(0);
        $nodeMother = @$xpath->query(
            "/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification"
        );

        if ($nodeMother && $nodeMother->length > 0) {
            $newNodeDesc = $metadata_doc->importNode($nodeToImport, true);
            switch ($type) {
                case 1:
                    $lastNode = $nodeMother->item(0)->getElementsByTagName("resourceConstraints");
                    if ($lastNode->length === 0) {
                        $lastNode = $nodeMother->item(0)->getElementsByTagName("spatialRepresentationType");
                    }
                    break;
                case 3:
                    $lastNode = $nodeMother->item(0)->getElementsByTagName("language");
                    break;
            }

            if ($lastNode && $lastNode->length > 0) {
                $nodeMother->item(0)->insertBefore($newNodeDesc, $lastNode->item(0));
            } else {
                $nodeMother->item(0)->appendChild($newNodeDesc);
            }
        }
        return $metadata_doc;
    }

    /**
     * @param $new_metadata_data
     * @return array
     * sauvegarde un nouveau xml et renvoie son id et son uuid
     */
    public function createXml(
        $new_metadata_data
    ): array {
        $geonetwork = new GeonetworkInterface($this->parameterBag->get('PRO_GEONETWORK_URLBASE'), 'srv/fre/');
        $params = [
            "uuidProcessing" => "GENERATEUUID"
        ];

        $this->logger->info('Create xml to geonetwork');
        $response = $geonetwork->put(
            'api/records?' . http_build_query($params),
            $new_metadata_data,
            false,
            false,
            true
        );

        $jsonResp = json_decode($response, JSON_FORCE_OBJECT);

        if ($jsonResp === null) {
            $this->logger->error('create xml error geonetwork ', ['error' => $response]);
            return ["error" => $response, "status" => 409];
        }
        if (isset($jsonResp["message"]) && $jsonResp["message"] === "IllegalArgumentException") {
            $this->logger->error('create xml error ', ['error' => $jsonResp["message"]]);
            return [
                "error" => "Unable to create the metadata from the card template",
                "status" => 422
            ];
        }
        if (array_key_exists('metadataInfos', $jsonResp)) {
            $id = array_keys($jsonResp["metadataInfos"])[0];

            return [
                'id' => $id,
                'uuid' => $jsonResp["metadataInfos"][$id][0]['uuid']
            ];
        }
        $this->logger->error('create xml error geonetwork :', ['error' => 'Recovery of metadata identifier failed']);

        return [
            "error" => "Recovery of metadata identifier failed",
            "status" => 422
        ];
    }

    public function publishXml(
        $metadataUuid
    ) {
        $geonetwork = new GeonetworkInterface($this->parameterBag->get('PRO_GEONETWORK_URLBASE'), 'srv/fre/');

        $this->logger->info('publish metadata');
        $response = $geonetwork->put(
            'api/records/' . urlencode($metadataUuid) . '/publish',
            null,
            false
        );

        preg_match('/apiError/', $response, $error);

        if (count($error) === 0) {
            return ['message' => 'the xml modification has been successfully completed', "status" => 200];
        }
        $this->logger->error('patch metadata xml vers admincarto', ['error' => $response[1]]);
        return ['error' => $response[1], 'status' => 404];
    }

    /**
     * @param $metadataUuid
     * @return array
     * renvoie sous forme de tableau les données liés à Data
     */
    public function getItemInfo(
        $metadataUuid
    ): array {
        $geonetwork = new GeonetworkInterface($this->parameterBag->get('PRO_GEONETWORK_URLBASE'), 'srv/fre/');
        $version = "1.0";
        $metadata_doc = new \DOMDocument($version, "UTF-8");
        $metadata_data = $this->getXmlInfo($geonetwork, $metadataUuid);
        if (array_key_exists('error', $metadata_data)) {
            return ['error' => $metadata_data['error'], 'status' => 404];
        }
        if (@$metadata_doc->loadXML($metadata_data['data']) !== false) {
            $xpath = new \DOMXPath($metadata_doc);

            $objXPathTitle = @$xpath->query(
                "/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:citation/gmd:CI_Citation/gmd:title/gco:CharacterString"
            );
            $objXPathAbstract = @$xpath->query(
                "/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:abstract/gco:CharacterString"
            );
            $objXPathLineage = @$xpath->query(
                "/gmd:MD_Metadata/gmd:dataQualityInfo/gmd:DQ_DataQuality/gmd:lineage"
            );
            $objXPathEquivalentScale = @$xpath->query(
                "/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:spatialResolution/gmd:MD_Resolution/gmd:equivalentScale/gmd:MD_RepresentativeFraction/gmd:denominator/gco:Integer"
            );
            if ($objXPathEquivalentScale->length > 0) {
                $equivalentScale = $objXPathEquivalentScale->item(0)->nodeValue;
            } else {
                $equivalentScale = null;
            }

            $objXPathTopicCategory = @$xpath->query(
                "/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:topicCategory/gmd:MD_TopicCategoryCode"
            );

            if ($objXPathTopicCategory->length > 0) {
                $categoryIso = $objXPathTopicCategory->item(0)->nodeValue;
                $categoryIso = ltrim($categoryIso);
                $categoryIso = rtrim($categoryIso);
            }
            $contact = $this->getContact($metadataUuid);

            $themes = $this->getKeywordThesaurus($metadata_doc, "theme");
            $localisation = $this->getKeywordThesaurus($metadata_doc, "place");
            $other = $this->getKeywordThesaurus($metadata_doc, "other");

            $objXPathStatus = @$xpath->query(
                "/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:status"
            );

            if ($objXPathStatus->length > 0) {
                if ($objXPathStatus->item(0)->getAttribute('codeListValue') === "") {
                    $objXPathProgressStatus = @$xpath->query(
                        "/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:status/gmd:MD_ProgressCode"
                    );
                    if ($objXPathProgressStatus->length > 0) {
                        $status = $objXPathProgressStatus->item(0)->getAttribute('codeListValue');
                    }
                } else {
                    $status = $objXPathStatus->item(0)->getAttribute('codeListValue');
                }
            } else {
                $status = null;
            }

            $objXPathMdConstrainte = @$xpath->query(
                "/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:resourceConstraints/gmd:MD_LegalConstraints/gmd:useLimitation/gco:CharacterString"
            );

            if ($objXPathMdConstrainte->length > 0) {
                $mdConstrainte = $objXPathMdConstrainte->item(0)->nodeValue;
            }

            $objXPathMdMaintenance = @$xpath->query(
                "/gmd:MD_Metadata/gmd:metadataMaintenance/gmd:MD_MaintenanceInformation/gmd:maintenanceAndUpdateFrequency/gmd:MD_MaintenanceFrequencyCode"
            );

            if ($objXPathMdMaintenance->length > 0) {
                $mdMaintenance = $objXPathMdMaintenance->item(0)->getAttribute('codeListValue');
            }


            $infos = [
                "themeKeyword" => $themes,
                "localisationKeyword" => $localisation,
                "equivalentScale" => $equivalentScale,
                "contact" => $contact,
                "thematiqueIso" => $categoryIso ?? null,
                "status" => $status,
                'mdConstrainte' => $mdConstrainte ?? null,
                'mdMaintenance' => $mdMaintenance ?? null
            ];

            if ($other !== null) {
                $infos["otherKeyword"] = $other;
            }

            if ($objXPathTitle->length !== 0) {
                $title = ["title" => $objXPathTitle->item(0)->nodeValue];
                $infos = array_merge($infos, $title);
            }

            if ($objXPathAbstract->length !== 0) {
                $abstract = ["abstract" => $objXPathAbstract->item(0)->nodeValue];
                $infos = array_merge($infos, $abstract);
            }

            if ($objXPathLineage->length !== 0) {
                $value = ltrim($objXPathLineage->item(0)->nodeValue);
                $value = rtrim($value);
                $lineage = ["lineage" => $value];
                $infos = array_merge($infos, $lineage);
            }
            return $infos;
        }
        return ['error' => 'xml reading failed', 'status' => 404];
    }

    /**
     * @param $metadata_doc
     * @param $thesaurus
     * @return array|null retourne la liste des thesaurus present dans le xml
     * retourne la liste des thesaurus present dans le xml
     */
    public function getKeywordThesaurus(
        $metadata_doc,
        $thesaurus
    ): ?array {
        $xpath = new \DOMXPath($metadata_doc);
        $objXPathKeyword = @$xpath->query(
            "/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:descriptiveKeywords"
        );
        $keywords = [];
        foreach ($objXPathKeyword as $descriptiveKeyword) {
            if ($thesaurus !== "other") {
                if ($descriptiveKeyword->getElementsByTagName("MD_KeywordTypeCode")->item(0) !== null) {
                    if ($descriptiveKeyword->getElementsByTagName("MD_KeywordTypeCode")->item(0)->getAttribute(
                            'codeListValue'
                        ) === $thesaurus) {
                        $thesaurusName = $descriptiveKeyword->getElementsByTagName("Anchor");

                        if ($thesaurusName !== null && $thesaurusName->length > 0) {
                            preg_match(
                                '/geonetwork.thesaurus.([A-Za-z].+)/',
                                $thesaurusName->item(0)->nodeValue,
                                $name
                            );
                            foreach ($descriptiveKeyword->getElementsByTagName("keyword") as $keyword) {
                                if ($name[1] !== "external.theme.prodige") {
                                    $keyword = ltrim($keyword->nodeValue);
                                    $keyword = rtrim($keyword);
                                    $keywords[$name[1]][] = $keyword;
                                }
                            }
                        }
                    }
                }
            } else {
                if ($descriptiveKeyword->getElementsByTagName("MD_KeywordTypeCode")->item(0) !== null) {
                    if ($descriptiveKeyword->getElementsByTagName("Anchor")->length === 0) {
                        foreach ($descriptiveKeyword->getElementsByTagName("keyword") as $keyword) {
                            $keyword = ltrim($keyword->nodeValue);
                            $keyword = rtrim($keyword);
                            $keywords[$descriptiveKeyword->getElementsByTagName("MD_KeywordTypeCode")->item(
                                0
                            )->getAttribute(
                                'codeListValue'
                            )][] = $keyword;
                        }
                    }
                }
            }
        }

        if (empty($keywords)) {
            $keywords = null;
        }

        return $keywords;
    }

    /**
     * @param Data $data
     * @param string $mode
     * @return array
     * @throws \Exception
     */
    public function metadataWfsService(
        Data $data,
        string $mode
    ): array {
        $geonetwork = new GeonetworkInterface($this->parameterBag->get('PRO_GEONETWORK_URLBASE'), 'srv/fre/');
        $version = "1.0";
        switch ($mode) {
            case "create":
                $proMetadataUuid = PRO_WFS_METADATA_UUID;
                $metadata = $this->getXmlInfo(
                    $geonetwork,
                    $data->getMetadataUuid(),
                    'create'
                )['data'];
                $metadata_doc = new \DOMDocument($version, "UTF-8");
                break;
            case "remove":
                $proMetadataUuid = $this->adminService->getMetadataWfsUuid(
                    $this->adminService->getAdminLayerIdByTableName($data->getTable())['layer']['id']
                )['wfsMetadataUuid'];
                break;
        }

        switch ($mode) {
            case "create":
                $metadata_Wfs = $this->getXmlInfo(
                    $geonetwork,
                    $proMetadataUuid
                )['data'];
                $metadata_doc_Wfs = new \DOMDocument($version, "UTF-8");
                if (@$metadata_doc_Wfs->loadXML($metadata_Wfs) !== false) {
                    if (@$metadata_doc->loadXML($metadata) !== false) {
                        $doc = $this->createMetadataWfs(
                            $metadata_doc_Wfs,
                            $metadata_doc,
                            $data
                        );
                        $new_metadata_Wfs = $doc['metadata_doc_Wfs']->saveXML();
                        $new_metadata = $doc['metadata_doc']->saveXML();
                        $this->patchXml($new_metadata, $data->getMetadataUuid());
                        return $this->createXml($new_metadata_Wfs);
                    }
                }
            case "remove":
                $deleteMetadataWfs = $this->deleteMetadata($proMetadataUuid);
                return ['data' => $proMetadataUuid, 'status' => 201];
        }


        return ['data' => 'le chargement du document a échoué', 'status' => 404];
    }

    /**
     * @param $metadata_doc_Wfs
     * @param $metadata_doc
     * @param Data $data
     * @return array
     */
    public function createMetadataWfs(
        $metadata_doc_Wfs,
        $metadata_doc,
        Data $data
    ): array {
        $xpath = new \DOMXPath($metadata_doc_Wfs);
        if ($metadata_doc_Wfs) {
            // First remove coupledResource and Operateson, ContainsOperations,DistributionInfo
            $objXPath = $xpath->query(
                "/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/srv:coupledResource"
            );
            if ($objXPath->length > 0) {
                foreach ($objXPath as $item) {
                    $item->parentNode->removeChild($item);
                }
            }
            $objXPath = $xpath->query(
                "/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/srv:operatesOn"
            );
            if ($objXPath->length > 0) {
                foreach ($objXPath as $item) {
                    $item->parentNode->removeChild($item);
                }
            }
            $objXPath = @$xpath->query(
                "/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/srv:containsOperations"
            );
            if ($objXPath->length > 0) {
                foreach ($objXPath as $item) {
                    $item->parentNode->removeChild($item);
                }
            }
            $objXPath = $xpath->query(
                "/gmd:MD_Metadata/gmd:distributionInfo/gmd:MD_Distribution/gmd:transferOptions/gmd:MD_DigitalTransferOptions/gmd:onLine"
            );
            if ($objXPath->length > 0) {
                foreach ($objXPath as $item) {
                    $item->parentNode->removeChild($item);
                }
            }
            $objXPath = $xpath->query(
                "/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/gmd:citation/gmd:CI_Citation/gmd:date/gmd:CI_Date/gmd:date/gco:Date"
            );
            if ($objXPath->length > 0) {
                $item = $objXPath->item(0);
                $item->nodeValue = date("Y-m-d");
            }
            //change URI
            $this->assignIdentifier($metadata_doc_Wfs, $xpath, $data->getMetadataUuid());
        }

        if ($metadata_doc_Wfs !== false) {
            $xpath = new \DOMXpath($metadata_doc_Wfs);
            $oXpathMeta = new \DOMXPath($metadata_doc);
            $dataRecopy = array(
                "/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/gmd:citation/gmd:CI_Citation/gmd:title"
                => "/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:citation/gmd:CI_Citation/gmd:title"
            ,
                "/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/gmd:abstract"
                => "/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:abstract"
            ,
                "/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/gmd:pointOfContact"
                => "/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:pointOfContact"
            ,
                "/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/gmd:descriptiveKeywords"
                => "/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:descriptiveKeywords"
            ,
                "/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/srv:extent/gmd:EX_Extent"
                => "/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:extent/gmd:EX_Extent"
            );
            foreach ($dataRecopy as $pathWFS => $pathMeta) {
                $xpathWFS = $xpath->query($pathWFS);
                $xpathMeta = $oXpathMeta->query($pathMeta);
                $index = 0;
                if ($xpathWFS->length > 0) {
                    foreach ($xpathWFS as $itemWFS) {
                        $itemMeta = null;
                        if ($xpathMeta->length > $index) {
                            $itemMeta = $xpathMeta->item($index);
                        }
                        if ($itemMeta) {
                            $itemMeta = $metadata_doc_Wfs->importNode($itemMeta, true);
                            $itemWFS->parentNode->replaceChild($itemMeta, $itemWFS);
                        } else {
                            $itemWFS->parentNode->removeChild($itemWFS);
                        }
                        $index++;
                    }
                }
                if ($xpathMeta->length > $index) {
                    $xpathWFS = $xpath->query(preg_replace("!\/[^/]+$!", "", $pathWFS));
                    for (; $index < $xpathMeta->length; $index++) {
                        foreach ($xpathWFS as $itemWFS) {
                            $itemMeta = $xpathMeta->item($index);
                            $itemMeta = $metadata_doc_Wfs->importNode($itemMeta, true);
                            $itemWFS->appendChild($itemMeta);
                        }
                    }
                }
                $last = explode("/", $pathWFS);
                $last = end($last);
                if (in_array($last, array("gmd:title", "gmd:abstract"))) {
                    $xpathWFS = $xpath->query($pathWFS . "/gco:CharacterString");
                    if ($xpathWFS->length > 0) {
                        foreach ($xpathWFS as $itemWFS) {
                            if ($last === "gmd:title") {
                                $itemWFS->nodeValue = $itemWFS->textContent . " (service WFS)";
                            }
                            if ($last === "gmd:abstract") {
                                $itemWFS->nodeValue = "Service WFS - " . $itemWFS->textContent;
                            }
                        }
                    }
                }
            }
        }

        if ($data->getMetadataUuid() && $data->getLayerName() !== null) {
            $exists = $xpath->query(
                "/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/srv:coupledResource/srv:SV_CoupledResource/gco:ScopedName[text()='" . $data->getLayerName(
                ) . "']"
            );

            if ($exists->length === 0) {
                // add coupledresource, operatesOn
                $strXml = '<tag xmlns:srv="http://www.isotc211.org/2005/srv" xmlns:gco="http://www.isotc211.org/2005/gco">';

                $strXml .= '<srv:coupledResource>
                    <srv:SV_CoupledResource>
                        <srv:operationName>
                            <gco:CharacterString>GetCapabilities</gco:CharacterString>
                        </srv:operationName>
                        <srv:identifier>
                            <gco:CharacterString>' . $data->getMetadataUuid() . '</gco:CharacterString>
                        </srv:identifier>
                        <gco:ScopedName>' . $data->getLayerName() . '</gco:ScopedName>
                    </srv:SV_CoupledResource>
                    </srv:coupledResource>
                    <srv:coupledResource>
                        <srv:SV_CoupledResource>
                            <srv:operationName>
                                <gco:CharacterString>DescribeFeatureType</gco:CharacterString>
                            </srv:operationName>
                            <srv:identifier>
                                <gco:CharacterString>' . $data->getMetadataUuid() . '</gco:CharacterString>
                        </srv:identifier>
                        <gco:ScopedName>' . $data->getLayerName() . '</gco:ScopedName>
                        </srv:SV_CoupledResource>
                    </srv:coupledResource>
                    <srv:coupledResource>
                        <srv:SV_CoupledResource>
                            <srv:operationName>
                                <gco:CharacterString>GetFeature</gco:CharacterString>
                            </srv:operationName>
                            <srv:identifier>
                                <gco:CharacterString>' . $data->getMetadataUuid() . '</gco:CharacterString>
                        </srv:identifier>
                        <gco:ScopedName>' . $data->getLayerName() . '</gco:ScopedName>
                        </srv:SV_CoupledResource>
                    </srv:coupledResource>';
                $strXml .= "</tag>";

                $tagRepport = new \DOMDocument();
                $tagRepport->loadXML($strXml);
                $nodeToImport = $tagRepport->getElementsByTagName("coupledResource")->item(0);

                $coupledResource = $metadata_doc_Wfs->getElementsByTagName("SV_ServiceIdentification");

                if ($coupledResource && $coupledResource->length > 0) {
                    $newNodeDesc = $metadata_doc_Wfs->importNode($nodeToImport, true);

                    if ($metadata_doc_Wfs->getElementsByTagName(
                            "coupledResource"
                        ) && $metadata_doc_Wfs->getElementsByTagName(
                            "coupledResource"
                        )->length > 0) {
                        $firstCoupledResource = $metadata_doc_Wfs->getElementsByTagName("coupledResource")->item(0);
                        $firstCoupledResource->parentNode->insertBefore($newNodeDesc, $firstCoupledResource);
                    } else {
                        $coupledResource->item(0)->appendChild($newNodeDesc);
                    }
                }
            }
            $itAlreadyExists = $metadata_doc->getElementsByTagName("distributionInfo");
            if ($itAlreadyExists->length === 0) {
                $racine = $metadata_doc->getElementsByTagName("MD_Metadata");
                $createStrXML =
                    '<gmd:MD_Metadata xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns="http://www.isotc211.org/2005/gmd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:fra="http://www.cnig.gouv.fr/2005/fra" xmlns:gco="http://www.isotc211.org/2005/gco" xmlns:gts="http://www.isotc211.org/2005/gts" xmlns:gml="http://www.opengis.net/gml" xmlns:gfc="http://www.isotc211.org/2005/gfc" xmlns:gmx="http://www.isotc211.org/2005/gmx" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:gmi="http://www.isotc211.org/2005/gmi">
                                      <gmd:distributionInfo>
                                        <gmd:MD_Distribution>
                                          <gmd:distributionFormat>
                                            <gmd:MD_Format>
                                              <gmd:name>
                                                <gco:CharacterString>ZIP</gco:CharacterString>
                                              </gmd:name>
                                              <gmd:version gco:nilReason="missing">
                                                <gco:CharacterString/>
                                              </gmd:version>
                                            </gmd:MD_Format>
                                          </gmd:distributionFormat>
                                          <gmd:transferOptions>
                                            <gmd:MD_DigitalTransferOptions>
                                                 <gmd:onLine>
                                                   <gmd:CI_OnlineResource>
                                                     <gmd:linkage>
                                                       <gmd:URL>' . $this->parameterBag->get(
                        'PRODIGE_URL_DATACARTO'
                    ) . "/wfs?service=WFS&request=GetCapabilities" . '</gmd:URL>
                                                     </gmd:linkage>
                                                     <gmd:protocol>
                                                       <gco:CharacterString>' . "OGC:WFS-1.0.0-http-get-capabilities" . '</gco:CharacterString>
                                                     </gmd:protocol>';
                if ($data->getTable() !== null) {
                    $nameStrXml =
                        '<gmd:name>
                               <gco:CharacterString>' . $data->getTable() . '</gco:CharacterString>
                         </gmd:name>';


                    $createStrXML .= $nameStrXml;
                }

                $endStrXml =
                    '</gmd:CI_OnlineResource>
                               </gmd:onLine>
                             </gmd:MD_DigitalTransferOptions>
                        </gmd:transferOptions>
                      </gmd:MD_Distribution>
                    </gmd:distributionInfo></gmd:MD_Metadata>';

                $createStrXML .= $endStrXml;

                $orgDoc = new \DOMDocument;
                $orgDoc->loadXML($createStrXML);
                $nodeToImport = $orgDoc->getElementsByTagName("distributionInfo")->item(0);
                $newNodeDesc = $metadata_doc->importNode($nodeToImport, true);
                $dataQualityInfo = $racine->item(0)->getElementsByTagName("dataQualityInfo");
                if ($dataQualityInfo && $dataQualityInfo->length > 0) {
                    $racine->item(0)->insertBefore($newNodeDesc, $dataQualityInfo->item(0));
                } else {
                    $racine->item(0)->appendChild($newNodeDesc);
                }
            }
            $exists = $xpath->query(
                "/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/srv:operatesOn[@uuidref='" . $data->getMetadataUuid(
                ) . "']"
            );

            if ($exists->length == 0) {
                $strXmlOp = "<tag xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:srv=\"http://www.isotc211.org/2005/srv\">";
                $strXmlOp .= '<srv:operatesOn uuidref="' . $data->getMetadataUuid() . '" xlink:href="' . rtrim(
                        $this->parameterBag->get('PRO_GEONETWORK_URLBASE'),
                        "/"
                    ) . '/srv/' . $data->getMetadataUuid() . '" />';
                $strXmlOp .= "</tag>";

                $tagRepport = new \DOMDocument();
                $tagRepport->loadXML($strXmlOp);
                $nodes = $tagRepport->getElementsByTagName("operatesOn");
                $serviceIdentification = $metadata_doc_Wfs->getElementsByTagName("SV_ServiceIdentification");
                for ($c = 0; $c < $nodes->length; $c++) {
                    $nodeToImport = $nodes->item($c);
                    if ($serviceIdentification && $serviceIdentification->length > 0) {
                        $newNodeDesc = $metadata_doc_Wfs->importNode($nodeToImport, true);
                        if ($metadata_doc_Wfs->getElementsByTagName(
                                "operatesOn"
                            ) && $metadata_doc_Wfs->getElementsByTagName(
                                "operatesOn"
                            )->length > 0) {
                            $firstOperatesOn = $metadata_doc_Wfs->getElementsByTagName("operatesOn")->item(0);
                            $firstOperatesOn->parentNode->insertBefore($newNodeDesc, $firstOperatesOn);
                        } else {
                            $serviceIdentification->item(0)->appendChild($newNodeDesc);
                        }
                    }
                }
            }
        }
        return ['metadata_doc_Wfs' => $metadata_doc_Wfs, 'metadata_doc' => $metadata_doc];
    }

    /**
     * Assign URI
     * @param  $doc
     * @param  $xpath
     * @param  $uuid
     */
    private
    function assignIdentifier(
        $doc,
        $xpath,
        $uuid
    ): void {
        $identifier = @$xpath->query(
            "/gmd:MD_Metadata/gmd:identificationInfo/*/gmd:citation/gmd:CI_Citation/gmd:identifier/gmd:MD_Identifier/gmd:code/gco:CharacterString"
        );
        if ($identifier && $identifier->length > 0) {
            $identifier->item(0)->nodeValue = $this->parameterBag->get('PRO_GEONETWORK_URLBASE') . "srv/" . $uuid;
        } else {
            $CI_Citation = @$xpath->query(
                "/gmd:MD_Metadata/gmd:identificationInfo/*/gmd:citation/gmd:CI_Citation/gmd:citedResponsibleParty"
            );
            if ($CI_Citation->length > 0) {
                $new_identifier = $doc->createElement('gmd:identifier');
                $gmdMD_Identifier = $doc->createElement('gmd:MD_Identifier');
                $gmdcode = $doc->createElement('gmd:code');
                $gcoCharacterString = $doc->createElement('gco:CharacterString');
                $CI_Citation->item(0)->parentNode->insertBefore($new_identifier, $CI_Citation->item(0));
                $new_identifier->appendChild($gmdMD_Identifier);
                $gmdMD_Identifier->appendChild($gmdcode);
                $gmdcode->appendChild($gcoCharacterString);
                $gcoCharacterString->nodeValue = $this->parameterBag->get('PRO_GEONETWORK_URLBASE') . "srv/" . $uuid;
            } else {
                //le tag citedResponsibleParty n'existe pas
                $CI_Citation = @$xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/*/gmd:citation/gmd:CI_Citation");
                if ($CI_Citation->length > 0) {
                    $new_identifier = $doc->createElement('gmd:identifier');
                    $gmdMD_Identifier = $doc->createElement('gmd:MD_Identifier');
                    $gmdcode = $doc->createElement('gmd:code');
                    $gcoCharacterString = $doc->createElement('gco:CharacterString');
                    $CI_Citation->item(0)->appendChild($new_identifier);
                    $new_identifier->appendChild($gmdMD_Identifier);
                    $gmdMD_Identifier->appendChild($gmdcode);
                    $gmdcode->appendChild($gcoCharacterString);
                    $gcoCharacterString->nodeValue = $this->parameterBag->get(
                            'PRO_GEONETWORK_URLBASE'
                        ) . "srv/" . $uuid;
                }
            }
        }
    }

    /**
     * @param $uuid
     * @return array
     */
    public function deleteMetadata(
        $uuid
    ): array {
        $geonetwork = new GeonetworkInterface($this->parameterBag->get('PRO_GEONETWORK_URLBASE'), 'srv/fre/');
        $response = $geonetwork->delete(
            'api/records/' . urlencode($uuid) . '?withBackup=true',
            false,
        );

        if ($response === "") {
            return ['error' => "xml not found", 'status' => 404];
        }
        return ['data' => $response, 'status' => 200];
    }

    /**
     * @param Data $data
     * @return array
     */
    public function deleteReference(
        Data $data
    ): array {
        $geonetwork = new GeonetworkInterface($this->parameterBag->get('PRO_GEONETWORK_URLBASE'), 'srv/fre/');
        $services = [
            "Téléchargement libre" => PRO_DOWNLOAD_METADATA_UUID,
            "Service WMS" => PRO_WMS_METADATA_UUID,
            "Service WFS" => PRO_WFS_METADATA_UUID,
        ];

        $getReference = $geonetwork->getMetadataRelations($data->getMetadataUuid(), null, "json");

        // ajouter un controle si il n'en a pas
        if ($getReference !== "") {
            $relations = json_decode($getReference, JSON_OBJECT_AS_ARRAY)['services'];
        } else {
            $relations = null;
        }

        if ($relations !== null) {
            foreach ($relations as $service) {
                if (in_array($service['id'], $services, true)) {
                    switch ($service['title']['eng']) {
                        case "Service WMS":
                        case "Service WFS":
                            if (preg_match('/Service ([a-zA-Z]{3})/', $service['title']['eng'], $match) !== 0) {
                                $wxs = strtolower($match[1]);
                                $response = $this->actionMetadataInWxSService(
                                    $data->getMetadataUuid(),
                                    $data->getLayerName(),
                                    "remove",
                                    $wxs
                                );
                                if (array_key_exists("error", $response)) {
                                    return [
                                        "error" => $response['error'],
                                        "status" => $response['status']
                                    ];
                                }
                            }
                            break;
                        case "Service de téléchargement simple de la plateforme":
                            $response = $this->actionMetadataInSimpleDownloadService(
                                $data->getMetadataUuid(),
                                "remove"
                            );
                            if (array_key_exists("error", $response)) {
                                return [
                                    "error" => $response['error'],
                                    "status" => $response['status']
                                ];
                            }
                            break;
                    }
                }
            }

            return [
                'data' => $response['message'] ?? 'pas de service associés',
                "status" => $response['status'] ?? 201
            ];
        }
        return ['data' => 'pas de réference trouvée', "status" => 200];
    }

    /**
     * @param string $metadataUiid
     * @param string $layerName
     * inserer ou retire la reference de la metadonnée de la fiche du service wms
     * @param string $mode
     * "add" or "remove"
     * @param string $wxs
     * "wfs" ou "wms"
     * @return array
     */
    public function actionMetadataInWxSService(
        string $metadataUiid,
        string $layerName,
        string $mode,
        string $wxs
    ): array {
        $this->baseController->getGeonetworkInterface(true);
        $geonetwork = new GeonetworkInterface($this->parameterBag->get('PRO_GEONETWORK_URLBASE'), 'srv/fre/');

        switch ($wxs) {
            case "wms":
                $proMetadataUuid = PRO_WMS_METADATA_UUID;
                break;
            case "wfs":
                $proMetadataUuid = PRO_WFS_METADATA_UUID;
                break;
        }
        $version = "1.0";
        $metadata_data_Service = $this->getXmlInfo(
            $geonetwork,
            $proMetadataUuid
        )['data'];
        $metadata_doc_service = new \DOMDocument($version, "UTF-8");
        if (@$metadata_doc_service->loadXML($metadata_data_Service) !== false) {
            $xpath = new \DOMXpath($metadata_doc_service);
            switch ($mode) {
                case "add":
                    $ifExist = @$xpath->query(
                        "/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/srv:operatesOn[@uuidref=\"" . $metadataUiid . "\"]"
                    );
                    if ($ifExist && $ifExist->length === 0) {
                        $response = $this->addMetadataWxSService($metadataUiid, $layerName, $metadata_doc_service);
                        if (array_key_exists('error', $response)) {
                            return ['error' => 'insertion failed', 'status' => 400];
                        }
                        $metadata_doc_service = $response['data'];
                    }
                    break;
                case "remove":
                    $ifExist = @$xpath->query(
                        "/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/srv:operatesOn[@uuidref=\"" . $metadataUiid . "\"]"
                    );
                    if ($ifExist && $ifExist->length > 0) {
                        $response = $this->removeMetadataWxSService($metadataUiid, $layerName, $metadata_doc_service);
                        $metadata_doc_service = $response['data'];
                    }
                    break;
            }

            $new_metadata_data = $metadata_doc_service->saveXML();
            $this->baseController->getGeonetworkInterface(false);
            $callbackReturn = $this->patchXml($new_metadata_data, $proMetadataUuid);

            return $callbackReturn;
        }

        return ['error' => 'xml reading failed', 'status' => 404];
    }

    /**
     * @param $metadataUiid
     * @param $layerName
     * @param $metadata_doc_service
     * @return array
     */
    public function addMetadataWxSService(
        $metadataUiid,
        $layerName,
        $metadata_doc_service
    ): array {
        $xpath = new \DOMXpath($metadata_doc_service);
        $srvIdentificationInfo = $xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification");
        if ($srvIdentificationInfo->length > 0) {
            $srvIdentifier = $xpath->query(
                "/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/srv:coupledResource/srv:SV_CoupledResource/srv:identifier/gco:CharacterString"
            );

            foreach ($srvIdentifier as $item) {
                $ScopedNames = $item->parentNode->parentNode->getElementsByTagName("ScopedName");
                if ($ScopedNames && $ScopedNames->length > 0) {
                    $ScopedName = $ScopedNames->item(0)->nodeValue;
                    if ($item->nodeValue === $metadataUiid && $ScopedName === $layerName) {
                        // mode modification, suppression puis recréation
                        $item->parentNode->parentNode->parentNode->parentNode->removeChild(
                            $item->parentNode->parentNode->parentNode
                        );
                    }
                }
            }

            $strXML = "<tag xmlns:gmd=\"http://www.isotc211.org/2005/gmd\" xmlns:gfc=\"http://www.isotc211.org/2005/gfc\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:gco=\"http://www.isotc211.org/2005/gco\" xmlns:gml=\"http://www.opengis.net/gml\" xmlns:srv=\"http://www.isotc211.org/2005/srv\" xmlns:gts=\"http://www.isotc211.org/2005/gts\" xmlns:gmx=\"http://www.isotc211.org/2005/gmx\" xmlns:geonet=\"http://www.fao.org/geonetwork\"><srv:coupledResource>
              <srv:SV_CoupledResource>
                <srv:operationName>
                  <gco:CharacterString>GETMAP</gco:CharacterString>
                </srv:operationName>
                <srv:identifier>
                  <gco:CharacterString>" . $metadataUiid . "</gco:CharacterString>
                </srv:identifier>
                <gco:ScopedName>" . $layerName . "</gco:ScopedName>
              </srv:SV_CoupledResource>
            </srv:coupledResource></tag>";

            $orgDoc = new \DOMDocument();
            $orgDoc->loadXML($strXML);
            $nodeToImport = $orgDoc->getElementsByTagName("coupledResource")->item(0);
            $newNodeDesc = $metadata_doc_service->importNode($nodeToImport, true);

            if ($metadata_doc_service->getElementsByTagName(
                    "coupledResource"
                ) && $metadata_doc_service->getElementsByTagName(
                    "coupledResource"
                )->length > 0) {
                $firstCoupledResource = $metadata_doc_service->getElementsByTagName("coupledResource")->item(0);
                $firstCoupledResource->parentNode->insertBefore($newNodeDesc, $firstCoupledResource);
            } else {
                $srvIdentificationInfo->item(0)->appendChild($newNodeDesc);
            }
            $objXPath = @$xpath->query("/gmd:MD_Metadata");
            if ($objXPath && $objXPath->length > 0) {
                $strXml = "<srv:operatesOn xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:srv=\"http://www.isotc211.org/2005/srv\"  uuidref=\"" . $metadataUiid . "\" xlink:href=\"" . rtrim(
                        $this->parameterBag->get('PRO_GEONETWORK_URLBASE'),
                        "/"
                    ) . "/srv/metadata/" . $metadataUiid . "\"/>";
                $tagSrvOperates = new \DOMDocument;
                $tagSrvOperates->loadXML($strXml);
                $nodeToImport = $tagSrvOperates->getElementsByTagName("operatesOn")->item(0);
                $tagIdentificationInfo = $objXPath->item(0)->getElementsByTagName(
                    "SV_ServiceIdentification"
                );
                if ($tagIdentificationInfo->length > 0) {
                    $newNodeDesc = $metadata_doc_service->importNode($nodeToImport, true);
                    if ($metadata_doc_service->getElementsByTagName(
                            "operatesOn"
                        ) && $metadata_doc_service->getElementsByTagName("operatesOn")->length > 0) {
                        $firstOperatesOn = $metadata_doc_service->getElementsByTagName("operatesOn")->item(
                            0
                        );
                        if ($firstOperatesOn->getAttribute('uuidref') !== $metadataUiid) {
                            $firstOperatesOn->parentNode->insertBefore($newNodeDesc, $firstOperatesOn);
                        }
                    } else {
                        $tagIdentificationInfo->item(0)->appendChild($newNodeDesc);
                    }
                }
            }

            return ['data' => $metadata_doc_service, 'status' => 200];
        }
        return ['error' => 'insertion failed', 'status' => 400];
    }

    /**
     * @param $metadataUiid
     * @param $layerName
     * @param $metadata_doc_service
     * @return array
     */
    public function removeMetadataWxSService(
        $metadataUiid,
        $layerName,
        $metadata_doc_service
    ): array {
        $xpath = new \DOMXPath($metadata_doc_service);
        $bHasOtherLayerForUuid = false;
        $srvIdentificationInfo = $xpath->query(
            "/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/srv:coupledResource/srv:SV_CoupledResource/srv:identifier/gco:CharacterString"
        );
        if ($srvIdentificationInfo->length > 0) {
            foreach ($srvIdentificationInfo as $item) {
                $ScopedNames = $item->parentNode->parentNode->getElementsByTagName("ScopedName");
                if ($ScopedNames && $ScopedNames->length > 0) {
                    $ScopedName = $ScopedNames->item(0)->nodeValue;
                    if ($item->nodeValue === $metadataUiid) {
                        if ($ScopedName === $layerName) {
                            $item->parentNode->parentNode->parentNode->parentNode->removeChild(
                                $item->parentNode->parentNode->parentNode
                            );
                        } else {
                            $bHasOtherLayerForUuid = true;
                        }
                    }
                }
            }
        }

        if (!$bHasOtherLayerForUuid) {
            $srvOperatesOn = $xpath->query(
                "/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/srv:operatesOn"
            );
            if ($srvOperatesOn->length > 0) {
                foreach ($srvOperatesOn as $item) {
                    $layerIdentifier = $item->getAttribute("uuidref");
                    if ($layerIdentifier === $metadataUiid) {
                        $item->parentNode->removeChild($item);
                    }
                }
            }
        }

        return ['data' => $metadata_doc_service, 'status' => 200];
    }

    /**
     * @param $metadataUiid
     * inserer ou retire la référence de la metadonnée dans la fiche de telechargement simple
     * @param $mode
     * "add" ou "remove"
     * @return array|void
     */
    public function actionMetadataInSimpleDownloadService(
        $metadataUiid,
        $mode
    ) {
        $this->baseController->getGeonetworkInterface(true);
        $geonetwork = new GeonetworkInterface($this->parameterBag->get('PRO_GEONETWORK_URLBASE'), 'srv/fre/');
        $proMetadataUuid = PRO_DOWNLOAD_METADATA_UUID;

        $version = "1.0";
        $metadata_data_Service = $this->getXmlInfo(
            $geonetwork,
            $proMetadataUuid
        )['data'];
        $metadata_doc_service = new \DOMDocument($version, "UTF-8");
        if (@$metadata_doc_service->loadXML($metadata_data_Service) !== false) {
            $xpath = new \DOMXpath($metadata_doc_service);
            switch ($mode) {
                case "add":
                    $ifExist = @$xpath->query(
                        "/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/srv:operatesOn[@uuidref=\"" . $metadataUiid . "\"]"
                    );
                    if ($ifExist && $ifExist->length === 0) {
                        $objXPath = @$xpath->query("/gmd:MD_Metadata");
                        if ($objXPath && $objXPath->length > 0) {
                            $strXml = "<srv:operatesOn xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:srv=\"http://www.isotc211.org/2005/srv\"  uuidref=\"" . $metadataUiid . "\" xlink:href=\"" . rtrim(
                                    $this->parameterBag->get('PRO_GEONETWORK_URLBASE'),
                                    "/"
                                ) . "/srv/metadata/" . $metadataUiid . "\"/>";
                            $tagSrvOperates = new \DOMDocument;
                            $tagSrvOperates->loadXML($strXml);
                            $nodeToImport = $tagSrvOperates->getElementsByTagName("operatesOn")->item(0);
                            $tagIdentificationInfo = $objXPath->item(0)->getElementsByTagName(
                                "SV_ServiceIdentification"
                            );
                            if ($tagIdentificationInfo && $tagIdentificationInfo->length > 0) {
                                $newNodeDesc = $metadata_doc_service->importNode($nodeToImport, true);
                                if ($metadata_doc_service->getElementsByTagName(
                                        "operatesOn"
                                    ) && $metadata_doc_service->getElementsByTagName("operatesOn")->length > 0) {
                                    $firstOperatesOn = $metadata_doc_service->getElementsByTagName("operatesOn")->item(
                                        0
                                    );
                                    $firstOperatesOn->parentNode->insertBefore($newNodeDesc, $firstOperatesOn);
                                } else {
                                    $tagIdentificationInfo->item(0)->appendChild($newNodeDesc);
                                }
                            }
                        }
                    }
                    break;
                case "remove":
                    $nodeToDelete = @$xpath->query(
                        "/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/srv:operatesOn[@uuidref=\"" . $metadataUiid . "\"]"
                    );
                    if ($nodeToDelete->length > 0) {
                        foreach ($nodeToDelete as $key => $node) {
                            foreach ($node->attributes as $attr) {
                                if ($attr->localName === "uuidref" && $attr->nodeValue === $metadataUiid) {
                                    $node->parentNode->removeChild($node);
                                }
                            }
                        }
                    }
                    break;
            }
            $new_metadata_data = $metadata_doc_service->saveXML();
            $this->baseController->getGeonetworkInterface(false);
            $callbackReturn = $this->patchXml($new_metadata_data, $proMetadataUuid);

            return $callbackReturn;
        }
    }

    public function deletingAssociatedFiles(
        Data $data
    ) {
        $this->baseController->getGeonetworkInterface(true);
        $geonetwork = new GeonetworkInterface($this->parameterBag->get('PRO_GEONETWORK_URLBASE'), 'srv/fre/');

        $getMetadata = $geonetwork->getMetadataRelations($data->getMetadataUuid(), null, "json");
        if ($getMetadata !== "") {
            $relations = json_decode($getMetadata, JSON_OBJECT_AS_ARRAY)['services'];
        } else {
            $relations = null;
        }

        $this->baseController->getGeonetworkInterface(false);
        if ($relations !== null) {
            foreach ($relations as $metadata) {
                $deleted = $this->deleteMetadata($metadata['id']);
                if (array_key_exists("error", $deleted)) {
                    return [
                        "error" => $deleted['error'],
                        "status" => $deleted['status']
                    ];
                }
            }

            return ['data' => $deleted['data'], "status" => $deleted['status']];
        }

        return ['data' => 'pas de réference trouvée', "status" => 200];
    }

    /**
     * @param Data $data
     * @return array
     */
    public function changeLayerName(
        Data $data
    ): array {
        $this->baseController->getGeonetworkInterface(true);
        $geonetwork = new GeonetworkInterface($this->parameterBag->get('PRO_GEONETWORK_URLBASE'), 'srv/fre/');

        $allLayers = [
            "wms" => PRO_WMS_METADATA_UUID,
            "wfs" => PRO_WFS_METADATA_UUID,
            "metadatawfs" => $data->getWfsMetadataUuid()
        ];

        foreach ($allLayers as $key => $layerIn) {
            $xml = $this->getXmlInfo($geonetwork, $layerIn);
            $metadata_data = $xml['data'];
            if (in_array($key, ['wms', 'wfs'])) {
                $reponse = $this->changeNameInService($data, $metadata_data, $layerIn);
            } else {
                $reponse = $this->changeNameInMetadata($data, $metadata_data, $layerIn);
            }
        }
        $this->baseController->getGeonetworkInterface(false);
        return $reponse;
    }

    /**
     * @param Data $data
     * @param string $metadata_data
     * @param string $layerIn
     * @return array
     */
    public function changeNameInService(
        Data $data,
        string $metadata_data,
        string $layerIn
    ): array {
        $version = "1.0";
        $metadata_doc = new \DOMDocument($version, "UTF-8");
        if (@$metadata_doc->loadXML($metadata_data) !== false) {
            $xpath = new \DOMXPath($metadata_doc);

            $srvIdentificationInfo = $xpath->query(
                "/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/srv:coupledResource/srv:SV_CoupledResource/srv:identifier/gco:CharacterString"
            );
            if ($srvIdentificationInfo->length > 0) {
                foreach ($srvIdentificationInfo as $item) {
                    $identifier = $item->parentNode->parentNode->getElementsByTagName("identifier");
                    if ($identifier->item(0)->getElementsByTagName("CharacterString")->item(
                            0
                        )->nodeValue === $data->getMetadataUuid()) {
                        $ScopedNames = $item->parentNode->parentNode->getElementsByTagName("ScopedName");
                        if ($ScopedNames && $ScopedNames->length > 0) {
                            $ScopedName = $ScopedNames->item(0);
                            $ScopedName->nodeValue = $data->getTable();
                        }
                    }
                }
            }
        }
        $new_metadata_doc = $metadata_doc->saveXML();
        return $this->patchXml($new_metadata_doc, $layerIn);
    }

    /**
     * @param Data $data
     * @param string $metadata_data
     * @param string $layerIn
     * @return array
     */
    public function changeNameInMetadata(
        Data $data,
        string $metadata_data,
        string $layerIn
    ): array {
        $version = "1.0";
        $metadata_doc = new \DOMDocument($version, "UTF-8");
        if (@$metadata_doc->loadXML($metadata_data) !== false) {
            $xpath = new \DOMXPath($metadata_doc);

            $objXPathTitle = @$xpath->query(
                "/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/gmd:citation/gmd:CI_Citation/gmd:title/gco:CharacterString"
            );
            if ($objXPathTitle->length > 0) {
                $element = $objXPathTitle->item(0);
                $element->nodeValue = $data->getTitle() . ' (service WFS)';
            }
        }
        $new_metadata_doc = $metadata_doc->saveXML();
        return $this->patchXml($new_metadata_doc, $layerIn);
    }

    /**
     * @param Data $data
     * @return array
     * @throws \Exception
     */
    public function layerNameInWfsMetadata(
        Data $data
    ): array {
        $geonetwork = new GeonetworkInterface($this->parameterBag->get('PRO_GEONETWORK_URLBASE'), 'srv/fre/');
        $proMetadataUuid = $this->adminService->getMetadataWfsUuid(
            $this->adminService->getAdminLayerIdByTableName($data->getTable())['layer']['id']
        )['wfsMetadataUuid'];

        $metadata_Wfs = $this->getXmlInfo(
            $geonetwork,
            $proMetadataUuid
        )['data'];
        $version = "1.0";
        $metadata_doc_Wfs = new \DOMDocument($version, "UTF-8");
        if (@$metadata_doc_Wfs->loadXML($metadata_Wfs) !== false) {
            $xpath = new \DOMXPath($metadata_doc_Wfs);
            $coupledResource = $xpath->query(
                "/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/srv:coupledResource/srv:SV_CoupledResource/gco:ScopedName"
            );
            if ($coupledResource->length > 0) {
                if ($coupledResource->item(0)->nodeValue !== $data->getTable()) {
                    $coupledResource->item(0)->nodeValue = $data->getTable();
                }
                $new_metadata = $metadata_doc_Wfs->saveXML();
                return $this->patchXml($new_metadata, $proMetadataUuid);
            }
            return ['data' => "the layer's name is rigth", 'status' => 200];
        }

        return ['error' => 'xml not found', 'status' => 404];
    }

    public function updateDomSDomMetadata(
        Data $data
    ) {
        $getPublicId = $this->getPublicId($data);
        if (array_key_exists('error', $getPublicId)) {
            return ['error' => 'public_id not found', 'status' => 404];
        } else {
            $public_id = $getPublicId['data'];
        }
        //update representationType in metadata

        $updateDomSdom = new UpdateDomSdomController();
        $updateDomSdom->setContainer($this->container);
        $request = new Request();
        $request->query->add(
            ["mode" => "couche"]
        );

        $request->request->add(['sdom' => $data->getSubdomains()]);

        $response = $updateDomSdom->updateDomSdomAction($request, $public_id, 'couche');

        if ($response->getContent() !== '{"success":true}') {
            return ['error' => 'subdomain update failed', 'status' => 404];
        }

        return ["data" => "subdomain updated", "status" => 200];
    }

    public function getPublicId(
        Data $data
    ) {
        $this->logger->info('Get public id from admin');
        $url = $this->parameterBag->get('PRODIGE_URL_ADMIN') . '/api/metadata_sheets/' . $data->getMetadataSheetId();

        $jsonResponse = $this->adminClientService->curlAdminWithAdmincli($url, 'GET');

        if ($jsonResponse === null) {
            $this->logger->error('curl get public id error ', ['error' => "Le serveur d'admin ne répond pas"]);
            return ["error" => "Le serveur d'admin ne répond pas", "status" => 409];
        }
        if ($jsonResponse["@type"] === "hydra:Error") {
            return ["error" => $jsonResponse["hydra:description"], "status" => 404];
        }
        if ($jsonResponse["@type"] === "MetadataSheet") {
            return ["data" => $jsonResponse['publicMetadataId'], "status" => 200];
        }
        return ["error" => 'echec de la demande', "status" => 409];
    }

    public function getExtentForXml(
        array $getExtent,
        int $srs
    ) {
        $conn = $this->connection;
        $sqlMinX = "SELECT ST_X(st_transform(ST_PointFromText('POINT(" . $getExtent['xmin'] . " " . $getExtent['ymin'] . ")', " . $srs . "), 4326)) AS xmin," .
            "ST_Y(st_transform(ST_PointFromText('POINT(" . $getExtent['xmin'] . " " . $getExtent['ymin'] . ")', " . $srs . "), 4326)) AS ymin," .
            "ST_X(st_transform(ST_PointFromText('POINT(" . $getExtent['xmax'] . " " . $getExtent['ymax'] . ")', " . $srs . "), 4326)) AS xmax," .
            "ST_Y(st_transform(ST_PointFromText('POINT(" . $getExtent['xmax'] . " " . $getExtent['ymax'] . ")', " . $srs . "), 4326)) AS ymax;";
        $stmt = $conn->prepare($sqlMinX);
        $response = $stmt->executeQuery();
        $result = $response->fetchAllAssociative();
        $extent = [];
        foreach ($result[0] as $key => $value) {
            $extent[$key] = $value;
        }
        return $extent;
    }

    public function addServiceInMetadataServiceWfs(
        $wfsUuid,
        $uuid,
        $user
    ) {
        $geonetwork = new GeonetworkInterface($this->parameterBag->get('PRO_GEONETWORK_URLBASE'), 'srv/fre/');

        $version = "1.0";
        $metadata_wfs_doc = $this->getXmlInfo(
            $geonetwork,
            $wfsUuid
        )['data'];
        $metadata_wfs_load = new \DOMDocument($version, "UTF-8");
        if (@$metadata_wfs_load->loadXML($metadata_wfs_doc) !== false) {
            $strXml['GetCapabilities'] = "<tag xmlns:gmd=\"http://www.isotc211.org/2005/gmd\" xmlns:gco=\"http://www.isotc211.org/2005/gco\" xmlns:srv=\"http://www.isotc211.org/2005/srv\">
                 <srv:containsOperations>
                  <srv:SV_OperationMetadata>
                    <srv:operationName>
                      <gco:CharacterString>GetCapabilities</gco:CharacterString>
                    </srv:operationName>
                    <srv:DCP>
                      <srv:DCPList codeList=\"http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#DCPList\" codeListValue=\"WebServices\"/>
                    </srv:DCP>
                    <srv:connectPoint>
                      <gmd:CI_OnlineResource>
                        <gmd:linkage>
                          <gmd:URL>" . $this->parameterBag->get(
                    'PRODIGE_URL_DATACARTO'
                ) . "/wfs/" . $wfsUuid . "?service=WFS&amp;request=GetCapabilities" . "</gmd:URL>
                        </gmd:linkage>
                        <gmd:protocol>
                          <gco:CharacterString>OGC:WFS</gco:CharacterString>
                        </gmd:protocol>
                      </gmd:CI_OnlineResource>
                    </srv:connectPoint>
                  </srv:SV_OperationMetadata>
                </srv:containsOperations>
                </tag>";

            $strXml['DescribeFeatureType'] = "<tag xmlns:gmd=\"http://www.isotc211.org/2005/gmd\" xmlns:gco=\"http://www.isotc211.org/2005/gco\" xmlns:srv=\"http://www.isotc211.org/2005/srv\">
                <srv:containsOperations>
                  <srv:SV_OperationMetadata>
                    <srv:operationName>
                      <gco:CharacterString>DescribeFeatureType</gco:CharacterString>
                    </srv:operationName>
                    <srv:DCP>
                      <srv:DCPList codeList=\"http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#DCPList\" codeListValue=\"WebServices\"/>
                    </srv:DCP>
                    <srv:connectPoint>
                      <gmd:CI_OnlineResource>
                        <gmd:linkage>
                          <gmd:URL>" . $this->parameterBag->get(
                    'PRODIGE_URL_DATACARTO'
                ) . "/wfs/" . $wfsUuid . "?service=WFS&amp;request=GetCapabilities" . "</gmd:URL>
                        </gmd:linkage>
                        <gmd:protocol>
                          <gco:CharacterString>OGC:WFS</gco:CharacterString>
                        </gmd:protocol>
                      </gmd:CI_OnlineResource>
                    </srv:connectPoint>
                  </srv:SV_OperationMetadata>
                </srv:containsOperations></tag>";

            $strXml['GetFeature'] = "<tag xmlns:gmd=\"http://www.isotc211.org/2005/gmd\" xmlns:gco=\"http://www.isotc211.org/2005/gco\" xmlns:srv=\"http://www.isotc211.org/2005/srv\">
                    <srv:containsOperations>
                  <srv:SV_OperationMetadata>
                    <srv:operationName>
                      <gco:CharacterString>GetFeature</gco:CharacterString>
                    </srv:operationName>
                    <srv:DCP>
                      <srv:DCPList codeList=\"http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#DCPList\" codeListValue=\"WebServices\"/>
                    </srv:DCP>
                    <srv:connectPoint>
                      <gmd:CI_OnlineResource>
                        <gmd:linkage>
                          <gmd:URL>" . $this->parameterBag->get(
                    'PRODIGE_URL_DATACARTO'
                ) . "/wfs/" . $wfsUuid . "?service=WFS&amp;request=GetCapabilities" . "</gmd:URL>
                        </gmd:linkage>
                        <gmd:protocol>
                          <gco:CharacterString>OGC:WFS</gco:CharacterString>
                        </gmd:protocol>
                      </gmd:CI_OnlineResource>
                    </srv:connectPoint>
                  </srv:SV_OperationMetadata>
                </srv:containsOperations></tag>";

            if ($user) {
                $xpath = new \DOMXPath($metadata_wfs_load);
                $userIdentifier = @$xpath->query(
                    "/gmd:MD_Metadata/gmd:contact/gmd:CI_ResponsibleParty/gmd:organisationName/gco:CharacterString"
                );
                if ($userIdentifier->length > 0) {
                    $element = $userIdentifier->item(0);
                    $element->nodeValue = $user['name'] . ' ' . $user['firstName'];
                }

                $userMail = @$xpath->query(
                    "/gmd:MD_Metadata/gmd:contact/gmd:CI_ResponsibleParty/gmd:contactInfo/gmd:CI_Contact/gmd:address/gmd:CI_Address/gmd:electronicMailAddress/gco:CharacterString"
                );
                if ($userMail->length > 0) {
                    $element = $userMail->item(0);
                    $element->nodeValue = $user['email'];
                }
            }

            foreach ($strXml as $fragment) {
                $service = new \DOMDocument($version, "UTF-8");
                $service->loadXML($fragment);

                $nodeToImport = $service->getElementsByTagName("containsOperations")->item(0);
                $newNodeDesc = $metadata_wfs_load->importNode($nodeToImport, true);
                $insertIn = $metadata_wfs_load->getElementsByTagName(
                    "SV_ServiceIdentification"
                );

                if ($metadata_wfs_load->getElementsByTagName(
                        "SV_ServiceIdentification"
                    ) && $metadata_wfs_load->getElementsByTagName(
                        "SV_ServiceIdentification"
                    )->length !== 0) {
                    $insertIn->item(0)->appendChild($newNodeDesc);
                }
            }
            $objXPath = $xpath->query(
                "/gmd:MD_Metadata/gmd:distributionInfo"
            );
            if ($objXPath->length > 0) {
                foreach ($objXPath as $item) {
                    $item->parentNode->removeChild($item);
                }
            }

            $strXmlGML = "<tag xmlns:gmd=\"http://www.isotc211.org/2005/gmd\" xmlns:gco=\"http://www.isotc211.org/2005/gco\">
                     <gmd:distributionInfo>
                        <gmd:MD_Distribution>
                          <gmd:distributionFormat>
                            <gmd:MD_Format>
                              <gmd:name>
                                <gco:CharacterString>GML</gco:CharacterString>
                              </gmd:name>
                              <gmd:version>
                                <gco:CharacterString>3.2.1</gco:CharacterString>
                              </gmd:version>
                            </gmd:MD_Format>
                          </gmd:distributionFormat>
                          <gmd:transferOptions>
                            <gmd:MD_DigitalTransferOptions>
                              <gmd:onLine>
                                <gmd:CI_OnlineResource>
                                  <gmd:linkage><gmd:URL>" . $this->parameterBag->get('PRODIGE_URL_DATACARTO') .
                "/wfs/" . $wfsUuid . "?service=WFS&amp;request=GetCapabilities" . "</gmd:URL>
                          </gmd:linkage>
                          <gmd:protocol>
                            <gco:CharacterString>OGC:WFS-1.0.0-http-get-capabilities</gco:CharacterString>
                          </gmd:protocol>
                        </gmd:CI_OnlineResource>
                      </gmd:onLine>
                    </gmd:MD_DigitalTransferOptions>
                  </gmd:transferOptions>
                </gmd:MD_Distribution>
                </gmd:distributionInfo></tag>";

            $service = new \DOMDocument($version, "UTF-8");
            $service->loadXML($strXmlGML);

            $nodeToImport = $service->getElementsByTagName("distributionInfo")->item(0);
            $newNodeDesc = $metadata_wfs_load->importNode($nodeToImport, true);
            $insertIn = $metadata_wfs_load->getElementsByTagName(
                "MD_Metadata"
            );

            if ($metadata_wfs_load->getElementsByTagName(
                    "MD_Metadata"
                ) && $metadata_wfs_load->getElementsByTagName(
                    "MD_Metadata"
                )->length !== 0) {
                $insertIn->item(0)->appendChild($newNodeDesc);
            }

            $new_metadata_wfs_service = $metadata_wfs_load->saveXML();
            $callbackReturn = $this->patchXml($new_metadata_wfs_service, $wfsUuid);

            return $callbackReturn;
        }
        return false;
    }

    public function addContactForOpenData(
        $metadataUuid,
        $user,
        $data
    ) {
        $geonetwork = new GeonetworkInterface($this->parameterBag->get('PRO_GEONETWORK_URLBASE'), 'srv/fre/');

        $version = "1.0";
        $metadata_doc = new \DOMDocument($version, "UTF-8");
        $entete = "<?xml version=\"" . $version . "\" encoding=\"UTF-8\"?>\n";
        $metadata_doc = new \DOMDocument($version, "UTF-8");
        $xml = $this->getXmlInfo($geonetwork, $metadataUuid);
        $data->setMetadataUuid($metadataUuid);
        $metadata_data = $xml['data'];
        if (@$metadata_doc->loadXML($metadata_data) !== false) {
            $xpath = new \DOMXPath($metadata_doc);
            if ($data->getContact()) {
                //on enleve tous les items
                $objXPathContact = @$xpath->query(
                    "/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:pointOfContact"
                );
                $c = count($objXPathContact);

                if ($c !== 0) {
                    for ($i = 0; $i <= $c - 1; $i++) {
                        $item = $objXPathContact->item($i);
                        $nodeMother = $xpath->query(
                            "/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification"
                        );
                        $node = $nodeMother->item(0);
                        $node->removeChild($item);
                    }
                }

                foreach ($data->getContact() as $contact) {
                    $metadata_doc = $this->addContact($metadata_doc, $contact, $user);
                }
                $metadata_doc->preserveWhiteSpace = false;

                $new_metadata_data = $metadata_doc->saveXML();

                $new_metadata_data = str_replace($entete, "", $new_metadata_data);

                return $this->patchXml($new_metadata_data, $data->getMetadataUuid());
            }
        }
    }
}
