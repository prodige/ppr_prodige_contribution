<?php

declare(strict_types=1);

namespace App\EventListener;

use Doctrine\ORM\Tools\Event\GenerateSchemaEventArgs;
use Doctrine\Common\Annotations\Reader;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Alkante\Core\ComponentBundle\Annotations\Ignore as IgnoreAnnotation;
use Symfony\Component\Console\Output\StreamOutput;

/**
 * IgnoreTablesListener
 *
 * services.yaml :
 *
 * parameters:
 *   app_doctrine_output              : bool   true to write diff on console
 *   app_doctrine_inc_filter_tables   : string with reg exp to include some tables (schema.table)
 *   app_doctrine_inc_filter_entities : string with reg exp to include some entities (className ou Namespace\ClassName)
 *   app_doctrine_exc_filter_tables   : string with reg exp to exclude some tables (schema.table)
 *   app_doctrine_exc_filter_sequences: string with reg exp to exclude some sequences (schema.sequence)
 *   app_doctrine_exc_filter_entities : string with reg exp to exclude some tables (schema.table)
 *   app_doctrine_ignored_tables      : array of tables to ignore (schemaName.tableName or tableName on public schema)
 *   app_doctrine_ignored_foreignKeys : array of foreignkeys to ignore (name of fk)
 *   app_doctrine_ignored_entities    : array of entities to ignore (className ou Namespace\ClassName)
 */
class IgnoreTablesListener
{
    /** regexp include filter tables */
    protected $incFilterTables;
    /** regexp include filter tables */
    protected $incFilterEntities;

    /** regexp exclude filter tables */
    protected $excFilterTables;
    /** regexp exclude filter sequences */
    protected $excFilterSequences;
    /** regexp exclude filter tables */
    protected $excFilterEntities;

    /** set of ignored Tables with schema name : schema.table */
    protected $ignoredTables;
    /** set of ignored ForeignKeys with foreignkey name */
    protected $ignoredForeignKeys;
    /** set of ignored entities with or without namespace */
    protected $ignoredEntites;

    protected $parameterBag;
    protected $annotationReader;
    protected $output;

    /**
     * constructeur
     * @param ParameterBagInterface $parameterBag
     * @param Reader                $annotationReader
     */
    public function __construct(ParameterBagInterface $parameterBag, Reader $annotationReader)
    {
        $this->parameterBag = $parameterBag;
        $this->annotationReader = $annotationReader;

        $useOutput = ( $this->parameterBag->has("app_doctrine_output") ? boolval($this->parameterBag->get("app_doctrine_output")) : false );
        $this->output = ( $useOutput ? new StreamOutput(fopen('php://stdout', 'w')) : null );

        $this->incFilterTables    = ( $this->parameterBag->has("app_doctrine_inc_filter_tables")    ? $this->parameterBag->get("app_doctrine_inc_filter_tables")    : null );
        $this->incFilterEntities  = ( $this->parameterBag->has("app_doctrine_inc_filter_entities")  ? $this->parameterBag->get("app_doctrine_inc_filter_entities")  : null );
        $this->excFilterSequences = ( $this->parameterBag->has("app_doctrine_exc_filter_sequences") ? $this->parameterBag->get("app_doctrine_exc_filter_sequences") : null );
        $this->excFilterTables    = ( $this->parameterBag->has("app_doctrine_exc_filter_tables")    ? $this->parameterBag->get("app_doctrine_exc_filter_tables")    : null );
        $this->excFilterEntities  = ( $this->parameterBag->has("app_doctrine_exc_filter_entities")  ? $this->parameterBag->get("app_doctrine_exc_filter_entities")  : null );
        $this->ignoredForeignKeys = ( $this->parameterBag->has("app_doctrine_ignored_foreignKeys")  ? $this->parameterBag->get("app_doctrine_ignored_foreignKeys")  : [] );
        $this->ignoredTables      = ( $this->parameterBag->has("app_doctrine_ignored_tables")       ? $this->parameterBag->get("app_doctrine_ignored_tables")       : [] );
        $this->ignoredEntities    = ( $this->parameterBag->has("app_doctrine_ignored_entities")     ? $this->parameterBag->get("app_doctrine_ignored_entities")     : [] );

        if( !is_array($this->ignoredTables) ) {
            $this->ignoredTables = [];
        }
        if( !is_array($this->ignoredEntities) ) {
            $this->ignoredEntities = [];
        }
    }

    /**
     * Remove ignored tables /entities from Schema
     *
     * @param GenerateSchemaEventArgs $args
     */
    public function postGenerateSchema(GenerateSchemaEventArgs $args)
    {
        $schema = $args->getSchema();
        $em = $args->getEntityManager();
        $schemaManager = $em->getConnection()->getSchemaManager();

        $incFilterTablesTest    = !empty($this->incFilterTables)    && is_string($this->incFilterTables);
        $incFilterEntitiesTest  = !empty($this->incFilterEntities)  && is_string($this->incFilterEntities);
        $excFilterTablesTest    = !empty($this->excFilterTables)    && is_string($this->excFilterTables);
        $excFilterSequencesTest = !empty($this->excFilterSequences) && is_string($this->excFilterSequences);
        $excFilterEntitiesTest  = !empty($this->excFilterEntities)  && is_string($this->excFilterEntities);
        $ignoreForeignKeysTest  = !empty($this->ignoredForeignKeys);
        $ignoreEntitiesTest     = !empty($this->ignoredEntities);

        $entities = $em->getConfiguration()->getMetadataDriverImpl()->getAllClassNames();
        foreach($entities as $className) {
            $reflect = new \ReflectionClass($className);
            $entityName = $reflect->getShortName();
            $tableName  = $em->getClassMetadata($className)->getSchemaName().'.'.$em->getClassMetadata($className)->getTableName();

            $ignoreAnnotation = $this->annotationReader->getClassAnnotation($reflect, IgnoreAnnotation::class);

            if( !is_null($ignoreAnnotation)
                || ( $ignoreEntitiesTest    && (in_array($entityName, $this->ignoredEntities) || in_array($className, $this->ignoredEntities)) )
                || ( $incFilterEntitiesTest && preg_match($this->incFilterEntities, $entityName) == 0 )
                || ( $excFilterEntitiesTest && preg_match($this->excFilterEntities, $entityName) >  0 )
                || ( $incFilterTablesTest   && preg_match($this->incFilterTables,   $tableName ) == 0 )
                || ( $excFilterTablesTest   && preg_match($this->excFilterTables,   $tableName ) >  0 ) ) {

                $this->ignoredTables[] = $tableName;
            }
        }

        $schemaTableNames = $schema->getTableNames();
        foreach ($schemaTableNames as $tableName) {

            if( in_array($tableName, $this->ignoredTables)
                || ( $incFilterTablesTest   && preg_match($this->incFilterTables,   $tableName ) == 0 )
                || ( $excFilterTablesTest   && preg_match($this->excFilterTables,   $tableName ) > 0  ) ) {

                // remove table from schema
                $schema->dropTable($tableName);
                $this->write(sprintf("-- ignored table : %s", $tableName));
            }
        }

        $dbTables = $schemaManager->listTableNames();
        foreach($dbTables as $tableName) {

            $schemaTable = ( $schema->hasTable($tableName) ? $schema->getTable($tableName) : null );

            $newTable = null;
            if( !$schema->hasTable($tableName)
                && ( in_array($tableName, $this->ignoredTables)
                    || ( $incFilterTablesTest && preg_match($this->incFilterTables, $tableName ) == 0 )
                    || ( $excFilterTablesTest && preg_match($this->excFilterTables, $tableName ) > 0 )) ) {

                $newTable = $schema->createTable($tableName);
                $newTable instanceof Table;
                $this->write(sprintf("-- added table : %s", $tableName));

                // add columns
                $dbColumns = $schemaManager->listTableColumns($tableName);
                foreach($dbColumns as $column) {
                    $column instanceof Column;

                    $newColumn = $newTable->addColumn($column->getName(), $column->getType()->getName());
                    $newColumn->setAutoincrement($column->getAutoincrement());
                    $newColumn->setColumnDefinition($column->getColumnDefinition());
                    $newColumn->setComment($column->getComment());
                    $newColumn->setCustomSchemaOptions($column->getCustomSchemaOptions());
                    $newColumn->setDefault($column->getDefault());
                    $newColumn->setFixed($column->getFixed());
                    $newColumn->setLength($column->getLength());
                    $newColumn->setPrecision($column->getPrecision());
                    $newColumn->setScale($column->getScale());
                    $newColumn->setUnsigned($column->getUnsigned());
                    $newColumn->setNotnull($column->getNotnull());
                    $this->write(sprintf("-- added column : %s.%s", $tableName, $column->getName()));
                }

                // add indexes
                $dbIndexes = $schemaManager->listTableIndexes($tableName);
                foreach($dbIndexes as $index) {
                    $index instanceof Index;

                    if( $index->isPrimary() ) {
                        $newTable->setPrimaryKey($index->getColumns(), $index->getName());
                        $this->write(sprintf("-- added primary key : %s on %s", $index->getName(), $tableName));
                    }
                    elseif( $index->isUnique()) {
                        $newTable->addUniqueIndex($index->getColumns(), $index->getName(), $index->getOptions());
                        $this->write(sprintf("-- added uniq index : %s on %s", $index->getName(), $tableName));
                    }
                    else {
                        $newTable->addIndex($index->getColumns(), $index->getName(), $index->getFlags(), $index->getOptions());
                        $this->write(sprintf("-- added simple index : %s on %s", $index->getName(), $tableName));
                    }
                }
            }

            // add fks
            if( !is_null($newTable) || ($ignoreForeignKeysTest && !is_null($schemaTable)) ) {
                $dbFkeys = $schemaManager->listTableForeignKeys($tableName);
                foreach($dbFkeys as $fk) {
                    $fk instanceof ForeignKeyConstraint;

                    if( !is_null($newTable) ) {
                        $newTable->addForeignKeyConstraint($fk->getForeignTableName(), $fk->getLocalColumns(), $fk->getForeignColumns(), $fk->getOptions(), $fk->getName());
                        $this->write(sprintf("-- added foreign key : %s on %s", $fk->getName(), $tableName));
                    }
                    elseif( $ignoreForeignKeysTest && in_array($fk->getName(), $this->ignoredForeignKeys) && $schemaTable->hasForeignKey($fk->getName())) {
                        $schemaTable->removeForeignKey($fk->getName());
                        $this->write(sprintf("-- ignored foreign key : %s on %s", $fk->getName(), $tableName));
                    }
                }
            }
        }

        $databaseSequences = $schemaManager->listSequences();
        foreach ($databaseSequences as $sequence) {
            $seqName = $sequence->getName();

            if( !$schema->hasSequence($seqName)
                && ( $excFilterSequencesTest   && preg_match($this->excFilterSequences,   $seqName ) > 0  ) ) {
                // add a sequence to the schema to avoid deleting it during migration
                $schema->createSequence($seqName, $sequence->getAllocationSize(), $sequence->getInitialValue());
                $this->write(sprintf("-- added sequence : %s", $seqName));
            }
        }
    }

    /**
     * write texte on console
     * @param string $text
     * @return void
     */
    protected function write(string $text): void
    {
        if( !is_null($this->output) ) {
            $this->output->write($text, true);
        }
    }
}
