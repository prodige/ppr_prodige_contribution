<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221212152800 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE contrib.data ADD files VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE contrib.data ADD status INT DEFAULT NULL');
        $this->addSql('ALTER TABLE contrib.data ADD CONSTRAINT FK_C27530E37B00651C FOREIGN KEY (status) REFERENCES contrib.status (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_C27530E37B00651C ON contrib.data (status)');
        $this->addSql('ALTER TABLE contrib.data DROP files');
        $this->addSql('ALTER TABLE contrib.data_file ADD updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL');

    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE contrib.data DROP files');
        $this->addSql('ALTER TABLE contrib.data DROP CONSTRAINT FK_C27530E37B00651C');
        $this->addSql('DROP INDEX IDX_C27530E37B00651C');
        $this->addSql('ALTER TABLE contrib.data DROP status');
        $this->addSql('ALTER TABLE contrib.data ADD files VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE contrib.data_file DROP updated_at');
    }
}
