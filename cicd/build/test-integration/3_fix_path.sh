#!/bin/sh
set -e

# Source env file
SCRIPT=`readlink -f $0`
SCRIPTDIR=`dirname $SCRIPT`/..
SCRIPTNAME=`basename $SCRIPT`


sed -i "s|/var/www/html|`pwd`|g" ./report/*
