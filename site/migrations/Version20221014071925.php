<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221014071925 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE contrib.contribution (id SERIAL NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE contrib.data_file ADD contribution_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE contrib.data_file ADD CONSTRAINT FK_E5131CD2FE5E5FBD FOREIGN KEY (contribution_id) REFERENCES contrib.contribution (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_E5131CD2FE5E5FBD ON contrib.data_file (contribution_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE contrib.data_file DROP CONSTRAINT FK_E5131CD2FE5E5FBD');
        $this->addSql('DROP TABLE contrib.contribution');
        $this->addSql('DROP INDEX IDX_E5131CD2FE5E5FBD');
        $this->addSql('ALTER TABLE contrib.data_file DROP contribution_id');
    }
}
