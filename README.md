# ppr_prodige_contribution

## Context

Template for Symfony >= 5.6 with php 8.1

## Rename template

To rename the template to your projet name, run this :

```bash
./cicd/update-name.sh
```

## Development
Symfony

To run all functional tests into docker
```bash
php bin/phpunit --testdox
```

To check yaml syntax 
```bash
php bin/console lint:yaml config/ --parse-tags
```

To check translation syntax
```bash
php bin/console lint:xliff translations/
```

## Build for test and production

All informations for build for test and production are in [cicd/build/README.md](cicd/build/README.md)

## History of steps to construct this project

This part is just information to understand how this project was constructed

Build container and init the Symfony project.

```bash
cd ./cicd/dev/
docker login docker.alkante.com
# Put UID and GID in env file
echo -e "LOCAL_USER_ID=$(id -u)\nLOCAL_GROUP_ID=$(id -g)" > docker-compose.env;
# Build dev image
docker-compose build
# Run containers
docker-compose up
```

In a 2nd terminal :  
```bash
docker exec --user www-data -w /var/www/html/ -it ppr_prodige_contribution_dev_web bash
```


Symfony 5  
```bash
composer create-project symfony/website-skeleton:"^5.6" site
```

Change access variable to database in `site/.env.local`  
```
DATABASE_URL=postgresql://user_test:user_pass@prodige_database:5432/catalogue?serverVersion=12&charset=utf8
```

Create migration file to initialize database
```bash
./site/bin/console doctrine:migrations:generate
```

Edit it `site/src/Migrations/Version20200221092441.php`

Create default controller

```bash
./site/console make:controller BlogController
```

Edit it `site/src/Controller/DefaultController.php`.

Create template used by this controller `site/templates/default/user.html.twig`.

Create `.gitignore`.

```bash
echo "/jenkins_release" >> ./.gitignore
```

Finished. :)

Restart all containers by following the dev procedure cf. [cicd/dev/README.md](cicd/dev/README.md)
