<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Entity\Trait\TimestampsTrait;
use Symfony\Component\Validator\Constraints as Assert;


#[
    ApiResource(
        collectionOperations: [
            'get',
            'post' => [
                'openapi_context' => [
                    'summary' => 'To initialize a data object to link datafiles to the download',
                    'description' => 'Return Data',
                ],
                'denormalization_context' => ['groups' => ['PostData']],
            ]
        ],
        itemOperations: [
            'get',
            'patch' => [
                'denormalization_context' => ['groups' => ['PatchData']],
                'openapi_context' => [
                    'summary' => 'For the 1st patch, you must send the status 4',
                    'description' => 'Retrieves the form with all the data of the Data object to be updated.'
                ]
            ],
            'initialize' => [
                'method' => 'PATCH',
                'path' => '/data/initialize/{id}',
                'denormalization_context' => ['groups' => ['InitializeData']],
                'validation_context' => ['groups' => ['ValidInitializeData']],
                'openapi_context' => [
                    'summary' => 'To start communication with adminCarto',
                    'description' => 'Return Data',

                ],
            ],
            'recreate_data_sets' => [
                'method' => 'PATCH',
                'path' => '/data/recreate_data_sets/{id}',
                'denormalization_context' => ['groups' => ['RecreateDataSets']],
                'openapi_context' => [
                    'summary' => 'To replay datafields',
                    'description' => 'Return Data',

                ],
            ],
            'featureCatalogue' => [
                'method' => 'PATCH',
                'path' => '/data/featureCatalogue/{id}',
                'denormalization_context' => ['groups' => ['featureCatalogue']],
                'openapi_context' => [
                    'summary' => "Permet de mettre à jour le catalogue d'atribut",
                    'description' => 'Return Data',

                ],
            ],
            'delete'
        ],
        normalizationContext: ['groups' => ['GetData']]
    )]
#[ORM\Table(name: 'data', schema: 'contrib')]
#[ApiFilter(SearchFilter::class, properties: [
    'metadataUuid' => 'exact',
    'metadataSheetId' => 'exact'
])]
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class Data
{
    use TimestampsTrait;

    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[Groups(['GetData'])]
    #[ORM\Id]
    private int $id;

    /**
     * @var Status|null
     */
    #[ORM\ManyToOne(targetEntity: Status::class)]
    #[ORM\JoinColumn(name: 'status', referencedColumnName: 'id')]
    #[Groups(['PatchData', 'GetData', 'InitializeData', 'RecreateDataSets'])]
    #[Assert\NotBlank(groups: ['ValidInitializeData'])]
    #[ApiProperty(
        attributes: [
            "openapi_context" => [
                "type" => "iri-reference",
                "example" => "api/statuses/3",
            ],
        ],
    )]
    private ?Status $status = null;


    #[ORM\Column(name: 'admincarto_data_id', type: 'integer', nullable: true)]
    #[Groups(['GetData'])]
    #[Assert\Type('integer')]
    #[ApiProperty(
        attributes: [
            "openapi_context" => [
                "type" => "int",
                "example" => "2",
            ],
        ],
    )]
    private ?int $admincartoDataId;

    #[ORM\Column(name: 'user_id', type: 'integer', nullable: false)]
    #[Groups(['GetData'])]
    #[Assert\Type('integer')]
    #[ApiProperty(
        attributes: [
            "openapi_context" => [
                "type" => "int",
                "example" => 2,
            ],
        ],
    )]
    private int $userId;

    #[ORM\Column(name: 'metadata_uuid', type: 'string', nullable: true)]
    #[Groups(['GetData', 'PatchData'])]
    #[Assert\Type('string')]
    #[ApiProperty(
        attributes: [
            "openapi_context" => [
                "type" => "string",
                "example" => "493035c9-eef0-58li-a2ce-000c2929140b",
            ],
        ],
    )]
    private ?string $metadataUuid;

    #[ORM\Column(name: 'metadata_sheet_id', type: 'integer', nullable: true)]
    #[Assert\Type('integer')]
    #[Groups(['GetData'])]
    private ?int $metadataSheetId = null;

    #[ORM\OneToMany(mappedBy: 'data', targetEntity: DataFile::class, orphanRemoval: true)]
    #[Groups(['GetData'])]
    private Collection $datafiles;

    #[Groups(['GetData'])]
    #[Assert\Type('string')]
    #[ApiProperty(
        attributes: [
            "openapi_context" => [
                "type" => "string",
                "example" => "temp_2_51478965",
            ],
        ],
    )]
    private ?string $layerName = null;

    #[Groups(['GetData'])]
    #[Assert\Type('string')]
    #[ApiProperty(
        attributes: [
            "openapi_context" => [
                "type" => "string",
                "example" => "Tmp_642.map",
            ],
        ],
    )]
    private ?string $map;

    #[Groups(['PatchData', 'GetData', 'featureCatalogue'])]
    #[ApiProperty(
        attributes: [
            "openapi_context" => [
                "type" => "array",
                "items" => [
                    "anyOf" => [
                        "type" => "string"
                    ]
                ],
                "example" => [
                    ["name" => "mi_prinx", "type" => "NUMBER", "alias" => "mi_prinx", "description" => ""],
                    ["name" => "zp_com", "type" => "STRING", "alias" => "zp_com", "description" => ""]
                ]
            ],
        ],
    )]
    private ?array $fields;

    #[Groups(['GetData'])]
    #[Assert\Type('integer')]
    #[ApiProperty(
        attributes: [
            "openapi_context" => [
                "type" => "int",
                "example" => 2154,
            ],
        ],
    )]
    private ?int $sourceSRS;

    #[Groups(['GetData'])]
    #[Assert\Type('string')]
    #[ApiProperty(
        attributes: [
            "openapi_context" => [
                "type" => "string",
                "example" => "UTF-8",
            ],
        ],
    )]
    private ?string $sourceEncoding;

    #[Groups(['GetData'])]
    #[ApiProperty(
        attributes: [
            "openapi_context" => [
                "type" => "array",
                "items" => [
                    "anyOf" => [
                        "type" => "number"
                    ]
                ],
                "example" => [
                    "xmin" => 209550.556122134,
                    "ymin" => 6801752.11055561,
                    "xmax" => 333165.25956506,
                    "ymax" => 6882101.40403796
                ]
            ],
        ],
    )]
    private ?array $extent = null;

    #[Groups(['PatchData', 'GetData'])]
    #[Assert\Type('string')]
    #[ApiProperty(
        attributes: [
            "openapi_context" => [
                "type" => "string",
                "example" => "Nom en rapport avec la map",
            ],
        ],
    )]
    private ?string $table = null;

    #[Groups(['PatchData', 'GetData'])]
    #[Assert\Type('string')]
    #[ApiProperty(
        attributes: [
            "openapi_context" => [
                "type" => "string",
                "example" => "un titre pour la carte",
            ],
        ],
    )]
    private ?string $title = null;

    #[Groups(['PatchData', 'GetData'])]
    #[Assert\Type('string')]
    #[ApiProperty(
        attributes: [
            "openapi_context" => [
                "type" => "string",
                "example" => "un titre un peu plus détaillé",
            ],
        ],
    )]
    private ?string $abstract = null;

    #[Groups(['PatchData', 'GetData'])]
    #[Assert\Type('string')]
    #[ApiProperty(
        attributes: [
            "openapi_context" => [
                "type" => "string",
                "example" => "TEXT",
            ],
        ],
    )]
    private ?string $lineage = null;

    #[Groups(['PatchData', 'GetData'])]
    #[ApiProperty(
        attributes: [
            "openapi_context" => [
                "type" => "array",
                "items" => [
                    "anyOf" => [
                        "type" => "string"
                    ]
                ],
                "example" => [
                    "external.place.regions" => [
                        "Czech Republic",
                        "Croatia",
                        "Continents"
                    ]
                ]
            ],
        ],
    )]
    private ?array $localisationKeyword = null;

    #[Groups(['PatchData', 'GetData'])]
    #[ApiProperty(
        attributes: [
            "openapi_context" => [
                "type" => "array",
                "items" => [
                    "anyOf" => [
                        "type" => "number"
                    ]
                ],
                "example" => [
                    "external.theme.inspire-theme" => [
                        "Adresses",
                        "Répartition de la population — démographie"
                    ]
                ]
            ],
        ],
    )]
    private ?array $themeKeyword = null;

    #[Groups(['PatchData', 'GetData'])]
    private ?array $otherKeyword = null;

    #[Groups(['PatchData', 'GetData'])]
    #[Assert\Type('integer')]
    #[ApiProperty(
        attributes: [
            "openapi_context" => [
                "type" => "int",
                "example" => 21000,
            ],
        ],
    )]
    private ?int $equivalentScale = null;

    #[Groups(['PatchData', 'GetData'])]
    #[ApiProperty(
        attributes: [
            "openapi_context" => [
                "type" => "array",
                "items" => [
                    "anyOf" => [
                        "type" => "string"
                    ]
                ],
                "example" => [
                    [
                        "mail" => "jesuisunmail@nomdedomaine.com",
                        "organization" => "ALKANTE",
                        "role" => "pointOfContact",
                        "type" => "link",
                    ]
                ]
            ],
        ],
    )]
    private ?array $contact = null;

    #[Groups(['PatchData', 'GetData'])]
    #[Assert\Type('string')]
    #[ApiProperty(
        attributes: [
            "openapi_context" => [
                "type" => "string",
                "example" => "required"
            ],
        ],
    )]
    private ?string $geonetworkStatus = null;

    #[Groups(['GetData'])]
    #[Assert\Type('string')]
    private ?string $wfsMetadataUuid = null;

    #[Groups(['PatchData', 'GetData'])]
    #[ApiProperty(
        attributes: [
            "openapi_context" => [
                "type" => "boolean",
                "example" => false,
            ],
        ],
    )]
    private ?bool $openData = null;

    #[Groups(['PatchData', 'GetData'])]
    #[ApiProperty(
        attributes: [
            "openapi_context" => [
                "type" => "boolean",
                "example" => false,
            ],
        ],
    )]
    private ?bool $wms = null;

    #[Groups(['PatchData', 'GetData'])]
    #[ApiProperty(
        attributes: [
            "openapi_context" => [
                "type" => "boolean",
                "example" => false,
            ],
        ],
    )]
    private ?bool $wfs = null;

    #[Groups(['PatchData', 'GetData'])]
    #[ApiProperty(
        attributes: [
            "openapi_context" => [
                "type" => "array",
                "items" => [
                    "anyOf" => [
                        "type" => "int"
                    ]
                ],
                "example" => [
                    1,
                    2,
                    3
                ]

            ],
        ],
    )]
    private ?array $subdomains = null;

    #[Groups(['GetData', 'PatchData'])]
    #[Assert\Type('string')]
    #[ApiProperty(
        attributes: [
            "openapi_context" => [
                "type" => "uri",
                "example" => "/api/lex_layer_types/3"
            ],
        ],
    )]
    private ?string $type = null;

    #[Groups(['PatchData', 'GetData'])]
    #[Assert\Type('string')]
    private ?string $thematiqueISO = null;

    #[Groups(['PatchData', 'GetData'])]
    #[Assert\Type('string')]
    private ?string $mdMaintenance = null;

    #[Groups(['PatchData', 'GetData'])]
    #[Assert\Type('string')]
    private ?string $mdContrainte = null;

    #[Groups(['InitializeData', 'GetData'])]
    #[Assert\Type('string')]
    #[ApiProperty(
        attributes: [
            "openapi_context" => [
                "type" => "string",
                "example" => "Projection EPSG",
            ],
        ],
    )]
    private ?string $projection = null;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Data
     */
    public function setId(int $id): Data
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Status|null
     */
    public function getStatus(): ?Status
    {
        return $this->status;
    }

    /**
     * @param Status|null $status
     * @return Data
     */
    public function setStatus(?Status $status): Data
    {
        $this->status = $status;
        return $this;
    }


    /**
     * @return int|null
     */
    public function getAdmincartoDataId(): ?int
    {
        return $this->admincartoDataId;
    }

    /**
     * @param int|null $admincartoDataId
     * @return Data
     */
    public function setAdmincartoDataId(?int $admincartoDataId): Data
    {
        $this->admincartoDataId = $admincartoDataId;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMetadataUuid(): ?string
    {
        return $this->metadataUuid;
    }

    /**
     * @param string|null $metadataUuid
     * @return Data
     */
    public function setMetadataUuid(?string $metadataUuid): Data
    {
        $this->metadataUuid = $metadataUuid;
        return $this;
    }

    /**
     * @return Collection|null
     */
    public function getDatafiles(): ?Collection
    {
        return $this->datafiles;
    }

    /**
     * @param Collection|null $datafiles
     * @return Data
     */
    public function setDatafiles(?Collection $datafiles): Data
    {
        $this->datafiles = $datafiles;
        return $this;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     * @return Data
     */
    public function setUserId(int $userId): Data
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMap(): ?string
    {
        return $this->map;
    }

    /**
     * @param string|null $map
     * @return Data
     */
    public function setMap(?string $map): Data
    {
        $this->map = $map;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLayerName(): ?string
    {
        return $this->layerName;
    }

    /**
     * @param string|null $layerName
     * @return Data
     */
    public function setLayerName(?string $layerName): Data
    {
        $this->layerName = $layerName;
        return $this;
    }


    /**
     * @return array|null
     */
    public function getFields(): ?array
    {
        return $this->fields;
    }

    /**
     * @param array|null $fields
     * @return Data
     */
    public function setFields(?array $fields): Data
    {
        $this->fields = $fields;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getSourceSRS(): ?int
    {
        return $this->sourceSRS;
    }

    /**
     * @param int|null $sourceSRS
     * @return Data
     */
    public function setSourceSRS(?int $sourceSRS): Data
    {
        $this->sourceSRS = $sourceSRS;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSourceEncoding(): ?string
    {
        return $this->sourceEncoding;
    }

    /**
     * @param string|null $sourceEncoding
     * @return Data
     */
    public function setSourceEncoding(?string $sourceEncoding): Data
    {
        $this->sourceEncoding = $sourceEncoding;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getExtent(): ?array
    {
        return $this->extent;
    }

    /**
     * @param array|null $extent
     * @return Data
     */
    public function setExtent(?array $extent): Data
    {
        $this->extent = $extent;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTable(): ?string
    {
        return $this->table;
    }

    /**
     * @param string|null $table
     * @return Data
     */
    public function setTable(?string $table): Data
    {
        $this->table = $table;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     * @return Data
     */
    public function setTitle(?string $title): Data
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAbstract(): ?string
    {
        return $this->abstract;
    }

    /**
     * @param string|null $abstract
     * @return Data
     */
    public function setAbstract(?string $abstract): Data
    {
        $this->abstract = $abstract;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLineage(): ?string
    {
        return $this->lineage;
    }

    /**
     * @param string|null $lineage
     * @return Data
     */
    public function setLineage(?string $lineage): Data
    {
        $this->lineage = $lineage;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getLocalisationKeyword(): ?array
    {
        return $this->localisationKeyword;
    }

    /**
     * @param array|null $localisationKeyword
     * @return Data
     */
    public function setLocalisationKeyword(?array $localisationKeyword): Data
    {
        $this->localisationKeyword = $localisationKeyword;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getThemeKeyword(): ?array
    {
        return $this->themeKeyword;
    }

    /**
     * @param array|null $themeKeyword
     * @return Data
     */
    public function setThemeKeyword(?array $themeKeyword): Data
    {
        $this->themeKeyword = $themeKeyword;
        return $this;
    }

    public function getOtherKeyword(): ?array
    {
        return $this->otherKeyword;
    }

    public function setOtherKeyword(?array $otherKeyword): void
    {
        $this->otherKeyword = $otherKeyword;
    }


    /**
     * @return int|null
     */
    public function getEquivalentScale(): ?int
    {
        return $this->equivalentScale;
    }

    /**
     * @param int|null $equivalentScale
     * @return Data
     */
    public function setEquivalentScale(?int $equivalentScale): Data
    {
        $this->equivalentScale = $equivalentScale;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getContact(): ?array
    {
        return $this->contact;
    }

    /**
     * @param array|null $contact
     * @return Data
     */
    public function setContact(?array $contact): Data
    {
        $this->contact = $contact;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getOpenData(): ?bool
    {
        return $this->openData;
    }

    /**
     * @param bool|null $openData
     * @return Data
     */
    public function setOpenData(?bool $openData): Data
    {
        $this->openData = $openData;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getWms(): ?bool
    {
        return $this->wms;
    }

    /**
     * @param bool|null $wms
     * @return Data
     */
    public function setWms(?bool $wms): Data
    {
        $this->wms = $wms;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getWfs(): ?bool
    {
        return $this->wfs;
    }

    /**
     * @param bool|null $wfs
     * @return Data
     */
    public function setWfs(?bool $wfs): Data
    {
        $this->wfs = $wfs;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getGeonetworkStatus(): ?string
    {
        return $this->geonetworkStatus;
    }

    /**
     * @param string|null $geonetworkStatus
     * @return Data
     */
    public function setGeonetworkStatus(?string $geonetworkStatus): Data
    {
        $this->geonetworkStatus = $geonetworkStatus;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getSubdomains(): ?array
    {
        return $this->subdomains;
    }

    /**
     * @param array|null $subdomains
     * @return Data
     */
    public function setSubdomains(?array $subdomains): Data
    {
        $this->subdomains = $subdomains;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getMetadataSheetId(): ?int
    {
        return $this->metadataSheetId;
    }

    /**
     * @param int|null $metadataSheetId
     * @return Data
     */
    public function setMetadataSheetId(?int $metadataSheetId): Data
    {
        $this->metadataSheetId = $metadataSheetId;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     * @return Data
     */
    public function setType(?string $type): Data
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getProjection(): ?string
    {
        return $this->projection;
    }

    /**
     * @param string|null $projection
     * @return Data
     */
    public function setProjection(?string $projection): Data
    {
        $this->projection = $projection;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getWfsMetadataUuid(): ?string
    {
        return $this->wfsMetadataUuid;
    }

    /**
     * @param string|null $wfsMetadataUuid
     * @return Data
     */
    public function setWfsMetadataUuid(?string $wfsMetadataUuid): Data
    {
        $this->wfsMetadataUuid = $wfsMetadataUuid;
        return $this;
    }

    public function getThematiqueISO(): ?string
    {
        return $this->thematiqueISO;
    }

    public function setThematiqueISO(?string $thematiqueISO): void
    {
        $this->thematiqueISO = $thematiqueISO;
    }

    public function getMdMaintenance(): ?string
    {
        return $this->mdMaintenance;
    }

    public function setMdMaintenance(?string $mdMaintenance): void
    {
        $this->mdMaintenance = $mdMaintenance;
    }

    public function getMdContrainte(): ?string
    {
        return $this->mdContrainte;
    }

    public function setMdContrainte(?string $mdContrainte): void
    {
        $this->mdContrainte = $mdContrainte;
    }

}
