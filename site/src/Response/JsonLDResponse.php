<?php

declare(strict_types=1);

namespace App\Response;

use Symfony\Component\HttpFoundation\Response;

use function Carmen\ApiBundle\Entity\str_starts_with;

class JsonLDResponse extends Response
{

    protected $data;
    protected $callback;

    private $resource;
    private $resourceId;
    private $encodingOptions = 0;

    /**
     * @param mixed $data The response data
     * @param int $status The response status code
     * @param array $headers An array of response headers
     * @param bool $json If the data is already a JSON string
     * @param string $resource Class resource name
     * @param int $id Virtual id of the resource
     */
    public function __construct(
        $data = [],
        int $status = 200,
        array $headers = [],
        bool $json = false,
        string $resource = "Custom",
        int $id = 0
    ) {
        parent::__construct('', $status, $headers);

        $this->resource = $resource;
        $this->resourceId = $id;

        if ($json && !\is_string($data) && !is_numeric($data) && !\is_callable([$data, '__toString'])) {
            throw new \TypeError(
                sprintf(
                    '"%s": If $json is set to true, argument $data must be a string or object implementing __toString(), "%s" given.',
                    __METHOD__,
                    get_debug_type($data)
                )
            );
        }

        if (null === $data) {
            $data = new \ArrayObject();
        }

        $json ? $this->setJson($data) : $this->setData($data);
    }

    /**
     * Sets a raw string containing a JSON document to be sent.
     *
     * @return $this
     */
    public function setJson(string $json)
    {
        $tmpJson = json_decode($json, true);
        $struct["@context"] = "/api/contexts/" . $this->resource;
        $struct["@id"] = "/api/" . strtolower($this->resource) . '/' . $this->resourceId;
        $struct["@type"] = "/api/contexts/" . $this->resource;
        $json = array_merge($struct, $tmpJson);
        $this->data = json_encode($json);

        return $this->update();
    }

    /**
     * Updates the content and headers according to the JSON data and callback.
     *
     * @return $this
     */
    protected function update()
    {
        if (!$this->headers->has('Content-Type') || 'text/javascript' === $this->headers->get('Content-Type')) {
            $this->headers->set('Content-Type', 'application/ld+json');
        }

        return $this->setContent($this->data);
    }

    /**
     * Sets the data to be sent as JSON.
     *
     * @param mixed $data
     *
     * @return $this
     *
     * @throws \InvalidArgumentException
     */
    public function setData(array $data = [])
    {
        try {
            $data = json_encode($data, $this->encodingOptions);
        } catch (\Exception $e) {
            if ('Exception' === \get_class($e) && str_starts_with($e->getMessage(), 'Failed calling ')) {
                throw $e->getPrevious() ?: $e;
            }
            throw $e;
        }
        if (\PHP_VERSION_ID >= 70300 && (\JSON_THROW_ON_ERROR & $this->encodingOptions)) {
            return $this->setJson($data);
        }

        if (\JSON_ERROR_NONE !== json_last_error()) {
            throw new \InvalidArgumentException(json_last_error_msg());
        }

        return $this->setJson($data);
    }
}
