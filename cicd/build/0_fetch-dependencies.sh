#!/bin/sh
set -e

# Source env file
SCRIPT=`readlink -f $0`
SCRIPTDIR=`dirname $SCRIPT`
SCRIPTNAME=`basename $SCRIPT`
. $SCRIPTDIR/env.sh

## Prompt nexus.alkante.com password if needed
if [ -z "${NEXUS_USR}" ]; then
  echo -n "Enter nexus.alkante.com login: "
  read NEXUS_USR
fi
if [ -z "${NEXUS_PSW}" ]; then
  echo -n "password: "
  stty -echo
  IFS= read -r NEXUS_PSW
  stty echo
  printf '\n'
fi
[ -z "${REPO_COMPOSER_DOMAIN}" ] && REPO_COMPOSER_DOMAIN='nexus.alkante.com'

echo -n "LOCAL_USER_ID=$(id -u)\nLOCAL_GROUP_ID=$(id -g)" > $SCRIPTDIR/docker-build/docker-compose.env;

# Create dockers
docker-compose -f $SCRIPTDIR/docker-build/docker-compose.yml build
docker-compose -f $SCRIPTDIR/docker-build/docker-compose.yml up -d

# Exec
sleep 1
#docker exec --user www-data -w /var/www/html/site ppr_prodige_contribution_build_web /bin/bash -c " \
#  set -e; \
#  COMPOSER_OVERRIDE=composer.json; \
#  if [ -f composer_prod.json ]; then COMPOSER_OVERRIDE=composer_prod.json; fi; \
#  COMPOSER=\${COMPOSER_OVERRIDE} composer install --no-interaction --no-progress;"


docker exec --user www-data -w /var/www/html/site ppr_prodige_contribution_build_web /bin/bash -c " \
  set -e; \
  echo '{
    \"http-basic\": {
      \""$REPO_COMPOSER_DOMAIN"\": {
			  \"username\": \""$NEXUS_USR"\",
			  \"password\": \""$NEXUS_PSW"\"
      }
    }
  }' > auth.json; \
  COMPOSER_OVERRIDE=composer.json; \
  if [ -f composer_prod.json ]; then COMPOSER_OVERRIDE=composer_prod.json; fi; \
  COMPOSER=\${COMPOSER_OVERRIDE} composer install --no-interaction --no-progress; \
  rm auth.json"

docker exec --user www-data -w /var/www/html/site ppr_prodige_contribution_build_web /bin/bash -c " \
  set -e;
  php bin/console assets:install"

# Stop dockers
docker-compose -f $SCRIPTDIR/docker-build/docker-compose.yml down
