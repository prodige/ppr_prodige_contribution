# Changelog

All notable changes to [ppr_prodige_contribution](https://gitlab.adullact.net/prodige/ppr_prodige_contribution) project will be documented in this file.

## [5.0.79](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.78...5.0.79) - 2025-03-05

## [5.0.78](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.77...5.0.78) - 2025-03-04

## [5.0.77](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.76...5.0.77) - 2025-03-04

## [5.0.76](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.75...5.0.76) - 2025-03-03

## [5.0.75](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.74...5.0.75) - 2025-03-03

## [5.0.74](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.73...5.0.74) - 2025-02-13

## [5.0.73](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.72...5.0.73) - 2025-02-13

## [5.0.72](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.71...5.0.72) - 2025-01-10

## [5.0.72](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.71...5.0.72) - 2025-01-10

## [5.0.71](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.70...5.0.71) - 2025-01-08

## [5.0.70](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.69...5.0.70) - 2025-01-08

## [5.0.69](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.68...5.0.69) - 2024-12-18

## [5.0.68](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.67...5.0.68) - 2024-12-18

## [5.0.67](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.66...5.0.67) - 2024-12-12

## [5.0.66](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.65...5.0.66) - 2024-12-11

## [5.0.65](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.64...5.0.65) - 2024-11-29

## [5.0.64](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.63...5.0.64) - 2024-11-29

## [5.0.63](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.62...5.0.63) - 2024-11-28

## [5.0.62](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.61...5.0.62) - 2024-11-28

## [5.0.61](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.60...5.0.61) - 2024-11-27

## [5.0.60](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.59...5.0.60) - 2024-10-22

## [5.0.59](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.58...5.0.59) - 2024-10-18

## [5.0.58](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.57...5.0.58) - 2024-09-05

## [5.0.57](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.56...5.0.57) - 2024-09-05

## [5.0.56](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.55...5.0.56) - 2024-09-04

## [5.0.55](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.54...5.0.55) - 2024-09-04

## [5.0.54](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.53...5.0.54) - 2024-09-04

## [5.0.53](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.52...5.0.53) - 2024-09-02

## [5.0.52](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.1.4...5.0.52) - 2024-08-27

## [5.0.1.4](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.1.3...5.0.1.4) - 2024-08-22

## [5.0.1.3](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.1.2...5.0.1.3) - 2024-08-22

## [5.0.1.2](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.1.1...5.0.1.2) - 2024-08-21

## [5.0.1.1](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.51...5.0.1.1) - 2024-08-05

## [5.0.51](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.50...5.0.51) - 2024-06-21

## [5.0.50](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.49...5.0.50) - 2024-06-11

## [5.0.49](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.48...5.0.49) - 2024-03-22

## [5.0.48](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.47...5.0.48) - 2024-02-27

## [5.0.47](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.46...5.0.47) - 2024-02-27

## [5.0.46](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.45...5.0.46) - 2024-02-20

## [5.0.45](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.44...5.0.45) - 2024-02-16

## [5.0.44](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.43...5.0.44) - 2024-02-15

## [5.0.43](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.42...5.0.43) - 2024-02-13

## [5.0.42](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.41...5.0.42) - 2024-02-12

## [5.0.41](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.40...5.0.41) - 2024-02-09

## [5.0.40](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.39...5.0.40) - 2024-02-06

## [5.0.39](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.38...5.0.39) - 2024-02-05

## [5.0.38](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.37...5.0.38) - 2024-02-01

## [5.0.37](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.36...5.0.37) - 2024-01-25

## [5.0.36](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.35...5.0.36) - 2024-01-24

## [5.0.35](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.34...5.0.35) - 2024-01-24

## [5.0.34](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.33...5.0.34) - 2024-01-24

## [5.0.33](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.32...5.0.33) - 2024-01-22

## [5.0.32](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.31...5.0.32) - 2024-01-18

## [5.0.31](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.30...5.0.31) - 2024-01-18

## [5.0.30](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.29...5.0.30) - 2024-01-02

## [5.0.29](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.28...5.0.29) - 2023-12-15

## [5.0.28](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.27...5.0.28) - 2023-12-12

## [5.0.27](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.26...5.0.27) - 2023-09-27

## [5.0.26](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.25...5.0.26) - 2023-09-19

## [5.0.25](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.24...5.0.25) - 2023-07-20

## [5.0.24](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.23...5.0.24) - 2023-07-18

## [5.0.23](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.22...5.0.23) - 2023-07-18

## [5.0.22](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.21...5.0.22) - 2023-07-11

## [5.0.21](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.20...5.0.21) - 2023-07-07

## [5.0.20](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.19...5.0.20) - 2023-06-06

## [5.0.19](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.18...5.0.19) - 2023-06-06

## [5.0.18](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.17...5.0.18) - 2023-06-05

## [5.0.17](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.16...5.0.17) - 2023-06-02

## [5.0.16](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.15...5.0.16) - 2023-06-02

## [5.0.15](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.14...5.0.15) - 2023-05-15

## [5.0.14](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.13...5.0.14) - 2023-04-11

## [5.0.13](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.12...5.0.13) - 2023-03-22

## [5.0.12](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.11...5.0.12) - 2023-03-22

## [5.0.11](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.10...5.0.11) - 2023-03-21

## [5.0.10](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.9...5.0.10) - 2023-03-21

## [5.0.9](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.8...5.0.9) - 2023-03-15

## [5.0.8](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.7...5.0.8) - 2023-03-15

## [5.0.7](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.6...5.0.7) - 2023-03-08

## [5.0.6](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.5...5.0.6) - 2023-03-07

## [5.0.5](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.4...5.0.5) - 2023-03-03

## [5.0.4](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.3...5.0.4) - 2023-03-03

## [5.0.3](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.2...5.0.3) - 2023-03-02

## [5.0.2](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.1...5.0.2) - 2023-03-01

## [5.0.1](https://gitlab.adullact.net/prodige/ppr_prodige_contribution/compare/5.0.0...5.0.1) - 2023-02-28

