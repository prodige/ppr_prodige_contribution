<?php

namespace App\Service;


use App\DataPersister\DataFilePersister;
use App\Entity\Data;
use App\Entity\DataFile;
use Doctrine\ORM\EntityManagerInterface;
use Prodige\ProdigeBundle\Services\AdminClientService;
use Prodige\ProdigeBundle\Services\CurlUtilities;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Exception;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class AdminCartoDataService
{

    private array $requiredExtensionForMulti = [
        'shp' => ['shp', 'prj', 'dbf', 'shx'],
        'mif' => ['mif', 'mid'],
        'tab' => ['tab', 'id', 'map', 'dat'],
    ];

    public function __construct(
        private ParameterBagInterface $parameterBag,
        private DataFilePersister $dataFilePersister,
        private EntityManagerInterface $entityManager,
        private LoggerInterface $logger,
        private AdminClientService $adminClientService,
        private HttpClientInterface $client,
        private Security $security,
    ) {
        $this->adminClientService = $this->adminClientService;
    }

    /**
     * @param $files
     * Function unifying the file names for ogr2ogr
     * And returning the query for curlPostData as an array
     * @return array
     */
    public function createRequestCurlPost($files): array
    {
        $filename = uniqid('temp_file_', false);
        $tableName = uniqid('TMP_', false);
        $datafiles = ['files' => [], 'table' => $tableName];
        foreach ($files as $file) {
            $fileExtension = pathinfo($file->getFilePath(), PATHINFO_EXTENSION);
            rename(
                $this->parameterBag->get('file_dir_temp') . $file->getFilePath(),
                $this->parameterBag->get('file_dir_temp') . $filename . '.' . $fileExtension
            );
            $file->setFilePath($filename . '.' . $fileExtension);
            $this->entityManager->persist($file);
            $this->entityManager->flush();
            $datafiles['files'][] = $file->getFilePath();
        }

        return $datafiles;
    }


    /**
     * @param array $datafiles
     * @param array $files
     * The list of file names
     * @return array
     * @throws Exception
     */
    public function curlPostData(array $datafiles, array $files): array
    {
        $url = $this->parameterBag->get('PRODIGE_URL_ADMINCARTO_API') . 'data';
        $this->logger->info('Post vers admincarto');
        if ($this->security->getUser()->getToken()[0]) {

            $response = $this->client->request('POST', $url, [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json'
                ],
                'body' => json_encode($datafiles),
                'auth_bearer' => $this->security->getUser()->getToken()[0],
            ]);
            $response = $response->getContent(false);
        } else {
            $curl = CurlUtilities::curl_init($url);
            $curlopts = [
                CURLOPT_HTTPHEADER => ['Accept: application/json', 'Content-Type: application/json'],
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POSTFIELDS => json_encode($datafiles),
                CURLOPT_CUSTOMREQUEST => 'POST',
            ];
            curl_setopt_array($curl, $curlopts);
            $response = curl_exec($curl);
        }
        $jsonResponse = json_decode($response, true);
        if ($jsonResponse === null) {
            $this->logger->error('curl postData error ', ['error' => $response]);
            return ["error" => $response, "status" => 409];
        }
        if (is_array($jsonResponse) && (array_key_exists(
                    "error",
                    $jsonResponse
                ) || array_key_exists("detail", $jsonResponse)
            )) {
            if (array_key_exists("detail", $jsonResponse)) {
                $jsonResponse["error"] = $jsonResponse["detail"];
            }
            $this->logger->error('curl postData error insertion', ['error' => $jsonResponse["error"]]);
            return ["error" => $jsonResponse["error"], "status" => 404];
        }
        foreach ($files as $file) {
            unlink($this->parameterBag->get('file_dir_temp') . $file->getFilePath());
            $this->dataFilePersister->remove($file);
        }

        if (preg_match(
                '/\/api\/lex_layer_types\/(\d+)/',
                $jsonResponse['Data']['type'],
                $dataTypeId
            ) === 0) {
            return ["error" => "Update of type id failed", "status" => 409];
        }

        return [
            "data" => $jsonResponse,
            "type" => $jsonResponse['Data']['type'],
            "typeId" => (int)$dataTypeId[1],
            "status" => 201
        ];
    }

    public function curlRecreateDataSet(Data $data, $datafiles): array
    {
        $this->logger->info('Post vers admincarto');
        $url = $this->parameterBag->get('PRODIGE_URL_ADMINCARTO_API') . 'data/' . $data->getAdmincartoDataId(
            ) . "/recreate_data_sets";
        $filename = uniqid('temp_file_', false);

        foreach ($datafiles as $file) {
            $fileExtension = pathinfo($file->getFilePath(), PATHINFO_EXTENSION);
            rename(
                $this->parameterBag->get('file_dir_temp') . $file->getFilePath(),
                $this->parameterBag->get('file_dir_temp') . $filename . '.' . $fileExtension
            );
            $file->setFilePath($filename . '.' . $fileExtension);
            $this->entityManager->persist($file);
            $this->entityManager->flush();
            $request['files'][] = $file->getFilePath();
        }


        $curl = CurlUtilities::curl_init($url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Content-Type: application/json'));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PATCH');
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($request));
        curl_setopt($curl, CURLOPT_POST, true);
        $response = curl_exec($curl);
        $jsonResponse = json_decode($response, true);

        curl_close($curl);
        if ($jsonResponse === null) {
            $this->logger->error('curl postData error ', ['error' => $response]);
            return ["error" => $response, "status" => 409];
        }
        if (is_array($jsonResponse) && array_key_exists("error", $jsonResponse)) {
            $this->logger->error('curl postData error insertion', ['error' => $jsonResponse["error"]]);
            return ["error" => $jsonResponse["error"], "status" => 404];
        }
        foreach ($datafiles as $file) {
            $this->dataFilePersister->remove($file);
        }

        if (preg_match(
                '/\/api\/lex_layer_types\/(\d+)/',
                $jsonResponse['Data']['type'],
                $dataTypeId
            ) === 0) {
            return ["error" => "Update of type id failed", "status" => 409];
        }

        return [
            "data" => $jsonResponse,
            "type" => $jsonResponse['Data']['type'],
            "typeId" => (int)$dataTypeId[1],
            "status" => 201
        ];
    }

    /**
     * @param int $admincartoId
     * Delete in admincarto_api the data and its associated fields
     * @return false|mixed
     * @throws Exception
     */
    public function curlDeleteData(int $admincartoId): mixed
    {
        $this->logger->info('Delete vers admincarto');
        $url = $this->parameterBag->get('PRODIGE_URL_ADMINCARTO_API') . 'data/' . $admincartoId;
        $curl = CurlUtilities::curl_init($url);
        //
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Accept: /'));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
        $deleteInfo = curl_exec($curl);
        $deleteInfoJson = json_decode($deleteInfo, true);
        $this->logger->error('curl deleteData  ', ['response' => $deleteInfoJson]);
        curl_close($curl);
        if ($deleteInfoJson === null) {
            $this->logger->error('curl deleteData error ', ['error' => $deleteInfo]);
            return ["error" => $deleteInfo, "status" => 409];
        }
        if (array_key_exists("error", $deleteInfoJson)) {
            return ["error" => $deleteInfoJson['error'], "status" => 404];
        }
        return ["data" => "The data " . $admincartoId . " has been deleted", "status" => 200];
    }

    /**
     * @param $data
     * @param string $metadataUuid
     * @param int $type
     * @return array
     */
    public function createRequestCurlPatch($data, string $metadataUuid, int $type): array
    {
        /** @var Data $data */
        $request = [
            "table" => $data->getTable(),
            "title" => $data->getTitle(),
            "fields" => $data->getFields(),
            "uuid" => $metadataUuid,
            "wms" => ($data->getWms() !== null) ? $data->getWms() : $data->getOpenData(),
            "wfs" => ($data->getWfs() !== null) ? $data->getWfs() : $data->getOpenData(),
            "type" => $type,
            "sourceSRS" => $data->getSourceSRS(),
            "sourceEncoding" => $data->getSourceEncoding(),
            "abstract" => $data->getAbstract(),
            "themeKeyword" => $data->getThemeKeyword()
        ];

        if ($type === 1) {
            $request['extent'] = $data->getExtent();
        }

        return $request;
    }

    /**
     * @param int $admincartoId
     * @param array $request
     * @return array
     * @throws Exception
     * @throws TransportExceptionInterface
     */
    public function curlPatchData(int $admincartoId, array $request): array
    {
        $this->logger->info('Patch vers admincarto');
        $url = $this->parameterBag->get('PRODIGE_URL_ADMINCARTO_API') . 'data/' . $admincartoId;
        if (is_array($this->security->getUser()->getToken()) && $this->security->getUser()->getToken()[0]) {
            $response = $this->client->request('PATCH', $url, [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json'
                ],
                'body' => json_encode($request),
                'auth_bearer' => $this->security->getUser()->getToken()[0],
            ]);
            $response = $response->getContent(false);
        } else {
            $curl = CurlUtilities::curl_init($url);

            curl_setopt($curl, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Content-Type: application/json'));
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PATCH');
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($request));
            curl_setopt($curl, CURLOPT_POST, true);
            $response = curl_exec($curl);
            curl_close($curl);
        }
        $jsonResponse = json_decode($response, true);
        if ($jsonResponse === null || (array_key_exists('status', $jsonResponse) && $jsonResponse['status'] === 500)) {
            $this->logger->error('curl patchData error ', ['error' => $response]);
            return ["error" => $response, "status" => 409];
        }
        if (array_key_exists("error", $jsonResponse)) {
            return ["error" => $jsonResponse["error"], 'status' => 404];
        }
        return ["data" => $jsonResponse, "status" => 200];
    }

    /**
     * @param string $string
     * @return string
     */
    public function formatTableName(string $string): string
    {
        $string = str_replace(' ', '_', $string);
        $string = $this->removeAccents($string);

        return strtolower($string);
    }

    /**
     * Retire tous les accents ou lettres accolées de la chaine en maintenant les lettres
     * Retourne la chaine transformée
     * @param string $str chaine à traiter
     * @param string $charset encodage de la chaine, =utf-8 par défaut
     * @return string
     */
    public function removeAccents(string $str, string $charset = 'utf-8'): string
    {
        $str = htmlentities($str, ENT_NOQUOTES, $charset);

        $str = preg_replace('#&([A-za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
        $str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str); // pour les ligatures e.g. '&oelig;'
        $str = preg_replace('#&[^;]+;#', '', $str); // supprime les autres caractères

        return $str;
    }

    /**
     * @param array $files
     * @return array
     */
    public function checkFieldIntegrity(array $files): array
    {
        $isPresent = [];

        foreach ($files as $file) {
            if (!file_exists($this->parameterBag->get('file_dir_temp') . $file->getFilePath())) {
                return ["error" => "missing files on the server", "status" => 400];
            }
            $isPresent[] = pathinfo($file->getFilePath(), PATHINFO_EXTENSION);
        }

        foreach ($files as $file) {
            switch (pathinfo($file->getFilePath(), PATHINFO_EXTENSION)) {
                case "shp":
                case "mif":
                case "tab":
                    $isRequired = array_values(
                        $this->requiredExtensionForMulti[pathinfo($file->getFilePath(), PATHINFO_EXTENSION)]
                    );
                    if (array_diff_key($isRequired, $isPresent) !== []) {
                        return ["error" => "one of the required files is missing", "status" => 400];
                    }
                    return ["data" => "The control of the files has been successfully completed", "status" => 200];
                case 'gxt':
                case 'json' :
                case 'gml'  :
                case 'gmt'  :
                case 'kml'  :
                case 'csv'  :
                case 'xlsx' :
                case 'ods'  :
                case 'gpkg' :
                    return ["data" => "The control of the files has been successfully completed", "status" => 200];
            }
        }
        return ["error" => "the registration process cannot be started from the downloaded files", "status" => 400];
    }


    /**
     * @param int $admincartoId
     * Retrieves data related to data in admincarto with the admincarto_id
     * @return array
     * @throws Exception
     */
    public function curlGetData(int $admincartoId): array
    {
        $this->logger->info('Get vers admincarto');
        $url = $this->parameterBag->get('PRODIGE_URL_ADMINCARTO_API') . 'data/' . $admincartoId;
        $dataInfo = $this->adminClientService->curlAdminWithAdmincli($url,'GET');

        if ($dataInfo === null || $dataInfo === false) {
            $this->logger->error('curl getData error ', ['error' => 'response failed']);
            return ["error" => $dataInfo, "status" => 409];
        }
        if (array_key_exists("error", $dataInfo)) {
            return ["error" => $dataInfo, "status" => 404];
        }

        return $dataInfo;
    }
}
